const { app, BrowserWindow } = require('electron');
const url = require('url');
const path = require('path');

let appWindow;

function initWindow() {
	appWindow = new BrowserWindow({
		width: 1000,
		height: 800,
		title: 'Connect',
		autoHideMenuBar: true,
		icon: path.join(__dirname, 'assets/images/logo/logo-512x512.png'),
		webPreferences: {
			nodeIntegration: true,
		},
	});

	console.log(__dirname);

	// Electron Build Path
	appWindow.loadURL(
		url.format({
			pathname: path.join(__dirname, 'index.html'),
			protocol: 'file:',
			slashes: true,
		})
	);

	// Initialize the DevTools.
	//appWindow.webContents.openDevTools();

	appWindow.on('closed', function () {
		appWindow = null;
	});
}

app.on('ready', initWindow);

// Close when all windows are closed.
app.on('window-all-closed', function () {
	// On macOS specific close process
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', function () {
	if (win === null) {
		initWindow();
	}
});
