export const environment = {
	production: true,
	ecosystemBaseUrl: 'ecosystem.kominal.app',
	isElectron: true,
	commit: 'COMMIT',
	connectBaseUrl: 'connect.kominal.app',
};
