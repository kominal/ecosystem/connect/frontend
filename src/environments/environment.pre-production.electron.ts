export const environment = {
	production: true,
	ecosystemBaseUrl: 'pre-production.ecosystem.kominal.app',
	isElectron: true,
	commit: 'COMMIT',
	connectBaseUrl: 'pre-production.connect.kominal.app',
};
