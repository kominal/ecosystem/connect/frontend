export const environment = {
	production: true,
	ecosystemBaseUrl: 'pre-production.ecosystem.kominal.app',
	isElectron: false,
	commit: 'COMMIT',
};
