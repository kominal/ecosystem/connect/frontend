export const environment = {
	production: false,
	connectBaseUrl: 'connect.kominal.app',
	isElectron: false,
	commit: 'develop',
	ecosystemBaseUrl: 'ecosystem.kominal.app',
};
