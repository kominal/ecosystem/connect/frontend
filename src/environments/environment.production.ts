export const environment = {
	production: true,
	ecosystemBaseUrl: 'ecosystem.kominal.app',
	isElectron: false,
	commit: 'COMMIT',
};
