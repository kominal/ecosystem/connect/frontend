class SpeakingProcessor extends AudioWorkletProcessor {
	init = true;
	lastSpeakingTime;
	speaking = false;
	microphoneSensitivity = 0.5;
	lastMaxAmplitude;

	constructor(options) {
		super();
		this.port.onmessage = (e) => {
			this.microphoneSensitivity = e.data;
			this.reportSpeaking(this.lastMaxAmplitude > this.microphoneSensitivity);
		};
	}

	process(inputs, outputs, parameters) {
		if (inputs.length < 1 || inputs[0].length < 1) {
			return true;
		}

		const samples = inputs[0][0];
		let maxAmplitude = -Infinity;
		for (let i = 0; i < samples.length; i++) {
			if (samples[i] > maxAmplitude) {
				maxAmplitude = samples[i];
			}
		}
		this.lastMaxAmplitude = maxAmplitude;

		if (maxAmplitude > this.microphoneSensitivity) {
			this.lastSpeakingTime = Date.now();
			if (!this.speaking) {
				this.reportSpeaking(true);
			}
		} else if (this.init || (this.lastSpeakingTime != null && this.speaking && new Date().getTime() > this.lastSpeakingTime + 300)) {
			this.init = false;
			this.reportSpeaking(false);
		}
		return true;
	}

	reportSpeaking(speaking) {
		this.speaking = speaking;
		this.port.postMessage(speaking);
	}
}

registerProcessor('speaking-processor', SpeakingProcessor);
