import { DragDropModule } from '@angular/cdk/drag-drop';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import '@angular/compiler';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';
import { EcosystemClientModule } from '@kominal/ecosystem-angular-client';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './core/classes/material.module';
// Components
import { CallBarComponent } from './core/components/call-bar/call-bar.component';
import { CallControlsComponent } from './core/components/call-controls/call-controls.component';
import { CallDialogComponent } from './core/components/call-dialog/call-dialog.component';
import { ContextMenuComponent } from './core/components/context-menu/context-menu.component';
import { DecisionDialogComponent } from './core/components/decision-dialog/decision-dialog.component';
import { InfoBarComponent } from './core/components/info-bar/info-bar.component';
import { LoadingBarComponent } from './core/components/loading-bar/loading-bar.component';
import { ProfileDialogComponent } from './core/components/profile-dialog/profile-dialog.component';
import { ScreenShareCardComponent } from './core/components/screen-share-card/screen-share-card.component';
import { TextDialogComponent } from './core/components/text-dialog/text-dialog.component';
import { TextWithProfileDialogComponent } from './core/components/text-with-profile-dialog/text-with-profile-dialog.component';
// Interceptor
import { AuthenticationInterceptor } from './core/interceptors/authentication/authentication';
import { ServicesInterceptor } from './core/interceptors/services/services';
import { ActionsService } from './core/services/actions/actions.service';
import { GroupService } from './core/services/group/group.service';
import { LiveService } from './core/services/live/live.service';
// Services
import { LoadingService } from './core/services/loading/loading.service';
import { MessageService } from './core/services/message/message.service';
import { PreferencesService } from './core/services/preferences/preferences.service';
import { StorageService } from './core/services/storage/storage.service';
import { UserService } from './core/services/user/user.service';
import { SharedModule } from './core/shared.module';

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http, `./assets/i18n/`);
}

@NgModule({
	declarations: [
		AppComponent,
		CallBarComponent,
		ContextMenuComponent,
		LoadingBarComponent,
		ScreenShareCardComponent,
		InfoBarComponent,
		CallDialogComponent,
		TextDialogComponent,
		TextWithProfileDialogComponent,
		ProfileDialogComponent,
		DecisionDialogComponent,
		CallControlsComponent,
	],
	imports: [
		// General imports
		RouterModule,
		AppRoutingModule,
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		ReactiveFormsModule,
		FormsModule,
		MaterialModule,
		DragDropModule,
		SharedModule,
		EcosystemClientModule,
		// Config
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient],
			},
		}),
		ServiceWorkerModule.register('./connect-worker.js', { enabled: environment.production, registrationStrategy: 'registerImmediately' }),
	],
	providers: [
		GroupService,
		MessageService,
		StorageService,
		UserService,
		LoadingService,
		ActionsService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthenticationInterceptor,
			multi: true,
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: ServicesInterceptor,
			multi: true,
		},
		{
			provide: APP_INITIALIZER,
			useFactory: () => () => {},
			deps: [PreferencesService, LiveService],
			multi: true,
		},
	],
	bootstrap: [AppComponent],
})
export class AppModule {
	constructor(ecosystemClientModule: EcosystemClientModule) {
		ecosystemClientModule.ecosystemBaseUrl = environment.ecosystemBaseUrl;
	}
}
