import { Injectable } from '@angular/core';
import { Router, CanLoad, Route, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../../services/user/user.service';

@Injectable({
	providedIn: 'root',
})
export class UserGuard implements CanLoad {
	constructor(private userService: UserService, private router: Router) {}

	canLoad(route: Route, segments: UrlSegment[]): boolean | Observable<boolean> | Promise<boolean> {
		if (!this.userService.isLoggedIn()) {
			this.router.navigateByUrl('/authentication/login');
			return false;
		}

		return true;
	}
}
