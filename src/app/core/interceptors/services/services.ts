import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export const CONNECT_BASE_URL = (environment as any).connectBaseUrl ? `https://${(environment as any).connectBaseUrl}` : '';

@Injectable()
export class ServicesInterceptor implements HttpInterceptor {
	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		if (req.url.startsWith('/assets/') || req.url.startsWith('./assets/') || req.url.startsWith('http')) {
			return next.handle(req);
		}

		const modReq = req.clone({
			url: `${CONNECT_BASE_URL}${req.url}`,
		});

		return next.handle(modReq);
	}
}
