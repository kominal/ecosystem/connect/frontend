import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { UserService } from '../../services/user/user.service';

const PUBLIC = [
	'/user-service/register',
	'/user-service/login',
	'/user-service/reset',
	'/user-service/sessions',
	'/user-service/logout',
	'/assets/',
	'/user-service/refresh',
];

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
	constructor(private userService: UserService) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		for (const url of PUBLIC) {
			if (req.url.indexOf(url) > -1) {
				return next.handle(req);
			}
		}

		return from(this.userService.getJwt()).pipe(
			flatMap((jwt) => {
				req = req.clone({
					setHeaders: {
						Authorization: `Bearer ${jwt}`,
					},
				});
				return next.handle(req);
			})
		);
	}
}
