import { TestBed } from '@angular/core/testing';

import { PushHttpService } from './push.http.service';

describe('PushHttpService', () => {
	let service: PushHttpService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(Push.HttpService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});
