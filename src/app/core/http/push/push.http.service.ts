import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PushSubscription } from '@kominal/ecosystem-connect-models/pushsubscription/pushsubscription';

@Injectable({
	providedIn: 'root',
})
export class PushHttpService {
	private SERVICE = '/push-service';

	constructor(private httpClient: HttpClient) {}

	public getPushSubscriptions(): Promise<PushSubscription[]> {
		return this.httpClient.get<PushSubscription[]>(`${this.SERVICE}`).toPromise();
	}

	public registerPushSubscription(subscription: any): Promise<void> {
		return this.httpClient.post<void>(`${this.SERVICE}`, subscription).toPromise();
	}

	public deletePushSubscription(id: string): Promise<void> {
		return this.httpClient.delete<void>(`${this.SERVICE}/${id}`).toPromise();
	}
}
