import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root',
})
export class CorsHttpService {
	constructor(private httpClient: HttpClient) {}

	async get(uri: string): Promise<any> {
		return await this.httpClient
			.get(`https://cors.kominal.app/${encodeURIComponent(uri)}`, {
				responseType: 'arraybuffer',
				observe: 'response',
			})
			.toPromise();
	}
}
