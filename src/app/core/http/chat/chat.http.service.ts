import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';
import { Group } from '@kominal/ecosystem-connect-models/group/group';
import { Message } from '@kominal/ecosystem-connect-models/message/message';
import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';
import { StorageEncrypted } from '@kominal/ecosystem-connect-models/storage/storage.encrypted';
import { Attachment } from '@kominal/ecosystem-connect-models/message/attachment';
import { Link } from '@kominal/ecosystem-connect-models/message/link';
import { Status } from '@kominal/ecosystem-connect-models/status';

export type Batch = { overflow: boolean; messages: Message[] };
export type Single = { overflow: boolean; message: Message };

@Injectable({
	providedIn: 'root',
})
export class ChatHttpService {
	private SERVICE = '/chat-service';

	constructor(private httpClient: HttpClient) {}

	add(groupId: string, partnerId: string, groupKey: number[], profileId: string, profileKey: AESEncrypted): Promise<void> {
		return this.httpClient
			.post<void>(`${this.SERVICE}/group/add`, { groupId, partnerId, groupKey, profileId, profileKey })
			.toPromise();
	}

	rename(groupId: string, name: AESEncrypted): Promise<void> {
		return this.httpClient
			.post<void>(`${this.SERVICE}/group/${groupId}`, { name })
			.toPromise();
	}

	changeAvatar(groupId: string, avatar: AESEncrypted): Promise<void> {
		return this.httpClient
			.post<void>(`${this.SERVICE}/group/${groupId}`, { avatar })
			.toPromise();
	}

	changeProfile(groupId: string, profileId: string, profileKey: AESEncrypted): Promise<void> {
		return this.httpClient
			.post<void>(`${this.SERVICE}/group/${groupId}/membership`, { profileId, profileKey })
			.toPromise();
	}

	create(params: {
		type: 'DM' | 'GROUP';
		groupKey: number[];
		profileId: string;
		profileKey: AESEncrypted;
		name?: AESEncrypted;
		partnerId?: string;
		partnerGroupKey?: number[];
		partnerProfileId?: string;
		partnerProfileKey?: AESEncrypted;
	}): Promise<string> {
		return this.httpClient.post<string>(`${this.SERVICE}/group`, params).toPromise();
	}

	getGroups(): Promise<Group[]> {
		return this.httpClient.get<Group[]>(`${this.SERVICE}/groups`).toPromise();
	}

	getGroup(groupId: string): Promise<Group> {
		return this.httpClient.get<Group>(`${this.SERVICE}/group/${groupId}`).toPromise();
	}

	leave(groupId: string): Promise<void> {
		return this.httpClient.post<void>(`${this.SERVICE}/group/${groupId}/leave`, {}).toPromise();
	}

	getMembers(groupId: string): Promise<Membership[]> {
		return this.httpClient.get<Membership[]>(`${this.SERVICE}/group/${groupId}/members`).toPromise();
	}

	getMember(groupId: string, userId: string, lastUpdated?: number): Promise<Membership> {
		return this.httpClient
			.get<Membership>(`${this.SERVICE}/group/${groupId}/member/${userId}/${lastUpdated ? '/' + lastUpdated : ''}`)
			.toPromise();
	}

	remove(groupId: string): Promise<void> {
		return this.httpClient.delete<void>(`${this.SERVICE}/group/${groupId}/remove`).toPromise();
	}

	setRole(groupId: string, partnerId: string, role: string): Promise<void> {
		return this.httpClient.post<void>(`${this.SERVICE}/group/${groupId}/role/${partnerId}/${role}`, {}).toPromise();
	}

	kick(groupId: string, partnerId: string): Promise<void> {
		return this.httpClient.delete<void>(`${this.SERVICE}/group/${groupId}/${partnerId}`, {}).toPromise();
	}

	send(groupId: string, text: AESEncrypted | undefined, attachments: Attachment[], links: Link[]): Promise<{ id: string; time: number }> {
		return this.httpClient
			.post<{ id: string; time: number }>(`${this.SERVICE}/message`, { groupId, text, attachments, links })
			.toPromise();
	}

	get(ids: string[]): Promise<StorageEncrypted[]> {
		return this.httpClient.get<StorageEncrypted[]>(`${this.SERVICE}/storage?ids=${ids.join(',')}`).toPromise();
	}

	save(data: AESEncrypted): Promise<StorageEncrypted> {
		return this.httpClient.post<StorageEncrypted>(`${this.SERVICE}/storage`, data).toPromise();
	}

	saveToGridFS(data: AESEncrypted): Promise<StorageEncrypted> {
		const blob = new Blob([new Uint8Array(data.data)]);
		const formData = new FormData();
		formData.append('file', blob);
		return this.httpClient.post<StorageEncrypted>(`${this.SERVICE}/storage/gridfs?iv=${data.iv}`, formData).toPromise();
	}

	async getFromGridFS(id: string): Promise<StorageEncrypted> {
		const metaData = await this.httpClient.get<any>(`${this.SERVICE}/gridfs/metadata?id=${id}`).toPromise();
		const blob = await this.httpClient
			.get(`${this.SERVICE}/storage/gridfs?id=${id}`, { responseType: 'arraybuffer', observe: 'response' })
			.toPromise();
		if (!blob.body) {
			throw new Error('Response ist null');
		}
		return {
			id,
			created: metaData.created,
			userId: metaData.userId,
			data: { iv: metaData.iv, data: Array.from(new Uint8Array(blob.body)) },
		};
	}

	getMessage(messageId: string, currentBatchLatestMessageId?: string): Promise<Single> {
		return this.httpClient
			.get<Single>(`${this.SERVICE}/batch/message/${messageId}/${currentBatchLatestMessageId ? currentBatchLatestMessageId : ''}`)
			.toPromise();
	}

	getPreviousMessages(groupId: string, currentBatchEarliestMessageId?: string, previousBatchLatestMessageId?: string): Promise<Batch> {
		return this.httpClient
			.get<Batch>(
				`${this.SERVICE}/batch/messages/${groupId}/PREVIOUS/${currentBatchEarliestMessageId || ''}/${
					previousBatchLatestMessageId ? previousBatchLatestMessageId : ''
				}`
			)
			.toPromise();
	}

	getNextMessages(groupId: string, currentBatchLatestMessageId?: string): Promise<Batch> {
		return this.httpClient
			.get<Batch>(`${this.SERVICE}/batch/messages/${groupId}/NEXT/${currentBatchLatestMessageId ? currentBatchLatestMessageId : ''}`)
			.toPromise();
	}

	setStatus(status: Status): Promise<void> {
		return this.httpClient
			.post<void>(`${this.SERVICE}/status`, { status })
			.toPromise();
	}

	findRelay(): Promise<string> {
		return this.httpClient.get(`${this.SERVICE}/relay`, { responseType: 'text' }).toPromise();
	}
}
