import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class ElectronService {
	constructor() {}

	public getElectron() {
		return window['require']('electron');
	}

	public getPath() {
		return window['require']('path');
	}

	public getUnusedFilename() {
		return window['require']('unused-filename');
	}
}
