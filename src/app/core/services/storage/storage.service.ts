import { Injectable } from '@angular/core';
import { CryptoService } from '../crypto/crypto.service';
import { DexieService } from '../dexie/dexie.service';
import { StorageDecrypted } from '@kominal/ecosystem-connect-models/storage/storage.decrypted';
import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';
import { LogService } from '../log/log.service';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ChatHttpService } from '../../http/chat/chat.http.service';
import { StorageEncrypted } from '@kominal/ecosystem-connect-models/storage/storage.encrypted';

@Injectable({
	providedIn: 'root',
})
export class StorageService {
	private cache: Map<string, SafeUrl> = new Map();
	private storageTable: Dexie.Table<StorageEncrypted, string>;

	constructor(
		private cryptoService: CryptoService,
		private chatHttpService: ChatHttpService,
		private logService: LogService,
		private domSanitizer: DomSanitizer,
		dexieService: DexieService
	) {
		this.storageTable = dexieService.storage();
	}

	async save(data: string | ArrayBuffer, key: string): Promise<string> {
		const encrypted: AESEncrypted = await this.cryptoService.encryptAES(data, key);
		const { id, userId, created } = await this.chatHttpService.save(encrypted);
		await this.storageTable.add({ id, userId, created, data: encrypted });
		return id;
	}

	async saveToGridFS(data: string | ArrayBuffer, key: string): Promise<{ id: string; safeUrl: SafeUrl }> {
		const encrypted = await this.cryptoService.encryptAES(data, key);
		const { id, userId, created } = await this.chatHttpService.saveToGridFS(encrypted);
		const safeUrl = this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(new Blob([data])));
		this.cache.set(id, safeUrl);
		await this.storageTable.add({ id, userId, created, data: encrypted });
		return { id, safeUrl };
	}

	async getFromGridFS(id: string, key: string, type: 'string' | 'object' | 'arraybuffer'): Promise<StorageDecrypted | undefined> {
		try {
			let encrypted = await this.storageTable.get(id);

			if (encrypted == null) {
				encrypted = await this.chatHttpService.getFromGridFS(id);

				if (encrypted == null) {
					return undefined;
				}

				//To large -> To be fixed
				//await this.storageTable.put(encrypted);
			}

			return {
				id: encrypted.id,
				userId: encrypted.userId,
				created: new Date(encrypted.created),
				data: await this.cryptoService.decryptAES(encrypted.data, key, type),
			};
		} catch (e) {
			console.log(e);
			return undefined;
		}
	}

	async preload(ids: string[]): Promise<void> {
		const toBeLoaded = [];

		for (const id of ids) {
			if ((await this.storageTable.get(id)) == null) {
				toBeLoaded.push(id);
			}
		}

		if (toBeLoaded.length === 0) {
			return;
		}

		try {
			const encrypted = await this.chatHttpService.get(toBeLoaded);
			await this.storageTable.bulkPut(encrypted);
		} catch (e) {
			this.logService.handleError(e);
		}
	}

	async get(id: string, key: string, type: 'string' | 'object' | 'arraybuffer'): Promise<StorageDecrypted | undefined> {
		try {
			let encrypted = await this.storageTable.get(id);

			if (encrypted == null) {
				const response = await this.chatHttpService.get([id]);
				encrypted = response[0];

				if (encrypted == null) {
					return undefined;
				}

				await this.storageTable.put(encrypted);
			}

			return {
				id: encrypted.id,
				userId: encrypted.userId,
				created: new Date(encrypted.created),
				data: await this.cryptoService.decryptAES(encrypted.data, key, type),
			};
		} catch (error) {
			return undefined;
		}
	}

	async getFromGridFSAsSafeUrl(storageId: string, key: string): Promise<SafeUrl | undefined> {
		let safeUrl = this.cache.get(storageId);
		if (safeUrl) {
			return safeUrl;
		}
		const storage = await this.getFromGridFS(storageId, key, 'arraybuffer');
		if (storage) {
			safeUrl = this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(new Blob([storage.data])));
			this.cache.set(storageId, safeUrl);
			return safeUrl;
		}
	}
}
