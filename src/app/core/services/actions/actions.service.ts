import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Subject } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class ActionsService {
	public sidenav: MatSidenav | undefined;
	public onSetBoundary = new Subject<boolean>();
	private boundary: HTMLElement | undefined = document.body;

	openSidenav() {
		if (this.sidenav != null && !this.sidenav.opened) {
			this.sidenav.open();
		}
	}

	closeSidenav() {
		if (this.sidenav != null && this.sidenav.opened) {
			this.sidenav.close();
		}
	}

	setBoundary(boundary: HTMLElement | undefined) {
		this.boundary = boundary;
		this.onSetBoundary.next();
	}

	getBoundary(): HTMLElement {
		let temp = this.boundary;

		if (!temp || !this.isBoundaryValid(temp)) {
			temp = document.body;
		}

		return temp;
	}

	private isBoundaryValid(boundary: HTMLElement): boolean {
		return boundary != null && boundary.clientWidth !== 0 && boundary.clientHeight !== 0;
	}
}
