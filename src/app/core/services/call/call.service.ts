import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CallDialogComponent } from '../../components/call-dialog/call-dialog.component';
import { WebrtcService } from '../webrtc/webrtc.service';
import { GroupService } from '../group/group.service';
import { SoundService } from '../sound/sound.service';
import { Subject } from 'rxjs';
import { LogService } from '../log/log.service';
import { LiveService, Event } from '../live/live.service';
import { MediaService } from '../media/media.service';
import { GroupCacheService } from '../../cache/group/group-cache.service';
import { MembershipCacheService } from '../../cache/membership/membership-cache.service';
import { filter } from 'rxjs/operators';
import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';
import { Group } from '@kominal/ecosystem-connect-models/group/group';
import { ChatHttpService } from '../../http/chat/chat.http.service';

@Injectable({
	providedIn: 'root',
})
export class CallService {
	public calls = new Map<string, Membership[]>();

	public dialog: MatDialogRef<CallDialogComponent, any> | undefined;

	public group: Group | undefined;

	private events: Event[] = [];
	private processingEvents = false;

	public readonly onMembersChange = new Subject<void>();

	constructor(
		public webrtcService: WebrtcService,
		public groupService: GroupService,
		private membershipCacheService: MembershipCacheService,
		public soundService: SoundService,
		public logService: LogService,
		public liveService: LiveService,
		private chatHttpService: ChatHttpService,
		private mediaService: MediaService,
		private groupCacheService: GroupCacheService,
		private dialogService: MatDialog
	) {
		this.liveService.resetSubject.subscribe(() => this.onReset());
		this.liveService.eventSubject.pipe(filter(({ type }) => type.startsWith('connect.call'))).subscribe((event) => this.onEvent(event));
		this.groupService.groupUpdatedSubject.subscribe((group) => {
			if (this.group?.id === group.id) {
				this.group = group;
			}
		});
	}

	onReset() {
		this.calls.clear();
		this.events = [];
		this.leaveCall();
	}

	onEvent(event: Event) {
		this.events.push(event);
		if (!this.processingEvents) {
			this.processEvents();
		}
	}

	private async processEvents() {
		this.processingEvents = true;
		try {
			while (this.events.length > 0) {
				const event = this.events.shift();

				if (!event) {
					break;
				}

				const { type, content } = event;

				try {
					if (type === 'connect.call.members') {
						await this.onMembers(content);
					} else if (type === 'connect.call.signaling') {
						await this.onSignaling(content);
					} else if (type === 'connect.call.started') {
						await this.onCallStarted(content);
					} else if (type === 'connect.call.joined') {
						await this.onUserJoined(content);
					} else if (type === 'connect.call.left') {
						await this.onUserLeft(content);
					}
				} catch (e) {
					this.logService.handleError(e);
				}
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		this.processingEvents = false;
	}

	async onSignaling(packet: any) {
		try {
			if (this.group) {
				const membershipCaller = this.calls.get(this.group.id)?.find((m) => m.connectionId === packet.connectionId);
				if (membershipCaller) {
					await this.webrtcService.handleSignaling(this, membershipCaller, packet.signaling);
				} else {
					this.logService.error(`Could not find call membership. Dropping signaling packet from ${packet.sender}.`);
				}
			} else {
				this.logService.info(`Currently not in a call. Dropping signaling packet from ${packet.sender}.`);
			}
		} catch (e) {
			this.logService.handleError(e);
		}
	}

	async onMembers(body: { groupId: string; members: any[] }) {
		const { groupId, members } = body;
		const group = await this.groupCacheService.getGroup(groupId);
		if (group.keyDecrypted) {
			const membershipCallers: Membership[] = [];
			for (const { userId, connectionId } of members) {
				const membership = await this.membershipCacheService.getMembership(group.id, userId, group.keyDecrypted, 'EVENTUAL');
				if (membership) {
					membership.connectionId = connectionId;
					membershipCallers.push(membership);
				}
			}
			this.calls.set(groupId, membershipCallers);
		} else {
			this.calls.set(groupId, []);
		}
		await this.onMemberUpdate(groupId);
	}

	async onCallStarted(body: { groupId: string; connectionId: string; userId: string }) {
		await this.onUserJoined(body);
		this.notifyIncomingCall(body.groupId, body.userId);
	}

	async onUserJoined(body: { groupId: string; connectionId: string; userId: string }) {
		const { groupId, connectionId, userId } = body;
		let members = this.calls.get(groupId);
		if (!members) {
			members = [];
			this.calls.set(groupId, members);
		}
		if (!members.find((m) => m.connectionId === connectionId)) {
			const group = await this.groupCacheService.getGroup(groupId);
			if (group.keyDecrypted) {
				const membership = await this.membershipCacheService.getMembership(group.id, userId, group.keyDecrypted, 'EVENTUAL');
				if (membership) {
					members.push({ ...membership, connectionId });
					await this.onMemberUpdate(groupId);
				}
			}
		}
	}

	async onUserLeft(body: { groupId: string; connectionId: string; userId: string }) {
		const { groupId, connectionId, userId } = body;
		if (this.calls.has(groupId)) {
			const members = this.calls.get(groupId);
			if (members) {
				const member = members.find((m) => m.connectionId === connectionId);
				if (member) {
					members.splice(members.indexOf(member), 1);
				}
				if (members.length === 0) {
					this.calls.delete(groupId);
				}
			}
		}
		await this.onMemberUpdate(groupId);
	}

	async onMemberUpdate(groupId: string) {
		this.onMembersChange.next();
		if (this.group?.id === groupId) {
			const memberList = this.calls.get(this.group.id);

			if (memberList) {
				await this.webrtcService.updateMemberConnections(this, memberList);
			}
		}
	}

	async notifyIncomingCall(groupId: string, userId: string) {
		if (this.group?.id === groupId) {
			return;
		}

		const group = await this.groupCacheService.getGroup(groupId);

		if (!group.keyDecrypted) {
			throw 'KeyDecrypted is undefined.';
		}

		const membership = await this.membershipCacheService.getMembership(group.id, userId, group.keyDecrypted, 'EVENTUAL');

		if (this.dialog) {
			this.dialog.close(false);
			this.dialog = undefined;
		}

		const time = await this.soundService.playIncoming();

		this.dialog = this.dialogService.open(CallDialogComponent, {
			disableClose: true,
			data: { group, membership },
		});

		setTimeout(() => {
			if (this.soundService.stop(time)) {
				if (this.dialog) {
					this.dialog.close(false);
					this.dialog = undefined;
				}
			}
		}, 20000);

		const result = await this.dialog.afterClosed().toPromise();

		this.soundService.stop(time);

		if (result) {
			this.joinCall(group);
		}
	}

	async joinCall(group?: Group) {
		if (!group) {
			throw 'Group is undefined';
		}

		await this.leaveCall(true);

		this.group = group;

		this.liveService.publish('connect.call.join', { groupId: group.id });

		await this.mediaService.toggleMicrophone(true);
	}

	async leaveCall(rejoin?: boolean) {
		if (this.group) {
			await this.soundService.playDisconnected();
		}

		this.group = undefined;

		if (!rejoin) {
			this.liveService.publish('connect.call.leave');
		}

		this.webrtcService.leaveCall();
	}

	findRelay(): Promise<string> {
		return this.chatHttpService.findRelay();
	}

	get showCallbar(): boolean {
		return !!this.group;
	}

	getMembers(groupId: string): Membership[] {
		return this.calls.get(groupId) || [];
	}
}
