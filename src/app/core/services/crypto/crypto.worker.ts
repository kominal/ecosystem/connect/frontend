/// <reference lib="webworker" />
import { expose } from 'comlink';
import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';
import { hash } from 'bcryptjs';

const textDecoder = new TextDecoder();
const textEncoder = new TextEncoder();
const CryptoWorker = {
	async encryptAES(data: ArrayBuffer | string, key: string | CryptoKey): Promise<AESEncrypted> {
		if (typeof data === 'string') {
			data = textEncoder.encode(data);
		}

		const iv = crypto.getRandomValues(new Uint8Array(12));

		if (typeof key === 'string') {
			key = (await this.deriveAESKey(key, iv)) as CryptoKey;
		}

		const encrypted = await crypto.subtle.encrypt({ name: 'AES-GCM', iv }, key, data);

		return {
			iv: Array.from(new Uint8Array(iv).values()),
			data: Array.from(new Uint8Array(encrypted).values()),
		};
	},
	async decryptAES(
		encrypted: AESEncrypted,
		key: string | CryptoKey,
		type: 'string' | 'object' | 'arraybuffer' = 'arraybuffer'
	): Promise<any> {
		const iv = new Uint8Array(encrypted.iv);
		const alg = { name: 'AES-GCM', iv };

		if (typeof key === 'string') {
			key = (await this.deriveAESKey(key, iv)) as CryptoKey;
		}

		const buffer = await crypto.subtle.decrypt(alg, key, new Uint8Array(encrypted.data));

		if (type === 'arraybuffer') {
			return buffer;
		}

		const decoded = textDecoder.decode(buffer);

		if (type === 'string') {
			return decoded;
		} else if (type === 'object') {
			return JSON.parse(decoded);
		}
	},
	async deriveAESKey(baseKey: string, salt: ArrayBuffer): Promise<CryptoKey> {
		const encodedBaseKey = textEncoder.encode(baseKey);
		const params = { name: 'PBKDF2', hash: 'SHA-1', salt, iterations: 5000 };
		const alg = { name: 'AES-GCM', length: 256 };

		const cryptoKey = await crypto.subtle.importKey('raw', encodedBaseKey, 'PBKDF2', false, ['deriveKey']);
		return crypto.subtle.deriveKey(params, cryptoKey, alg, false, ['encrypt', 'decrypt']);
	},

	async encryptRSA(data: ArrayBuffer | string, key: ArrayBuffer | CryptoKey): Promise<number[]> {
		if (typeof data === 'string') {
			data = textEncoder.encode(data);
		}
		if (key instanceof ArrayBuffer) {
			key = (await this.importPublicRsaKey(key)) as CryptoKey;
		}
		return Array.from(new Uint8Array(await crypto.subtle.encrypt({ name: 'RSA-OAEP' }, key, data)));
	},
	async decryptRSA(data: number[], key: ArrayBuffer | CryptoKey): Promise<string> {
		if (key instanceof ArrayBuffer) {
			key = (await this.importPrivateRsaKey(key)) as CryptoKey;
		}
		const decrypted = await crypto.subtle.decrypt({ name: 'RSA-OAEP' }, key, new Uint8Array(data));
		return new TextDecoder().decode(decrypted);
	},
	async importPrivateRsaKey(privateKey: ArrayBuffer): Promise<CryptoKey> {
		return crypto.subtle.importKey('pkcs8', privateKey, { name: 'RSA-OAEP', hash: 'SHA-256' }, true, ['decrypt']);
	},
	async importPublicRsaKey(publicKey: ArrayBuffer): Promise<CryptoKey> {
		return crypto.subtle.importKey('spki', publicKey, { name: 'RSA-OAEP', hash: 'SHA-256' }, true, ['encrypt']);
	},
	async generateRsaKeypair(): Promise<{ privateKey: ArrayBuffer; publicKey: ArrayBuffer }> {
		const { privateKey, publicKey } = await crypto.subtle.generateKey(
			{
				name: 'RSA-OAEP',
				modulusLength: 4096,
				publicExponent: new Uint8Array([1, 0, 1]),
				hash: 'SHA-256',
			},
			true,
			['encrypt', 'decrypt']
		);

		if (!privateKey) {
			throw new Error('PrivateKey is undefined');
		} else if (!publicKey) {
			throw new Error('PublicKey is undefined');
		}

		return {
			privateKey: await crypto.subtle.exportKey('pkcs8', privateKey),
			publicKey: await crypto.subtle.exportKey('spki', publicKey),
		};
	},

	async hash(data: string | Promise<string>, salt: string): Promise<string> {
		return hash(await data, salt);
	},
};

expose(CryptoWorker);
