import { Injectable } from '@angular/core';
import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';
import { wrap } from 'comlink';

const cryptoWorker = wrap<CryptoWorker>(new Worker('./crypto.worker', { type: 'module' }));

@Injectable({
	providedIn: 'root',
})
export class CryptoService {
	/*
	 * AES Functions
	 */

	async deriveAESKey(baseKey: string, salt: ArrayBuffer): Promise<CryptoKey> {
		return cryptoWorker.deriveAESKey(baseKey, salt);
	}

	async encryptAES(data: ArrayBuffer | string, key: string | CryptoKey): Promise<AESEncrypted> {
		if (data == null) {
			throw new Error('Data cant be null');
		} else if ((data instanceof ArrayBuffer && data.byteLength === 0) || (data instanceof String && data.length === 0)) {
			throw new Error('Data length must be greater than 0');
		}

		return cryptoWorker.encryptAES(data, key);
	}

	async decryptAES(
		encrypted: AESEncrypted,
		key: string | CryptoKey,
		type: 'string' | 'object' | 'arraybuffer' = 'arraybuffer'
	): Promise<any> {
		if (encrypted == null) {
			throw new Error('Encrypted cant be null');
		} else if (encrypted.data == null) {
			throw new Error('Data cant be null');
		} else if (encrypted.iv == null) {
			throw new Error('Iv cant be null');
		} else if (encrypted.data.length === 0) {
			throw new Error('Data length must be greater than 0');
		} else if (encrypted.iv.length === 0) {
			throw new Error('Iv length must be greater than 0');
		} else if (key == null) {
			throw new Error('Type cant be null');
		} else if (type == null) {
			throw new Error('Type cant be null');
		}

		return cryptoWorker.decryptAES(encrypted, key, type);
	}

	/*
	 * RSA Functions
	 */

	async encryptRSA(data: ArrayBuffer | string, key: ArrayBuffer | CryptoKey): Promise<number[]> {
		return cryptoWorker.encryptRSA(data, key);
	}

	async decryptRSA(data: number[], key: ArrayBuffer | CryptoKey): Promise<string> {
		if (data == null) {
			throw new Error('Data cant be null');
		} else if (data.length === 0) {
			throw new Error('Data length must be greater than 0');
		}

		return cryptoWorker.decryptRSA(data, key);
	}

	async importPrivateRsaKey(privateKey: ArrayBuffer): Promise<CryptoKey> {
		if (privateKey.byteLength === 0) {
			throw new Error('PrivateKey length must be greater than 0');
		}

		return cryptoWorker.importPrivateRsaKey(privateKey);
	}

	async importPublicRsaKey(publicKey: ArrayBuffer): Promise<CryptoKey> {
		if (publicKey.byteLength === 0) {
			throw new Error('PublicKey length must be greater than 0');
		}

		return cryptoWorker.importPublicRsaKey(publicKey);
	}

	async generateRsaKeypair(): Promise<{ privateKey: ArrayBuffer; publicKey: ArrayBuffer }> {
		return cryptoWorker.generateRsaKeypair();
	}

	/*
	 * Hashing
	 */
	async hash(data: string, salt: string): Promise<string> {
		if (data == null) {
			throw new Error('Data cant be null');
		} else if (data.length === 0) {
			throw new Error('Data length must be greater than 0');
		} else if (salt.length === 0) {
			throw new Error('Salt length must be greater than 0');
		}

		return cryptoWorker.hash(data, salt);
	}

	/*
	 * Utils
	 */
	genRandomString(length: number): string {
		const buffer = crypto.getRandomValues(new Uint8Array(length / 2));
		return this.buf2hex(buffer);
	}

	private buf2hex(buffer: ArrayBuffer): string {
		return Array.prototype.map.call(new Uint8Array(buffer), (x) => ('00' + x.toString(16)).slice(-2)).join('');
	}
}

interface CryptoWorker {
	encryptAES(data: ArrayBuffer | string, key: string | CryptoKey): Promise<AESEncrypted>;
	decryptAES(encrypted: AESEncrypted, key: string | CryptoKey, type: 'string' | 'object' | 'arraybuffer'): Promise<any>;
	deriveAESKey(baseKey: string, salt: ArrayBuffer): Promise<CryptoKey>;

	encryptRSA(data: ArrayBuffer | string, key: ArrayBuffer | CryptoKey): Promise<number[]>;
	decryptRSA(data: number[], key: ArrayBuffer | CryptoKey): Promise<string>;
	importPrivateRsaKey(privateKey: ArrayBuffer): Promise<CryptoKey>;
	importPublicRsaKey(publicKey: ArrayBuffer): Promise<CryptoKey>;
	generateRsaKeypair(): Promise<{ privateKey: ArrayBuffer; publicKey: ArrayBuffer }>;

	hash(data: string | Promise<string>, salt: string): Promise<string>;
}
