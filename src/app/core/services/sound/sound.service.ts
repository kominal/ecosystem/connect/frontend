import { Injectable } from '@angular/core';
import { LogService } from '../log/log.service';

@Injectable({
	providedIn: 'root',
})
export class SoundService {
	private startTime: number | undefined;
	public audio = new Audio();

	constructor(private logService: LogService) {
		this.audio.addEventListener('canplaythrough', async (event) => {
			try {
				this.logService.debug(`Starting to play`);
				await this.audio.play();
			} catch (error) {
				this.logService.error(`Could not play audio. ${error}`);
			}
		});
	}

	public stop(startTime?: number): boolean {
		if (!startTime || startTime === this.startTime) {
			this.startTime = undefined;
			this.audio.src = '';
			this.audio.pause();
			return true;
		}

		return false;
	}

	private async play(sound: string, loop: boolean): Promise<number> {
		this.stop();
		this.startTime = new Date().getTime();

		this.audio.src = `assets/sounds/${sound}.mp3`;
		this.audio.loop = loop;

		this.logService.debug(`Start playing Sound(${sound}) Loop(${loop})`);

		return this.startTime;
	}

	public async playDeafen(): Promise<number> {
		return this.play('deafen', false);
	}

	public async playDisconnected(): Promise<number> {
		return this.play('disconnected', false);
	}

	public async playIncoming(): Promise<number> {
		return this.play('incoming', true);
	}

	public async playJoined(): Promise<number> {
		return this.play('joined', false);
	}

	public async playLeft(): Promise<number> {
		return this.play('left', false);
	}

	public async playMessage(): Promise<number> {
		return this.play('message', false);
	}

	public async playMute(): Promise<number> {
		return this.play('mute', false);
	}

	public async playOutgoing(): Promise<number> {
		return this.play('outgoing', true);
	}

	public async playUndeafen(): Promise<number> {
		return this.play('undeafen', false);
	}

	public async playUnmute(): Promise<number> {
		return this.play('unmute', false);
	}
}
