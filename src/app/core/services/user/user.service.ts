import { Injectable } from '@angular/core';
import { CryptoService } from '../crypto/crypto.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { LoadingService } from '../loading/loading.service';
import { DexieService } from '../dexie/dexie.service';
import { Subject } from 'rxjs';
import { Session } from '@kominal/ecosystem-models/session';
import { LogService, Level } from '../log/log.service';
import { ConnectError } from '../../classes/connect.error';
import { PasswordConfirmDialogComponent } from 'src/app/modules/connect/components/password-confirm-dialog/password-confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { SerialService } from '../serial/serial.service';
import { UserHttpService } from '@kominal/ecosystem-angular-client';

@Injectable({
	providedIn: 'root',
})
export class UserService {
	public loginSubject = new Subject();
	public logoutSubject = new Subject();

	public jwtExpires: number | undefined;
	private userId: string | undefined;
	private jwt: string | undefined;
	private masterEncryptionKey: string | undefined;
	private privateKey: ArrayBuffer | undefined;
	private publicKey: ArrayBuffer | undefined;

	constructor(
		private userHttpService: UserHttpService,
		private cryptoService: CryptoService,
		private snackBar: MatSnackBar,
		private translateService: TranslateService,
		private router: Router,
		private loadingService: LoadingService,
		private dexieService: DexieService,
		private logService: LogService,
		private serialService: SerialService,
		private matDialog: MatDialog
	) {}

	async getSessions(): Promise<Session[]> {
		try {
			const sessions = await this.userHttpService.getSessions();
			for (const session of sessions) {
				session.deviceDecrypted = await this.cryptoService.decryptAES(session.device, await this.getMasterEncryptionKey(), 'string');
			}
			return sessions;
		} catch (e) {
			this.logService.error('Could not load sessions.', 'error.sessions.listFailed');
			this.logService.handleError(e);
		}
		return [];
	}

	async register(username: string, displayname: string, password: string, offlineMode: boolean) {
		const subscribtion = this.loadingService.subscribe();
		try {
			const resetToken = this.cryptoService.genRandomString(32);
			const masterEncryptionKey = this.cryptoService.genRandomString(32);

			const profileKey = this.cryptoService.genRandomString(32);

			const [passwordHash, rsaKeypair, displaynameHash, encryptedDisplayname, encryptedProfileKey] = await Promise.all([
				this.cryptoService.hash(password, '$2a$10$tBAXtqLmT9pS6qkyn866u.'),
				this.cryptoService.generateRsaKeypair(),
				this.cryptoService.hash(displayname, '$2a$10$Y94cfvTmYVAjfC2NhMj4f.'),
				this.cryptoService.encryptAES(displayname, profileKey),
				this.cryptoService.encryptAES(profileKey, masterEncryptionKey),
			]);

			const [
				usernameHash,
				displaynameHash2x,
				passwordHash2x,
				resetTokenHash,
				encryptedPasswordHash,
				encryptedMasterEncryptionKey,
				encryptedPrivateKey,
				encryptedPublicKeyWithMasterEncryptionKey,
				encryptedProfileKeyWithDisplaynameHash,
				encryptedPublicKeyWithProfileKey,
			] = await Promise.all([
				this.cryptoService.hash(username, '$2a$10$2RNNkME2r9HR8YJPvAJdA.'),
				this.cryptoService.hash(displaynameHash, '$2a$10$6RqvepKpQd3VG7HFVtzqP.'),
				this.cryptoService.hash(passwordHash, '$2a$10$UuZnbPU2UZbNsWLqnLVyC.'),
				this.cryptoService.hash(resetToken, '$2a$10$H9tzRpMvZR8DxcVbd5s2x.'),
				this.cryptoService.encryptAES(passwordHash, resetToken),
				this.cryptoService.encryptAES(masterEncryptionKey, passwordHash),
				this.cryptoService.encryptAES(rsaKeypair.privateKey, masterEncryptionKey),
				this.cryptoService.encryptAES(new Uint8Array(rsaKeypair.publicKey), masterEncryptionKey),
				this.cryptoService.encryptAES(profileKey, displaynameHash),
				this.cryptoService.encryptAES(new Uint8Array(rsaKeypair.publicKey), profileKey),
			]);

			await this.userHttpService.register({
				username: usernameHash,
				displaynameHash: displaynameHash2x,
				password: passwordHash2x,
				resetToken: resetTokenHash,
				resetData: encryptedPasswordHash,
				masterEncryptionKey: encryptedMasterEncryptionKey,
				privateKey: encryptedPrivateKey,
				publicKey: encryptedPublicKeyWithMasterEncryptionKey,
				displayname: encryptedDisplayname,
				profilePublicKey: encryptedPublicKeyWithProfileKey,
				keyDisplayname: encryptedProfileKeyWithDisplaynameHash,
				keyMasterEncryptionKey: encryptedProfileKey,
			});

			this.snackBar.open(this.translateService.instant('simple.registerSuccessful'), '', { duration: 3000 });
			await this.login(username, password, offlineMode);
		} catch (e) {
			if (e.error?.error) {
				this.logService.error('Register failed.', e.error.error);
			} else {
				this.logService.error('Register failed.', 'error.register.failed');
			}
		}
		subscribtion.unsubscribe();
	}

	async login(username: string, password: string, offlineMode: boolean) {
		const subscribtion = this.loadingService.subscribe();
		try {
			const key = this.cryptoService.genRandomString(32);

			const [usernameHash, passwordHash] = await Promise.all([
				this.cryptoService.hash(username, '$2a$10$2RNNkME2r9HR8YJPvAJdA.'),
				this.cryptoService.hash(password, '$2a$10$tBAXtqLmT9pS6qkyn866u.'),
			]);

			const serverSidePassword = await this.cryptoService.hash(passwordHash, '$2a$10$UuZnbPU2UZbNsWLqnLVyC.');

			const masterEncryptionKeyEncrypted = await this.userHttpService.login(usernameHash, serverSidePassword);

			const masterEncryptionKey = await this.cryptoService.decryptAES(masterEncryptionKeyEncrypted, passwordHash, 'string');

			const [encryptedMetainfo, encryptedMasterEncryption, encryptedKey] = await Promise.all([
				this.cryptoService.encryptAES(`${navigator.platform}, ${navigator.appCodeName}`, masterEncryptionKey),
				this.cryptoService.encryptAES(masterEncryptionKey, key),
				this.cryptoService.hash(key, '$2a$10$9QUgTnFAB5tcCprTb2F6t.'),
			]);

			await this.userHttpService.createSession(
				encryptedKey,
				usernameHash,
				serverSidePassword,
				encryptedMasterEncryption,
				encryptedMetainfo
			);

			localStorage.setItem('sessionKey', key);

			await this.refresh();
			this.loginSubject.next();

			this.logService.info('Login successful.', 'simple.loginSuccessful');
			this.router.navigate(['/connect/home']);
		} catch (error) {
			this.logService.error('Login failed.', 'error.login.failed');
		}
		subscribtion.unsubscribe();
	}

	async refresh(): Promise<void> {
		await this.serialService.doOnce(async () => {
			if (!this.isLoggedIn()) {
				//await this.logout();
				return;
			}
			try {
				this.logService.info('Refreshing session...');

				const sessionKey = localStorage.getItem('sessionKey');
				if (!sessionKey) {
					throw new Error('SessionKey not found');
				}

				const response = await this.userHttpService.refreshToken(
					await this.cryptoService.hash(sessionKey, '$2a$10$9QUgTnFAB5tcCprTb2F6t.')
				);
				this.userId = response.userId;
				this.jwt = response.jwt;
				this.jwtExpires = response.jwtExpires;

				this.masterEncryptionKey = await this.cryptoService.decryptAES(response.masterEncryptionKey, sessionKey, 'string');

				if (!this.masterEncryptionKey) {
					throw new Error('MasterEncryptionKey is undefined');
				}

				const promise0 = await Promise.all([
					this.cryptoService.decryptAES(response.publicKey, this.masterEncryptionKey, 'arraybuffer'),
					this.cryptoService.decryptAES(response.privateKey, this.masterEncryptionKey, 'arraybuffer'),
				]);

				this.publicKey = promise0[0];
				this.privateKey = promise0[1];
			} catch (error) {
				this.logService.handleError(error);
			}
		});
	}

	async changePassword(password: string, newPassword: string) {
		const subscribtion = this.loadingService.subscribe();
		try {
			await this.refresh();
			const resetToken = this.cryptoService.genRandomString(32);

			if (!this.masterEncryptionKey) {
				throw new Error('MasterEncryptionKey is undefined');
			}

			const [passwordHash, newPasswordHash] = await Promise.all([
				this.cryptoService.hash(password, '$2a$10$tBAXtqLmT9pS6qkyn866u.'),
				this.cryptoService.hash(newPassword, '$2a$10$tBAXtqLmT9pS6qkyn866u.'),
			]);

			const [
				passwordHash2x,
				newPasswordHash2x,
				resetTokenHash,
				encryptedNewPasswordHash,
				encryptedMasterEncryptionKey,
			] = await Promise.all([
				this.cryptoService.hash(passwordHash, '$2a$10$UuZnbPU2UZbNsWLqnLVyC.'),
				this.cryptoService.hash(newPasswordHash, '$2a$10$UuZnbPU2UZbNsWLqnLVyC.'),
				this.cryptoService.hash(resetToken, '$2a$10$H9tzRpMvZR8DxcVbd5s2x.'),
				this.cryptoService.encryptAES(newPasswordHash, resetToken),
				this.cryptoService.encryptAES(this.masterEncryptionKey, newPasswordHash),
			]);

			await this.userHttpService.changePassword({
				password: passwordHash2x,
				newPassword: newPasswordHash2x,
				resetToken: resetTokenHash,
				resetData: encryptedNewPasswordHash,
				masterEncryptionKey: encryptedMasterEncryptionKey,
			});

			this.snackBar.open(this.translateService.instant('simple.changedPassword'), '', { duration: 3000 });
			this.logout();
		} catch (error) {
			this.logService.error('Failed to change password', 'error.password.failed');
		}
		subscribtion.unsubscribe();
	}

	async logout() {
		this.logService.info(`Logging you out...`, 'info.logout');

		try {
			const sessionKey = localStorage.getItem('sessionKey');
			localStorage.removeItem('sessionKey');

			this.router.navigate(['/core/home']);
			this.userId = this.masterEncryptionKey = this.jwt = this.jwtExpires = this.privateKey = this.publicKey = undefined;
			this.logoutSubject.next();
			await this.dexieService.delete();
			await this.dexieService.open();
			if (sessionKey) {
				await this.userHttpService.logout(sessionKey);
			}
		} catch (error) {
			this.logService.handleError(error);
		}
	}

	isLoggedIn(): boolean {
		return !!localStorage.getItem('sessionKey');
	}

	async getRemoteId(displayname: string): Promise<string> {
		try {
			let displaynameHash = await this.cryptoService.hash(displayname, '$2a$10$SH2kf3ULj8dYsh5YbqBfy.');
			displaynameHash = await this.cryptoService.hash(displaynameHash, '$2a$10$6RqvepKpQd3VG7HFVtzqP.');
			return await this.userHttpService.getId(displaynameHash);
		} catch (error) {
			throw new ConnectError(Level.INFO, `User (displayname='${displayname}') was not found.`, 'error.user.notfound');
		}
	}

	async getRemotePublicKey(displayname: string): Promise<ArrayBuffer> {
		try {
			const key = await this.cryptoService.hash(displayname, '$2a$10$SH2kf3ULj8dYsh5YbqBfy.');
			const displaynameHash = await this.cryptoService.hash(key, '$2a$10$6RqvepKpQd3VG7HFVtzqP.');
			const encryptedPublicKey = await this.userHttpService.getPublicKey(displaynameHash);
			return this.cryptoService.decryptAES(encryptedPublicKey, key, 'arraybuffer');
		} catch (error) {
			throw new ConnectError(Level.ERROR, `Public key for user '${displayname}' not found.`, 'error.publickey.notfound');
		}
	}

	async getMasterEncryptionKey(): Promise<string> {
		if (!this.masterEncryptionKey) {
			await this.refresh();

			if (!this.masterEncryptionKey) {
				throw new Error('MasterEncryptionKey is undefined');
			}
		}

		return this.masterEncryptionKey;
	}

	async getPrivateKey(): Promise<ArrayBuffer> {
		if (!this.privateKey) {
			await this.refresh();

			if (!this.privateKey) {
				throw new Error('PrivateKey is undefined');
			}
		}

		return this.privateKey;
	}

	async getPublicKey(): Promise<ArrayBuffer> {
		if (!this.publicKey) {
			await this.refresh();

			if (!this.publicKey) {
				throw new Error('PublicKey is undefined');
			}
		}

		return this.publicKey;
	}

	async getJwt(): Promise<string> {
		if (!this.jwt || !this.jwtExpires || this.jwtExpires - 150000 < Date.now()) {
			await this.refresh();

			if (!this.jwt || !this.jwtExpires || this.jwtExpires - 150000 < Date.now()) {
				throw new Error('Jwt is undefined');
			}
		}

		return this.jwt;
	}

	async removeSession(sessionId: string): Promise<void> {
		try {
			await this.userHttpService.removeSession(sessionId);
		} catch (error) {
			this.logService.handleError(error);
		}
	}

	async getUserId(): Promise<string> {
		if (!this.userId) {
			await this.refresh();

			if (!this.userId) {
				throw new Error('UserId is undefined');
			}
		}

		return this.userId;
	}

	async deleteAccount() {
		const password = await this.matDialog
			.open(PasswordConfirmDialogComponent, {
				data: { title: 'simple.deleteAccount', button: 'simple.confirm' },
			})
			.afterClosed()
			.toPromise<string | undefined>();
		if (password) {
			try {
				const serverSidePassword = await this.cryptoService.hash(
					await this.cryptoService.hash(password, '$2a$10$tBAXtqLmT9pS6qkyn866u.'),
					'$2a$10$UuZnbPU2UZbNsWLqnLVyC.'
				);

				await this.userHttpService.deleteAccount(serverSidePassword);

				this.snackBar.open(this.translateService.instant('simple.accountDeleted'), '', { duration: 3000 });
				this.logout();
			} catch (e) {
				this.logService.handleError(e);
			}
		}
	}
}
