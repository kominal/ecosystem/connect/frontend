import { Injectable } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { toSvg } from 'jdenticon';
import { LogService } from '../log/log.service';
import { hashSync } from 'bcryptjs';
import { CryptoService } from '../crypto/crypto.service';

@Injectable({
	providedIn: 'root',
})
export class AvatarService {
	private identiconCache = new Map<string, SafeUrl>();
	private avatarCache = new Map<string, SafeUrl>();

	private textDecoder = new TextDecoder();

	constructor(private domSanitizer: DomSanitizer, private logService: LogService, private cryptoService: CryptoService) {}

	getIdenticon(identifier: string): SafeUrl {
		let identicion = this.identiconCache.get(identifier);
		if (!identicion) {
			identicion = this.domSanitizer.bypassSecurityTrustUrl(
				URL.createObjectURL(new Blob([toSvg(identifier, 40, { padding: 0.1 })], { type: 'image/svg+xml' }))
			);
			this.identiconCache.set(identifier, identicion);
		}
		return identicion;
	}

	async getAvatar(identifier: string, avatar?: ArrayBuffer): Promise<SafeUrl> {
		if (avatar) {
			try {
				const hash = await this.cryptoService.hash(this.textDecoder.decode(avatar), '$2a$10$PNsHA3nWPhJc8KyN7kGe3.');
				const cachedAvatar = this.avatarCache.get(hash);
				if (cachedAvatar) {
					return cachedAvatar;
				} else {
					const safeUrl = this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(new Blob([avatar])));
					this.avatarCache.set(hash, safeUrl);
					return safeUrl;
				}
			} catch (e) {
				this.logService.handleError(e);
			}
		}
		return this.getIdenticon(identifier);
	}
}
