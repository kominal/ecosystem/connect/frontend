import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class SerialService {
	private transactions = new Map<string, Promise<any>>();

	async doOnce<T>(producer: () => Promise<T>, key?: string): Promise<T> {
		const transactionKey = key || producer.toString();
		try {
			if (this.transactions.has(transactionKey)) {
				return await this.transactions.get(transactionKey);
			} else {
				const transaction = producer();
				this.transactions.set(transactionKey, transaction);
				return await transaction;
			}
		} catch (e) {
			throw e;
		} finally {
			this.transactions.delete(transactionKey);
		}
	}
}
