import { Injectable } from '@angular/core';
import Dexie from 'dexie';
import { StorageEncrypted } from '@kominal/ecosystem-connect-models/storage/storage.encrypted';
import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';
import { Message } from '@kominal/ecosystem-connect-models/message/message';
import { Group } from '@kominal/ecosystem-connect-models/group/group';
import { Profile } from '@kominal/ecosystem-models/profile';

@Injectable({
	providedIn: 'root',
})
export class DexieService extends Dexie {
	constructor() {
		super('connect');
		this.version(1).stores({
			group: 'id, lastActivity',
			message: 'id, groupId, time',
			storage: 'id',
			volume: 'id',
			preferences: 'id',
			profile: 'id, userId, displaynameHash, [userId+active]',
			membership: 'id, groupId, [userId+groupId]',
		});
	}

	group(): Dexie.Table<Group, string> {
		return this.table('group');
	}

	message(): Dexie.Table<Message, string> {
		return this.table('message');
	}

	storage(): Dexie.Table<StorageEncrypted, string> {
		return this.table('storage');
	}

	volume(): Dexie.Table<any, string> {
		return this.table('volume');
	}

	preferences(): Dexie.Table<any, string> {
		return this.table('preferences');
	}

	profile(): Dexie.Table<Profile, string> {
		return this.table('profile');
	}

	membership(): Dexie.Table<Membership, string> {
		return this.table('membership');
	}
}
