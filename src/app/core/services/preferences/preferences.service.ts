import { Injectable } from '@angular/core';
import { DexieService } from '../dexie/dexie.service';
import { TranslateService } from '@ngx-translate/core';
import { MediaService } from '../media/media.service';

@Injectable({
	providedIn: 'root',
})
export class PreferencesService {
	private preferences = this.dexieService.preferences();

	constructor(private dexieService: DexieService, private translateService: TranslateService, private mediaService: MediaService) {
		this.updateTheme();
		this.updateLanguage();
		this.updateMicrophoneSensitivity();
	}

	async setTheme(darkTheme: boolean) {
		await this.preferences.put({ id: 'darkTheme', value: darkTheme });
		await this.updateTheme();
	}

	async isDarkTheme(): Promise<boolean> {
		const localPreference = await this.preferences.get('darkTheme');
		if (localPreference) {
			return localPreference.value;
		} else {
			return matchMedia('(prefers-color-scheme: dark)').matches;
		}
	}

	async updateTheme() {
		if (await this.isDarkTheme()) {
			document.body.classList.add('dark-theme');
		} else {
			document.body.classList.remove('dark-theme');
		}
	}

	async setLanguage(language: string) {
		await this.preferences.put({ id: 'language', value: language });
		await this.updateLanguage();
	}

	async updateLanguage() {
		this.translateService.setDefaultLang('en');

		const localPreference = await this.preferences.get('language');
		if (localPreference) {
			this.translateService.use(localPreference.value);
		} else {
			this.translateService.use(this.translateService.getBrowserLang());
		}
	}

	async setMicrophoneSensitivity(value: number) {
		await this.preferences.put({ id: 'microphoneSensitivity', value });
		this.updateMicrophoneSensitivity();
	}

	async getMicrophoneSensitivity() {
		return (await this.preferences.get({ id: 'microphoneSensitivity' }))?.value || 0.5;
	}

	async updateMicrophoneSensitivity() {
		this.mediaService.microphoneSensitivitySubject.next(await this.getMicrophoneSensitivity());
	}

	async notificationEnabled(): Promise<boolean> {
		const localPreference = await this.preferences.get('notifications');
		if (localPreference) {
			return localPreference.value;
		}
		return false;
	}

	async toggleNotifications(enabled: boolean) {
		await this.preferences.put({ id: 'notifications', value: enabled });
	}
}
