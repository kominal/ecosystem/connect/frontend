import { Injectable } from '@angular/core';
import { Subject, PartialObserver } from 'rxjs';
import { LogService } from '../log/log.service';

@Injectable({
	providedIn: 'root',
})
export class LoadingService {
	readonly loading = new Subject<void>();

	constructor(private logService: LogService) {}

	get isLoading(): boolean {
		return this.loading.observers.length > 0;
	}

	subscribe(observer?: PartialObserver<void>) {
		return this.loading.subscribe(observer);
	}

	async doWhileLoading<T>(producer: () => Promise<T>): Promise<T> {
		const subscribtion = this.loading.subscribe();
		try {
			return await producer();
		} catch (e) {
			throw e;
		} finally {
			subscribtion.unsubscribe();
		}
	}

	async doWithErrorHandlingWhileLoading<T>(producer: () => Promise<T>): Promise<T | undefined> {
		try {
			return await this.doWhileLoading(producer);
		} catch (e) {
			this.logService.handleError(e);
		}
	}
}
