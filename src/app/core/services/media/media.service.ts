import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LogService } from '../log/log.service';
import { environment } from 'src/environments/environment';
import { ElectronService } from '../electron/electron.service';
import { MatDialog } from '@angular/material/dialog';
import { SelectScreenDialogComponent } from 'src/app/modules/connect/components/select-screen-dialog/select-screen-dialog.component';
import { SoundService } from '../sound/sound.service';

@Injectable({
	providedIn: 'root',
})
export class MediaService {
	public videoStreamSubject = new BehaviorSubject<MediaStream[]>([]);
	public screenStreamSubject = new BehaviorSubject<MediaStream[]>([]);
	public microphoneStreamSubject = new BehaviorSubject<MediaStream | undefined>(undefined);

	public microphoneMutedSubject = new BehaviorSubject<boolean>(true);
	public speakerMutedSubject = new BehaviorSubject<boolean>(false);

	private audioContext?: AudioContext;

	public speakingSubject = new BehaviorSubject<boolean>(false);
	private speakingCache = false;
	public microphoneSensitivitySubject = new BehaviorSubject<number>(0.5);

	private microphoneStreamDestination?: MediaStreamAudioDestinationNode;

	constructor(
		public logService: LogService,
		public electronService: ElectronService,
		private matDialog: MatDialog,
		private soundService: SoundService
	) {
		this.speakingSubject.subscribe((speaking) => {
			this.microphoneStreamDestination?.stream.getTracks().forEach((t) => (t.enabled = speaking));
		});
	}

	private stopStream(streamSubject: BehaviorSubject<MediaStream | undefined>) {
		const stream = streamSubject.value;
		if (stream) {
			stream.getTracks().forEach((t) => t.stop());
			streamSubject.next(undefined);
		}
	}

	stopMicrophoneStream() {
		this.stopStream(this.microphoneStreamSubject);
		this.speakingSubject.next(false);
		this.microphoneMutedSubject.next(true);
		this.microphoneStreamDestination = undefined;
		this.audioContext?.close();
		this.audioContext = undefined;
	}

	stopVideoStream(stream: MediaStream) {
		stream.getTracks().forEach((t) => t.stop());
		const streams = this.videoStreamSubject.value;
		if (streams.includes(stream)) {
			streams.splice(streams.indexOf(stream), 1);
			this.videoStreamSubject.next(streams);
		}
	}

	stopScreenStream(stream: MediaStream) {
		stream.getTracks().forEach((t) => t.stop());
		const streams = this.screenStreamSubject.value;
		if (streams.includes(stream)) {
			streams.splice(streams.indexOf(stream), 1);
			this.screenStreamSubject.next(streams);
		}
	}

	stopAllScreenStreams() {
		[...this.screenStreamSubject.value].forEach((mediaStream) => this.stopScreenStream(mediaStream));
	}

	stopAllVideoStreams() {
		[...this.videoStreamSubject.value].forEach((mediaStream) => this.stopVideoStream(mediaStream));
	}

	async toggleMicrophone(ignoreIfEnabled?: true) {
		const microphoneStream = this.microphoneStreamSubject.value;
		if (microphoneStream) {
			if (ignoreIfEnabled) {
				return;
			}

			this.microphoneMutedSubject.next(!this.microphoneMutedSubject.value);
			microphoneStream.getTracks().forEach((t) => (t.enabled = !this.microphoneMutedSubject.value));
			if (this.microphoneMutedSubject.value) {
				this.speakingSubject.next(false);
				await this.soundService.playMute();
			} else {
				this.speakingSubject.next(this.speakingCache);
				await this.soundService.playUnmute();
			}
		} else {
			try {
				const mediaStream = await navigator.mediaDevices.getUserMedia({
					audio: { echoCancellation: true, latency: 0, noiseSuppression: true, autoGainControl: true },
				});

				if (this.audioContext) {
					return;
				}
				this.audioContext = new AudioContext();

				const audioSourceNode = this.audioContext.createMediaStreamSource(mediaStream);
				audioSourceNode.connect(this.createFilterNode(this.audioContext));
				audioSourceNode.connect(await this.createAnalyserNode(this.audioContext));

				this.microphoneStreamDestination = this.audioContext.createMediaStreamDestination();

				audioSourceNode.connect(this.microphoneStreamDestination);

				this.microphoneStreamSubject.next(this.microphoneStreamDestination.stream);
				this.microphoneMutedSubject.next(false);
			} catch (e) {
				this.logService.info('No microphone found or permission to access micrphone was not granted.', 'error.microphone');
			}
		}
	}

	private createFilterNode(audioContext: AudioContext): BiquadFilterNode {
		const LOW_FREQ_CUT = 85;
		const HIGH_FREQ_CUT = 1000;
		const bandPassMiddleFrequency = (HIGH_FREQ_CUT - LOW_FREQ_CUT) / 2 + LOW_FREQ_CUT;
		const Q = bandPassMiddleFrequency / (HIGH_FREQ_CUT - LOW_FREQ_CUT);
		const filter = audioContext.createBiquadFilter();
		filter.type = 'bandpass';
		filter.frequency.value = bandPassMiddleFrequency;
		filter.Q.value = Q;
		return filter;
	}

	private async createAnalyserNode(audioContext: AudioContext): Promise<AnalyserNode> {
		await audioContext.audioWorklet.addModule('assets/speaking-processor.js');

		const analyserNode = audioContext.createAnalyser();

		const speakingProcessor = new AudioWorkletNode(audioContext, 'speaking-processor');
		speakingProcessor.port.onmessage = (event) => {
			this.speakingCache = Boolean(event.data);
			if (!this.microphoneMutedSubject.value) {
				this.speakingSubject.next(this.speakingCache);
			}
		};
		speakingProcessor.port.postMessage(this.microphoneSensitivitySubject.value);
		speakingProcessor.connect(audioContext.createMediaStreamDestination());

		this.microphoneSensitivitySubject.subscribe((microphoneSensitivity) => speakingProcessor.port.postMessage(microphoneSensitivity));

		analyserNode.connect(speakingProcessor);
		return analyserNode;
	}

	async toggleSpeaker() {
		this.speakerMutedSubject.next(!this.speakerMutedSubject.value);
		if (this.speakerMutedSubject.value) {
			await this.soundService.playDeafen();
		} else {
			await this.soundService.playUndeafen();
		}
	}

	async startScreenStream() {
		try {
			let stream: MediaStream | undefined;
			if (environment.isElectron) {
				const electron = this.electronService.getElectron();
				const desktopCapturer = electron.desktopCapturer;
				const sources: any[] = await desktopCapturer.getSources({ types: ['window', 'screen'] });
				if (sources.length === 0) {
					this.logService.info('No Electron screen scource found.', 'error.screen');
					return;
				}

				const sourceId = await this.matDialog.open(SelectScreenDialogComponent, { data: sources }).afterClosed().toPromise();

				if (!sourceId) {
					return;
				}

				stream = await navigator.mediaDevices.getUserMedia({
					audio: false,
					video: {
						mandatory: {
							chromeMediaSource: 'desktop',
							chromeMediaSourceId: sourceId,
						},
					},
				} as any);
			} else {
				stream = await (navigator.mediaDevices as any).getDisplayMedia({
					video: { frameRate: 60, resizeMode: 'none', cursor: 'always' },
				});
			}
			if (stream) {
				const streams = this.screenStreamSubject.value;
				streams.push(stream);
				this.screenStreamSubject.next(streams);
			}
		} catch (e) {
			this.logService.info('No screen found or permission to access screen was not granted.', 'error.screen');
		}
	}

	async startVideoStream() {
		try {
			const stream = await navigator.mediaDevices.getUserMedia({ video: { frameRate: 60, resizeMode: 'none' } });
			const streams = this.screenStreamSubject.value;
			streams.push(stream);
			this.videoStreamSubject.next(streams);
		} catch (e) {
			this.logService.info('No video found or permission to access video was not granted.', 'error.video');
		}
	}
}
