import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CryptoService } from '../crypto/crypto.service';
import { UserService } from '../user/user.service';
import { DexieService } from '../dexie/dexie.service';
import { Group } from '@kominal/ecosystem-connect-models/group/group';
import { Router } from '@angular/router';
import { LeaveGroupDialogComponent } from 'src/app/modules/connect/components/leave-group-dialog/leave-group-dialog.component';
import { LogService } from '../log/log.service';
import { Subject } from 'rxjs';
import { TextDialogComponent } from '../../components/text-dialog/text-dialog.component';
import { LoadingService } from '../loading/loading.service';
import { TextWithProfileDialogComponent } from '../../components/text-with-profile-dialog/text-with-profile-dialog.component';
import { ProfileService } from '../profile/profile.service';
import { ChatHttpService } from '../../http/chat/chat.http.service';
import { GroupCacheService } from '../../cache/group/group-cache.service';
import { ProfileCacheService } from '../../cache/profile/profile-cache.service';
import { Profile } from '@kominal/ecosystem-models/profile';

@Injectable({
	providedIn: 'root',
})
export class GroupService {
	readonly groupUpdatedSubject = new Subject<Group>();
	readonly membershipUpdatedSubject = new Subject<void>();

	private preferencesTable: Dexie.Table<any, string>;

	constructor(
		private chatHttpService: ChatHttpService,
		private matDialog: MatDialog,
		private cryptoService: CryptoService,
		private userService: UserService,
		private router: Router,
		private logService: LogService,
		private loadingService: LoadingService,
		private groupCacheService: GroupCacheService,
		private profileService: ProfileService,
		private profileCacheService: ProfileCacheService,
		dexieService: DexieService
	) {
		this.preferencesTable = dexieService.preferences();
	}

	private async leave(group: string): Promise<void> {
		await this.chatHttpService.leave(group);
		//await this.refreshGroups();
	}

	remove(group: string): Promise<void> {
		return this.chatHttpService.remove(group);
	}

	setRole(groupId: string, partnerId: string, role: string): Promise<void> {
		return this.chatHttpService.setRole(groupId, partnerId, role);
	}

	kick(groupId: string, partnerId: string): Promise<void> {
		return this.chatHttpService.kick(groupId, partnerId);
	}

	async showOpenDirectMessageDialog(): Promise<void> {
		await this.profileCacheService.loadUserProfiles();
		const profiles = this.profileCacheService.userProfilesSubject.value;

		if (profiles.length === 0) {
			this.logService.info('No profiles available.', 'error.profiles.missing');
			return;
		}

		const response = await this.matDialog
			.open(TextWithProfileDialogComponent, {
				data: { title: 'simple.addDirectMessage', label: 'simple.displayname', button: 'simple.addDirectMessage', profiles },
			})
			.afterClosed()
			.toPromise();

		if (response && response.text && response.profile) {
			const displayname: string = response.text;
			const profile: Profile = response.profile;

			try {
				const groupKey = this.cryptoService.genRandomString(32);

				const partnerProfile = await this.profileService.getProfileByDisplayname(displayname);

				if (!partnerProfile) {
					throw new Error('error.profile.notfound');
				}

				if (!profile.keyDecrypted) {
					throw new Error('error.profileKey.notfound');
				}

				if (!partnerProfile.publicKeyDecrypted || !partnerProfile.keyDecrypted) {
					throw new Error('error.profileKey.notfound');
				}

				const groupId = await this.chatHttpService.create({
					type: 'DM',
					groupKey: await this.cryptoService.encryptRSA(groupKey, await this.userService.getPublicKey()),
					profileId: profile.id,
					profileKey: await this.cryptoService.encryptAES(profile.keyDecrypted, groupKey),
					partnerId: partnerProfile.userId,
					partnerGroupKey: await this.cryptoService.encryptRSA(groupKey, partnerProfile.publicKeyDecrypted),
					partnerProfileId: partnerProfile.id,
					partnerProfileKey: await this.cryptoService.encryptAES(partnerProfile.keyDecrypted, groupKey),
				});

				//await this.refreshGroups();
				this.router.navigate([`/connect/group/${groupId}`]);
			} catch (error) {
				this.logService.handleError(error);
			}
		}
	}

	async showCreateGroupDialog(): Promise<void> {
		try {
			await this.profileCacheService.loadUserProfiles();
			const profiles = this.profileCacheService.userProfilesSubject.value;

			if (profiles.length === 0) {
				this.logService.info('No profiles available.', 'error.profiles.missing');
				return;
			}

			const response = await this.matDialog
				.open(TextWithProfileDialogComponent, {
					data: { title: 'simple.addGroup', label: 'simple.name', button: 'simple.addGroup', profiles },
				})
				.afterClosed()
				.toPromise();

			if (response && response.text && response.profile) {
				const { profile, text } = response;

				const groupKey = this.cryptoService.genRandomString(32);

				const groupId = await this.chatHttpService.create({
					type: 'GROUP',
					groupKey: await this.cryptoService.encryptRSA(groupKey, await this.userService.getPublicKey()),
					profileId: profile.id,
					profileKey: await this.cryptoService.encryptAES(profile.keyDecrypted, groupKey),
					name: await this.cryptoService.encryptAES(text, groupKey),
				});

				this.router.navigate([`/connect/group/${groupId}`]);
			}
		} catch (error) {
			this.logService.handleError(error);
		}
	}

	async showAddMemberDialog(group: Group): Promise<void> {
		try {
			if (!group.keyDecrypted) {
				return;
			}
			const displayname = await this.matDialog
				.open(TextDialogComponent, { data: { title: 'simple.addMember', label: 'simple.displayname', button: 'simple.addMember' } })
				.afterClosed()
				.toPromise<string | undefined>();

			if (displayname) {
				const partnerProfile = await this.profileService.getProfileByDisplayname(displayname);

				if (!partnerProfile) {
					throw new Error('error.profile.notfound');
				}

				if (!partnerProfile.publicKeyDecrypted || !partnerProfile.keyDecrypted) {
					throw new Error('error.profileKey.notfound');
				}

				await this.chatHttpService.add(
					group.id,
					partnerProfile.userId,
					await this.cryptoService.encryptRSA(group.keyDecrypted, partnerProfile.publicKeyDecrypted),
					partnerProfile.id,
					await this.cryptoService.encryptAES(partnerProfile.keyDecrypted, group.keyDecrypted)
				);
			}
		} catch (error) {
			this.logService.handleError(error);
		}
	}

	async showRenameDialog(group?: Group): Promise<void> {
		if (!group) {
			throw 'Group not defined';
		}

		const name = await this.matDialog
			.open(TextDialogComponent, {
				data: { title: 'simple.renameGroup', label: 'simple.name', button: 'simple.save', text: group.nameDecrypted },
			})
			.afterClosed()
			.toPromise<string | undefined>();

		if (name && name !== group.nameDecrypted && group.keyDecrypted) {
			const subscribtion = this.loadingService.subscribe();
			try {
				await this.chatHttpService.rename(group.id, await this.cryptoService.encryptAES(name, group.keyDecrypted));
				//await this.onGroupUpdated(group.id);
			} catch (error) {
				this.logService.handleError(error);
			}
			subscribtion.unsubscribe();
		}
	}

	async showLeaveGroupDialog(group?: Group): Promise<void> {
		if (!group) {
			throw 'Group is undefined';
		}

		const response = await this.matDialog
			.open(LeaveGroupDialogComponent, { data: group?.left != null || group?.type !== 'GROUP' })
			.afterClosed()
			.toPromise();

		if (response === 'leave_group') {
			const groupId = group.id;
			await this.leave(groupId);
			this.logService.info(`Group (groupId=${groupId}) was left.`, 'simple.groupLeft');
		} else if (response === 'delete_group') {
			await this.remove(group.id);
			//this.handleGroupRemoved(this.group.id);
		}
	}

	async changeAvatar(group: Group, image?: ArrayBuffer) {
		if (!image || !group.keyDecrypted || group.role !== 'ADMIN' || group.type !== 'GROUP' || group.left != null) {
			return;
		}

		const avatar = await this.cryptoService.encryptAES(image, group.keyDecrypted);
		await this.chatHttpService.changeAvatar(group.id, avatar);
		//await this.onGroupUpdated(group.id);
	}

	async handleGroupLeft(groupId: string) {
		//await this.refreshGroups();
	}

	async handleGroupRemoved(groupId: string) {
		//await this.refreshGroups();
		if ((await this.preferencesTable.get('selectedGroup')).value === groupId) {
			await this.preferencesTable.delete('selectedGroup');
		}
		this.router.navigate(['/connect/home']);
	}

	async markActivity(groupId: string, type?: 'MESSAGE') {
		const activityAt = Date.now();
		const group = await this.groupCacheService.getGroup(groupId);
		group.activityAt = activityAt;
	}

	async changeProfile(group: Group, profile: Profile) {
		if (group.keyDecrypted && profile.keyDecrypted) {
			await this.chatHttpService.changeProfile(
				group.id,
				profile.id,
				await this.cryptoService.encryptAES(profile.keyDecrypted, group.keyDecrypted)
			);
		}
	}
}
