import { Injectable } from '@angular/core';
import { LoadingService } from '../loading/loading.service';
import { LogService } from '../log/log.service';
import { MatDialog } from '@angular/material/dialog';
import { TextDialogComponent } from '../../components/text-dialog/text-dialog.component';
import { CryptoService } from '../crypto/crypto.service';
import { UserService } from '../user/user.service';
import { Subject } from 'rxjs';
import { AvatarService } from '../avatar/avatar.service';
import { ProfileCryptoService } from '../../crypto/profile/profile-crypto.service';
import { UserHttpService } from '@kominal/ecosystem-angular-client';
import { Profile } from '@kominal/ecosystem-models/profile';

@Injectable({
	providedIn: 'root',
})
export class ProfileService {
	public profiles: Profile[] = [];

	public readonly profileUpdatedSubject = new Subject<string>();

	constructor(
		private userHttpService: UserHttpService,
		private loadingService: LoadingService,
		private logService: LogService,
		private matDialog: MatDialog,
		private cryptoService: CryptoService,
		private avatarService: AvatarService,
		private userService: UserService,
		private profileCryptoService: ProfileCryptoService
	) {}

	async createProfile() {
		const displayname = await this.matDialog
			.open(TextDialogComponent, { data: { title: 'simple.addProfile', label: 'simple.displayname', button: 'simple.addProfile' } })
			.afterClosed()
			.toPromise<string | undefined>();

		if (!displayname) {
			return;
		}

		this.loadingService.doWithErrorHandlingWhileLoading(async () => {
			const profileKey = this.cryptoService.genRandomString(32);
			const displaynameHash = await this.cryptoService.hash(displayname, '$2a$10$Y94cfvTmYVAjfC2NhMj4f.');

			await this.userHttpService.createProfile(
				await this.cryptoService.encryptAES(displayname, profileKey),
				await this.cryptoService.encryptAES(await this.userService.getPublicKey(), profileKey),
				await this.cryptoService.hash(displaynameHash, '$2a$10$6RqvepKpQd3VG7HFVtzqP.'),
				await this.cryptoService.encryptAES(profileKey, displaynameHash),
				await this.cryptoService.encryptAES(profileKey, await this.userService.getMasterEncryptionKey())
			);
			this.logService.info(`Profile created.`, 'info.profile.created');
			//await this.refreshUserProfiles();
		});
	}

	async deleteProfile(id: string): Promise<void> {
		this.loadingService.doWithErrorHandlingWhileLoading(async () => {
			await this.userHttpService.deleteProfile(id);
			this.logService.info(`Profile ${id} deleted.`, 'info.profile.deleted');
			//await this.refreshUserProfiles();
		});
	}

	async renameProfile(profile: Profile): Promise<void> {
		try {
			const newDisplayname = await this.matDialog
				.open(TextDialogComponent, {
					data: { title: 'simple.editProfile', label: 'simple.displayname', button: 'simple.save', text: profile.displaynameDecrypted },
				})
				.afterClosed()
				.toPromise<string | undefined>();

			if (newDisplayname) {
				const subscription = this.loadingService.subscribe();
				try {
					const { id, keyDecrypted } = profile;
					if (keyDecrypted) {
						const displayname = await this.cryptoService.encryptAES(newDisplayname, keyDecrypted);
						const displaynameHash = await this.cryptoService.hash(newDisplayname, '$2a$10$Y94cfvTmYVAjfC2NhMj4f.');
						const keyDisplayname = await this.cryptoService.encryptAES(keyDecrypted, newDisplayname);

						this.logService.info(`Profile ${id} updated.`, 'info.profile.renamed');
						profile.displaynameDecrypted = newDisplayname;
						profile.displaynameHash = displaynameHash;

						await this.userHttpService.updateProfile(id, {
							displayname,
							displaynameHash,
							keyDisplayname,
						});
					}
				} catch (e) {
					this.logService.handleError(e);
				}
				subscription.unsubscribe();
			}
		} catch (e) {
			this.logService.handleError(e);
		}
	}

	async changeAvatar(profile: Profile, image: ArrayBuffer) {
		this.loadingService.doWithErrorHandlingWhileLoading(async () => {
			const { id, keyDecrypted } = profile;
			if (keyDecrypted) {
				const avatar = await this.cryptoService.encryptAES(image, keyDecrypted);
				profile.avatarDecrypted = await this.avatarService.getAvatar(id, image);
				await this.userHttpService.updateProfile(id, {
					avatar,
				});
				this.logService.info(`Profile ${id} updated.`, 'info.profile.avatarChanged');
			}
		});
	}

	async getProfileByDisplayname(displayname: string): Promise<Profile> {
		return this.loadingService.doWhileLoading<Profile>(async () => {
			const displaynameHash = await this.cryptoService.hash(displayname, '$2a$10$Y94cfvTmYVAjfC2NhMj4f.');
			const displaynameSecondHash = await this.cryptoService.hash(displaynameHash, '$2a$10$6RqvepKpQd3VG7HFVtzqP.');
			const profile = await this.userHttpService.getByDisplaynameHash(displaynameSecondHash);
			await this.profileCryptoService.setProfileState(profile, 'DECRYPTED', { displaynameHash });
			return profile;
		});
	}
}
