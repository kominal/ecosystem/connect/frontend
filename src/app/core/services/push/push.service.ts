import { Injectable } from '@angular/core';
import { SwPush } from '@angular/service-worker';
import { UserService } from '../user/user.service';
import { PushSubscription } from '@kominal/ecosystem-connect-models/pushsubscription/pushsubscription';
import { CryptoService } from '../crypto/crypto.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { LogService } from '../log/log.service';
import { PushHttpService } from '../../http/push/push.http.service';
import { formatFullTime } from '../../classes/time.util';

@Injectable({
	providedIn: 'root',
})
export class PushService {
	private readonly VAPID_PUBLIC_KEY = 'BNNuHuZEWJzRaEZ_CtRVlPRwQ2Gp107cCc_TNq0ltC-CHen1kXU30hnfX6AulVhQlymcRzpVCElF330Qj2vnJ0M';

	constructor(
		private pushHttpService: PushHttpService,
		private swPush: SwPush,
		private userService: UserService,
		private cryptoService: CryptoService,
		private snackBar: MatSnackBar,
		private logService: LogService,
		private translateService: TranslateService
	) {}

	async subscribeToNotifications() {
		try {
			const token = await this.swPush.requestSubscription({
				serverPublicKey: this.VAPID_PUBLIC_KEY,
			});
			if (token != null) {
				await this.pushHttpService.registerPushSubscription({
					token,
					device: await this.cryptoService.encryptAES(
						`${navigator.platform}, ${navigator.appCodeName}`,
						await this.userService.getMasterEncryptionKey()
					),
				});
				console.log(token);
				this.snackBar.open(this.translateService.instant('notifications.enabled'), '', { duration: 3000 });
			}
		} catch (err) {
			this.snackBar.open(this.translateService.instant('notifications.couldNotEnable') + err, '', { duration: 3000 });
			console.error('Could not subscribe due to:', err);
		}
	}

	async remove(id: string) {
		try {
			await this.pushHttpService.deletePushSubscription(id);
		} catch (error) {}
	}

	async getSubscriptions(): Promise<PushSubscription[]> {
		let pushSubscriptions: PushSubscription[] = [];
		try {
			pushSubscriptions = await this.pushHttpService.getPushSubscriptions();

			const masterEncryptionKey = await this.userService.getMasterEncryptionKey();
			await Promise.all(pushSubscriptions.map((pushSubscription) => this.setState(pushSubscription, 'RESOLVED', masterEncryptionKey)));
		} catch (e) {
			this.logService.error('Could not load push subscriptions.', 'error.pushsubscription.listFailed');
			this.logService.handleError(e);
		}
		return pushSubscriptions;
	}

	async setState(pushSubscription: PushSubscription, state: 'RESOLVED', key: string): Promise<boolean> {
		let modified = false;
		const { device, deviceDecrypted, created } = pushSubscription;
		if (state === 'RESOLVED') {
			if (device && !deviceDecrypted) {
				pushSubscription.deviceDecrypted = await this.cryptoService.decryptAES(device, key, 'string');
				modified = true;
			}
			if (!pushSubscription.createdString) {
				pushSubscription.createdString = formatFullTime(created);
				modified = true;
			}
		}
		return modified;
	}
}
