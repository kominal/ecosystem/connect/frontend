import { Injectable, NgZone, ChangeDetectorRef, Injector, ElementRef } from '@angular/core';
import { StorageService } from '../storage/storage.service';
import { GroupService } from '../group/group.service';
import { LogService } from '../log/log.service';
import { CorsHttpService } from '../../http/cors.http/cors.http.service';
import { PreferencesService } from '../preferences/preferences.service';
import { UserService } from '../user/user.service';
import { Router } from '@angular/router';
import { SoundService } from '../sound/sound.service';
import { ChatHttpService } from '../../http/chat/chat.http.service';
import { MessageTree } from '../../classes/message-tree';
import { DomSanitizer } from '@angular/platform-browser';
import { GroupCacheService } from '../../cache/group/group-cache.service';
import { MembershipCacheService } from '../../cache/membership/membership-cache.service';
import { MessageCryptoService } from '../../crypto/message/message-crypto.service';
import { MessageCacheService } from '../../cache/message/message-cache.service';
import { Message } from '@kominal/ecosystem-connect-models/message/message';
import { Group } from '@kominal/ecosystem-connect-models/group/group';
import { Attachment } from '@kominal/ecosystem-connect-models/message/attachment';
import { Link } from '@kominal/ecosystem-connect-models/message/link';

@Injectable({
	providedIn: 'root',
})
export class MessageService {
	private textDecoder = new TextDecoder();

	constructor(
		private storageService: StorageService,
		private groupService: GroupService,
		private groupCacheService: GroupCacheService,
		private logService: LogService,
		private chatHttpService: ChatHttpService,
		private preferencesService: PreferencesService,
		private userService: UserService,
		private router: Router,
		private ngZone: NgZone,
		private soundService: SoundService,
		private corsHttpService: CorsHttpService,
		private domSanitizer: DomSanitizer,
		private membershipCacheService: MembershipCacheService,
		private messageCryptoService: MessageCryptoService,
		private messageCacheService: MessageCacheService,
		private injector: Injector
	) {}

	async send(group: Group, message: Message): Promise<void> {
		const { groupId, userId, text, attachments, links } = message;

		const attachmentsEncrypted = attachments.map((a) => {
			const { storageId, name, type, size, imageStorageId, imageSize } = a;
			return { storageId, name, type, size, imageStorageId, imageSize };
		});

		const linksEncrypted = links.map((a) => {
			const { url, site, title, description, imageStorageId, imageSize } = a;
			return { url, site, title, description, imageStorageId, imageSize };
		});

		try {
			const { id, time } = await this.chatHttpService.send(group.id, text, attachmentsEncrypted, linksEncrypted);

			message.id = id;
			message.time = time;
			message.status = 'SENT';

			await this.messageCacheService.addMessage({
				id,
				groupId,
				userId,
				text,
				time,
				status: 'SENT',
				attachments: attachmentsEncrypted,
				links: linksEncrypted,
			});
		} catch (error) {
			this.logService.handleError(error);
			message.status = 'FAILED';
		}
	}

	public async resolveAttachments(message: Message, files: File[]) {
		const attachments: Attachment[] = [];

		for (const file of files) {
			const { name, size, type } = file;

			const arrayBuffer = await new Response(file).arrayBuffer();

			attachments.push({
				nameDecrypted: name,
				sizeDecrypted: this.convertToHumanReadableSize(size),
				typeDecrypted: type,
				image: type.startsWith('image/')
					? this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(new Blob([arrayBuffer])))
					: undefined,
				arrayBuffer,
			});
		}

		message.attachments = attachments;
	}

	public async resolveLinks(group: Group, message: Message) {
		if (!group?.keyDecrypted) {
			throw new Error('Key is undefined.');
		}

		if (!message.textDecrypted) {
			return;
		}

		const links: Link[] = [];

		const urls = new RegExp(/(https?:\/\/[^\s]+)/g);

		while (true) {
			const match = urls.exec(message.textDecrypted);
			if (match == null) {
				break;
			}
			for (const urlDecrypted of match) {
				if (!links.find((link) => link.urlDecrypted === urlDecrypted)) {
					const link: Link = {
						urlDecrypted,
						site: undefined,
						description: undefined,
						title: undefined,
						imageStorageId: undefined,
					};
					try {
						const youtubeUrl = new RegExp(
							/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/gi
						).exec(urlDecrypted);

						if (youtubeUrl) {
							const request = await this.corsHttpService.get(`https://i.ytimg.com/vi/${youtubeUrl[1]}/maxresdefault.jpg`);
							if (!request.body) {
								throw new Error('Body is undefined');
							}
							link.image = this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(new Blob([link.image])));
						} else {
							const request = await this.corsHttpService.get(urlDecrypted);

							if (!request.body) {
								throw new Error('Body is undefined');
							}

							if (request.headers.get('content-type')?.startsWith('image/')) {
								const image = new Image();
								image.src = URL.createObjectURL(new Blob([request.body]));

								const imageSizeDecrypted = await new Promise<{ height: number; width: number }>((resolve) => {
									image.onload = () => {
										const { height, width } = image;
										resolve({ height, width });
									};
								});

								const { id, safeUrl } = await this.storageService.saveToGridFS(request.body, group.keyDecrypted);
								link.imageStorageId = id;
								link.image = safeUrl;
								link.imageSizeDecrypted = imageSizeDecrypted;
							} else {
								const body = this.textDecoder.decode(request.body).replace(/\r?\n|\r/g, ' ');

								const doc = new DOMParser().parseFromString(body, 'text/html');

								link.siteDecrypted = this.extractSiteName(doc);
								link.titleDecrypted = this.extractTitle(doc);
								link.descriptionDecrypted = this.extractDescription(doc);

								const imageUrl = this.extractImageUrl(doc);
								if (imageUrl) {
									try {
										const request = await this.corsHttpService.get(imageUrl);
										if (!request.body) {
											throw new Error('Body is undefined');
										}

										const image = new Image();
										image.src = URL.createObjectURL(new Blob([request.body]));

										const imageSizeDecrypted = await new Promise<{ height: number; width: number }>((resolve) => {
											image.onload = () => {
												const { height, width } = image;
												resolve({ height, width });
											};
										});

										const { id, safeUrl } = await this.storageService.saveToGridFS(request.body, group.keyDecrypted);
										link.imageStorageId = id;
										link.image = safeUrl;
										link.imageSizeDecrypted = imageSizeDecrypted;
									} catch (e) {
										this.logService.handleError(e);
									}
								}
							}
						}
					} catch (e) {
						this.logService.handleError(e);
					}
					links.push(link);
				}
			}
		}

		message.links = links;
	}

	private extractSiteName(doc: Document): string | undefined {
		const ogSiteName = this.getMeta('og:site_name', doc);
		if (ogSiteName != null) {
			return ogSiteName;
		}
		return undefined;
	}

	private extractTitle(doc: Document): string | undefined {
		const ogTitle = this.getMeta('og:title', doc);
		if (ogTitle != null) {
			return ogTitle;
		}
		const title = doc.getElementsByTagName('title');
		if (title != null) {
			return title.item(0)?.innerText;
		}
		return undefined;
	}

	private extractDescription(doc: Document) {
		const ogDescription = this.getMeta('og:description', doc);
		if (ogDescription != null) {
			return ogDescription;
		}
		const description = this.getMeta('description', doc);
		if (description != null) {
			return description;
		}
		return undefined;
	}

	private extractImageUrl(doc: Document): string | undefined {
		const ogImage = this.getMeta('og:image', doc);
		if (ogImage != null) {
			return ogImage;
		}
		return undefined;
	}

	private getMeta(name: string, doc: Document): string | undefined {
		const meta = doc.getElementsByTagName('meta');

		for (let i = 0; i < meta.length; i++) {
			const item = meta.item(i);
			if (item != null) {
				if (item.attributes.getNamedItem('property')?.value === name || item.attributes.getNamedItem('property')?.name === name) {
					return item.content;
				}
			}
		}
	}

	private convertToHumanReadableSize(size: number): string {
		let i = -1;
		const byteUnits = [' KB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
		do {
			size = size / 1000;
			i++;
		} while (size > 1000);

		return Math.max(size, 0.1).toFixed(1) + byteUnits[i];
	}
	private messageTrees: Map<string, MessageTree> = new Map();

	async getMessageTree(group: Group, cdr: ChangeDetectorRef, viewport: ElementRef<HTMLElement>) {
		let messageTree = this.messageTrees.get(group.id);
		if (!messageTree) {
			messageTree = new MessageTree(this.injector, group, await this.userService.getUserId(), cdr, viewport);
			this.messageTrees.set(group.id, messageTree);
		} else {
			messageTree.init(cdr, viewport);
		}
		return messageTree;
	}
}
