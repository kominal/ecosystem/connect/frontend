import { Injectable } from '@angular/core';
import { Status } from '@kominal/ecosystem-connect-models/status';
import { Subject } from 'rxjs';
import { LiveService, Event } from '../live/live.service';
import { filter } from 'rxjs/operators';
import { ChatHttpService } from '../../http/chat/chat.http.service';

@Injectable({
	providedIn: 'root',
})
export class StatusService {
	public statusSubject = new Subject<{ userId: string; status: Status }>();

	public status: Map<string, 'AWAY_FROM_KEYBOARD' | 'DO_NOT_DISTURB' | 'ONLINE'> = new Map();

	constructor(private chatHttpService: ChatHttpService, private liveService: LiveService) {
		this.liveService.resetSubject.subscribe(() => this.onReset());
		this.liveService.eventSubject.pipe(filter(({ type }) => type === 'connect.status.changed')).subscribe((event) => this.onEvent(event));
	}

	onReset() {
		for (const userId of this.status.keys()) {
			this.statusSubject.next({ userId, status: Status.OFFLINE });
		}
		this.status.clear();
	}

	onEvent({ content }: Event) {
		const { userId, status } = content;

		if (status === 'OFFLINE') {
			this.status.delete(userId);
		} else {
			this.status.set(userId, status);
		}

		this.statusSubject.next({ userId, status });
	}

	setStatus(status: Status) {
		this.chatHttpService.setStatus(status);
	}
}
