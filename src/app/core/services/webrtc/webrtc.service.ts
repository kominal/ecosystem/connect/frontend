import { Injectable } from '@angular/core';
import { CallService } from '../call/call.service';
import { CryptoService } from '../crypto/crypto.service';
import { LogService } from '../log/log.service';
import { SoundService } from '../sound/sound.service';
import { BaseMemberConnection, FocusedMediaStream } from '../../classes/memberconnections/baseMemberConnection';
import { DexieService } from '../dexie/dexie.service';
import { LiveService } from '../live/live.service';
import { MediaService } from '../media/media.service';
import { RemoteMemberConnection } from '../../classes/memberconnections/remoteMemberConnection';
import { LocalMemberConnection } from '../../classes/memberconnections/localMemberConnection';
import { BehaviorSubject } from 'rxjs';
import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';
import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';

@Injectable({
	providedIn: 'root',
})
export class WebrtcService {
	public memberConnections: BaseMemberConnection[] = [];
	public onMediaStreamAdd = new BehaviorSubject<FocusedMediaStream | undefined>(undefined);

	constructor(
		private cryptoService: CryptoService,
		private liveService: LiveService,
		public dexieService: DexieService,
		public logService: LogService,
		public mediaService: MediaService,
		private soundService: SoundService
	) {}

	async sendSignaling(callService: CallService, connectionId: string, type: string, data: string) {
		if (!callService.group?.keyDecrypted) {
			this.logService.info(`Currently not in a call. Dropping signaling packet for ${connectionId}.`);
			return;
		}

		this.logService.debug(`${connectionId} | Sending ${type}...`);

		this.liveService.publish('connect.call.signaling', {
			targetConnectionId: connectionId,
			signaling: await this.cryptoService.encryptAES(JSON.stringify({ type, data }), callService.group.keyDecrypted),
		});
	}

	async handleSignaling(callService: CallService, membershipCaller: Membership, encryptedData: AESEncrypted) {
		if (!callService.group?.keyDecrypted) {
			throw new Error('Key is undefined.');
		}

		const { type, data } = await this.cryptoService.decryptAES(encryptedData, callService.group.keyDecrypted, 'object');

		await (await this.getOrCreateMemberConnection(callService, membershipCaller, true, false)).handleSignaling(type, data);
	}

	async updateMemberConnections(callService: CallService, members: Membership[]) {
		let polite = false;
		for (const member of members) {
			const local = this.liveService.socket.ioSocket.id === member.connectionId;
			if (local) {
				polite = true;
			}

			await this.getOrCreateMemberConnection(callService, member, polite, local);
		}
		for (const memberConnection of [...this.memberConnections]) {
			if (!members.find((member) => member.connectionId === memberConnection.membershipCaller.connectionId)) {
				await this.soundService.playLeft();
				await memberConnection.destroy();
			}
		}
	}

	async getOrCreateMemberConnection(
		callService: CallService,
		membershipCaller: Membership,
		polite: boolean,
		local: boolean
	): Promise<BaseMemberConnection> {
		let memberConnection = this.memberConnections.find((m) => m.membershipCaller.connectionId === membershipCaller.connectionId);

		if (!memberConnection) {
			if (!callService.group) {
				throw new Error('Group is undefined');
			}

			const member = callService.getMembers(callService.group.id).find((m) => m.connectionId === membershipCaller.connectionId);

			if (!member) {
				throw new Error('Member is undefined');
			}

			if (local) {
				memberConnection = new LocalMemberConnection(membershipCaller, this.cryptoService, callService, this.mediaService);
			} else {
				memberConnection = new RemoteMemberConnection(membershipCaller, this.cryptoService, callService, polite, this.mediaService);
			}

			await memberConnection.setup();
			this.memberConnections.push(memberConnection);
		}

		return memberConnection;
	}

	leaveCall() {
		for (const memberConnection of [...this.memberConnections]) {
			memberConnection.destroy();
		}
		this.memberConnections = [];
		this.mediaService.stopMicrophoneStream();
		this.mediaService.stopAllScreenStreams();
		this.mediaService.stopAllVideoStreams();
	}
}
