import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { ConnectError } from '../../classes/connect.error';

export enum Level {
	UNKNOWN = 'UNKNOWN',
	DEBUG = 'DEBUG',
	INFO = 'INFO',
	LOG = 'LOG',
	WARN = 'WARN',
	ERROR = 'ERROR',
}

@Injectable({
	providedIn: 'root',
})
export class LogService {
	public issues = new Map<string, string>();

	constructor(private snackBar: MatSnackBar, private translateService: TranslateService) {}

	dismiss(type: string) {
		this.issues.delete(type);
	}

	private print(level: Level, consoleMessage: string, uiMessage?: string) {
		if (!level) {
			level = Level.UNKNOWN;
		}
		let color = '#ffff00';
		if (level === Level.DEBUG) {
			color = '#00b0ff';
		} else if (level === Level.INFO) {
			color = '#bada55';
		} else if (level === Level.WARN) {
			color = '#ffd700';
		} else if (level === Level.ERROR) {
			color = '#f44336';
		}

		const message = `${new Date().toLocaleTimeString(undefined, { hour12: false })} | [%c${level}%c] ${consoleMessage}`;

		if (level === Level.DEBUG) {
			console.debug(message, `color: ${color}`, '');
		} else if (level === Level.INFO) {
			console.info(message, `color: ${color}`, '');
		} else if (level === Level.WARN) {
			console.warn(message, `color: ${color}`, '');
		} else if (level === Level.ERROR) {
			console.error(message, `color: ${color}`, '');
		} else {
			console.log(message, `color: ${color}`, '');
		}
		if (uiMessage) {
			this.snackBar.open(this.translateService.instant(uiMessage), '', { duration: 3000 });
		}
	}

	public debug(consoleMessage: string, uiMessage?: string) {
		this.print(Level.DEBUG, consoleMessage, uiMessage);
	}

	public info(consoleMessage: string, uiMessage?: string) {
		this.print(Level.INFO, consoleMessage, uiMessage);
	}

	public log(consoleMessage: string, uiMessage?: string) {
		this.print(Level.LOG, consoleMessage, uiMessage);
	}

	public warn(consoleMessage: string, uiMessage?: string) {
		this.print(Level.WARN, consoleMessage, uiMessage);
	}

	public error(consoleMessage: string, uiMessage?: string) {
		this.print(Level.ERROR, consoleMessage, uiMessage);
	}
	public handleError(error: any) {
		if (error instanceof ConnectError) {
			this.print(error.level, error.message, error.uiMessage);
		} else {
			if (error.error?.error) {
				this.error(this.translateService.instant(error.error.error), error.error.error);
			} else if (error.message) {
				this.print(Level.ERROR, error.message);
				console.trace(error);
			} else {
				this.print(Level.ERROR, 'Unknown error.');
				console.trace(error);
			}
		}
	}
}
