import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Socket } from 'ngx-socket-io';
import { Subject } from 'rxjs';
import { UserService } from '../user/user.service';

export type Event = { type: string; content: any };

@Injectable({
	providedIn: 'root',
})
export class LiveService {
	private ECOSYSTEM_BASE_URL = this.getEcosystemBaseUrl();

	getEcosystemBaseUrl(): string {
		const { ecosystemBaseUrl } = environment as any;

		if (ecosystemBaseUrl) {
			return `${ecosystemBaseUrl}`;
		}

		return window.location.hostname.includes('test') ? 'ecosystem-test.kominal.com' : 'ecosystem.kominal.com';
	}

	public socketStatus: 'CONNECTED' | 'DISCONNECTED' = 'CONNECTED';

	public socket = new Socket({
		url: `wss://${this.ECOSYSTEM_BASE_URL}`,
		options: {
			transports: ['websocket'],
			path: `/live-service/socket.io`,
		},
	});

	readonly resetSubject = new Subject<void>();

	readonly eventSubject = new Subject<Event>();

	constructor(private userService: UserService) {
		this.socket.fromEvent<string>('connect_timeout').subscribe(() => this.onDisconnect());
		this.socket.fromEvent<string>('connect_error').subscribe(() => this.onDisconnect());
		this.socket.fromEvent<string>('reconnect_error').subscribe(() => this.onDisconnect());
		this.socket.fromEvent<string>('reconnect_failed').subscribe(() => this.onDisconnect());
		this.socket.fromEvent<string>('disconnect').subscribe(() => this.onDisconnect());
		this.socket.fromEvent<string>('connect').subscribe(() => {
			this.socketStatus = 'CONNECTED';
			this.registerSocket();
		});

		this.socket.fromEvent<Event>('EVENT').subscribe((event) => this.eventSubject.next(event));

		this.userService.loginSubject.subscribe(() => this.registerSocket());
		this.userService.logoutSubject.subscribe(() => this.unregisterSocket());
	}

	private onDisconnect() {
		this.socketStatus = 'DISCONNECTED';
		this.resetSubject.next();
	}

	private async registerSocket() {
		if (this.userService.isLoggedIn()) {
			this.socket.emit('REGISTER', await this.userService.getJwt());
		}
	}

	private unregisterSocket() {
		this.socket.emit('UNREGISTER');
		this.resetSubject.next();
	}

	publish(type: string, content?: any) {
		this.socket.emit('PUBLISH', { type, content });
	}
}
