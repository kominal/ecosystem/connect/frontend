export function formatFullTime(time: number) {
	const date = new Date(time);

	const year = date.getFullYear();
	let month = '' + (date.getMonth() + 1);
	let day = '' + date.getDate();
	let hours = '' + date.getHours();
	let minutes = '' + date.getMinutes();

	if (month.length < 2) {
		month = '0' + month;
	}
	if (day.length < 2) {
		day = '0' + day;
	}
	if (hours.length < 2) {
		hours = '0' + hours;
	}
	if (minutes.length < 2) {
		minutes = '0' + minutes;
	}

	return [year, month, day].join('-') + ' ' + [hours, minutes].join(':');
}
