import { WebrtcService } from '../../services/webrtc/webrtc.service';
import { CallService } from '../../services/call/call.service';
import { LogService } from '../../services/log/log.service';
import { CryptoService } from '../../services/crypto/crypto.service';
import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';
import { MediaService } from '../../services/media/media.service';
import { BaseMemberConnection, MediaStreamType } from './baseMemberConnection';

export class LocalMemberConnection extends BaseMemberConnection {
	public webrtcService: WebrtcService;
	public logService: LogService;

	constructor(
		membershipCaller: Membership,
		public cryptoService: CryptoService,
		public callService: CallService,
		private mediaService: MediaService
	) {
		super(membershipCaller);
		this.webrtcService = callService.webrtcService;
		this.logService = this.webrtcService.logService;
		this.logService.info(`${this.membershipCaller.connectionId} | Creating LocalMemberConnection...`);
		this.subscribeToSubjects();
	}

	private subscribeToSubjects() {
		this.screenStreamSubscription = this.mediaService.screenStreamSubject.subscribe((mediaStreams) => {
			this.updateMediaStreams(MediaStreamType.SCREEN, mediaStreams);
		});

		this.videoStreamSubscription = this.mediaService.videoStreamSubject.subscribe((mediaStreams) => {
			this.updateMediaStreams(MediaStreamType.VIDEO, mediaStreams);
		});

		this.speakingSubscription = this.mediaService.speakingSubject.subscribe((speaking) => {
			this.speakingSubject.next(speaking);
		});

		this.microphoneMutedSubscription = this.mediaService.microphoneMutedSubject.subscribe((muted) => {
			this.microphoneMutedSubject.next(muted);
		});

		this.speakerMutedSubscription = this.mediaService.speakerMutedSubject.subscribe((muted) => {
			this.speakerMutedSubject.next(muted);
		});
	}

	private updateMediaStreams(type: MediaStreamType, incomingMediaStreams: MediaStream[]) {
		this.mediaStreams
			.filter((mediaStream) => mediaStream.type === type)
			.filter((mediaStream) => !incomingMediaStreams.includes(mediaStream.stream))
			.forEach((mediaStream) => {
				this.mediaStreams.splice(this.mediaStreams.indexOf(mediaStream));
			});

		incomingMediaStreams
			.filter((mediaStream) => !this.mediaStreams.find((m) => m.stream === mediaStream))
			.forEach((stream) => {
				this.mediaStreams.push({ type, stream });
				this.webrtcService.onMediaStreamAdd.next({ memberConnection: this, typedMediaStream: { stream, type } });
			});
	}

	public async destroy(): Promise<void> {
		this.microphoneStreamSubscription?.unsubscribe();
		this.screenStreamSubscription?.unsubscribe();
		this.videoStreamSubscription?.unsubscribe();
		this.speakerMutedSubscription?.unsubscribe();
		this.speakingSubscription?.unsubscribe();
		this.microphoneMutedSubscription?.unsubscribe();
	}
}
