import { WebrtcService } from '../../services/webrtc/webrtc.service';
import { CallService } from '../../services/call/call.service';
import { LogService, Level } from '../../services/log/log.service';
import { CryptoService } from '../../services/crypto/crypto.service';
import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';
import { BehaviorSubject } from 'rxjs';
import { MediaService } from '../../services/media/media.service';
import { Event } from '../../services/live/live.service';
import { filter } from 'rxjs/operators';
import { ConnectError } from '../connect.error';
import { BaseMemberConnection, MediaStreamType } from './baseMemberConnection';

export class RemoteMemberConnection extends BaseMemberConnection {
	private audioStream: MediaStream | undefined;
	private audioStreamAudio?: HTMLAudioElement;

	private outgoingMicrophoneStream?: MediaStream;
	private outgoingScreenStreams: MediaStream[] = [];
	private outgoingVideoStreams: MediaStream[] = [];

	private setupInProgress = new BehaviorSubject<boolean>(false);

	private rtcPeerConnection: RTCPeerConnection | undefined;
	private rtcDataChannel: RTCDataChannel | undefined;

	private webrtcService: WebrtcService;
	private logService: LogService;

	private negotiationInProgress = false;
	private reconnecting = false;

	private rtcSenders: RTCRtpSender[] = [];

	public volume = 1;

	private mediaStreamIdentifier = new Map<string, MediaStreamType>();

	constructor(
		membershipCaller: Membership,
		public cryptoService: CryptoService,
		public callService: CallService,
		private polite: boolean,
		private mediaService: MediaService
	) {
		super(membershipCaller);
		this.webrtcService = callService.webrtcService;
		this.logService = this.webrtcService.logService;
		this.logService.info(`${this.membershipCaller.connectionId} | Creating MemberConnection... (Polite: ${polite})`);
	}

	public async setup() {
		const volume = await this.callService.webrtcService.dexieService.volume().get(this.membershipCaller.userId);
		if (volume?.volume > 0) {
			this.volume = volume?.volume || 1;
		}

		await this.setupRtcConnection();
	}

	private async setupRtcConnection() {
		if (this.setupInProgress.value) {
			return new Promise((resolve) => this.setupInProgress.pipe(filter((v) => !v)).subscribe((v) => resolve()));
		}
		this.setupInProgress.next(true);
		try {
			let relay;
			try {
				relay = await this.callService.findRelay();
			} catch (error) {
				await this.destroyOrReconnect(true);
				throw new ConnectError(Level.ERROR, 'Could not find a relay service.', 'error.relay.notfound');
			}

			this.rtcPeerConnection = new RTCPeerConnection({
				iceServers: [
					{
						urls: `turn:${relay}`,
						username: 'kominal',
						credential: 'kominal',
					},
				],
				iceTransportPolicy: 'relay',
			});

			this.rtcPeerConnection.onicecandidate = async (event) => {
				if (event.candidate) {
					this.sendSignaling('ICE', event.candidate);
				}
			};

			this.rtcPeerConnection.oniceconnectionstatechange = () => {
				this.logService.debug(
					`${this.membershipCaller.connectionId} | IceConnectionState changed to ${this.rtcPeerConnection?.iceConnectionState}.`
				);
				if (this.rtcPeerConnection) {
					if (this.rtcPeerConnection.iceConnectionState === 'disconnected') {
						this.destroyOrReconnect(true);
					} else if (this.rtcPeerConnection.iceConnectionState === 'failed') {
						(this.rtcPeerConnection as any).restartIce();
					}
				}
			};

			this.rtcPeerConnection.onsignalingstatechange = () => {
				this.logService.debug(
					`${this.membershipCaller.connectionId} | SignalingState changed to ${this.rtcPeerConnection?.signalingState}.`
				);
			};

			this.rtcPeerConnection.onicegatheringstatechange = () => {
				this.logService.debug(
					`${this.membershipCaller.connectionId} | IceGatheringState changed to ${this.rtcPeerConnection?.iceGatheringState}.`
				);
			};

			this.rtcPeerConnection.onconnectionstatechange = () => {
				this.logService.debug(
					`${this.membershipCaller.connectionId} | ConnectionState changed to ${this.rtcPeerConnection?.connectionState}.`
				);
				if (this.rtcPeerConnection?.connectionState === 'connected') {
					this.callService.soundService.playJoined();
					this.reconnecting = true;
				}
			};

			this.rtcPeerConnection.onnegotiationneeded = () => this.negotiate();

			this.rtcPeerConnection.ontrack = async (event) => {
				this.logService.debug(`${this.membershipCaller.connectionId} | Adding remote ${event.track.kind} stream...`);

				const stream = event.streams[0];

				if (!stream) {
					this.logService.debug(`${this.membershipCaller.connectionId} | Received track without stream...`);
					return;
				}

				if (event.track.kind === 'video') {
					stream.onremovetrack = () => {
						const index = this.mediaStreams.findIndex((mediaStream) => mediaStream.stream === stream);
						if (index) {
							this.mediaStreams.splice(index, 1);
						}
					};

					if (this.mediaStreams.find((mediaStream) => mediaStream.stream === stream)) {
						return;
					}

					const type = this.mediaStreamIdentifier.get(stream.id) || MediaStreamType.UNKNOWN;
					this.mediaStreams.push({ type, stream });
					this.webrtcService.onMediaStreamAdd.next({ memberConnection: this, typedMediaStream: { stream, type } });
				} else if (event.track.kind === 'audio') {
					stream.onremovetrack = () => {
						this.audioStream = undefined;
						this.audioStreamAudio = undefined;
					};

					this.audioStream = stream;

					this.audioStreamAudio = new Audio();
					this.audioStreamAudio.autoplay = true;
					this.audioStreamAudio.volume = this.volume;
					this.audioStreamAudio.muted = this.mediaService.speakerMutedSubject.value;
					this.audioStreamAudio.srcObject = this.audioStream;
				}
			};

			this.rtcPeerConnection.ondatachannel = (event) => {
				this.handleDataChannel(event.channel);
			};

			this.subscribeToSubjects();

			if (!this.polite) {
				this.handleDataChannel(this.rtcPeerConnection.createDataChannel('data'));
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		this.setupInProgress.next(false);
	}

	private async handleDataChannel(rtcDataChannel: RTCDataChannel) {
		this.rtcDataChannel = rtcDataChannel;

		this.rtcDataChannel.onopen = () => {
			this.sendEvent({ type: 'SPEAKING', content: this.mediaService.speakingSubject.value });
			this.sendEvent({ type: 'MICROPHONE', content: this.mediaService.microphoneMutedSubject.value });
			this.sendEvent({ type: 'SPEAKER', content: this.mediaService.speakerMutedSubject.value });
			this.outgoingScreenStreams.forEach(({ id }) => this.sendEvent({ type: 'MEDIASTREAM_TYPE', content: { id, type: 'SCREEN' } }));
			this.outgoingVideoStreams.forEach(({ id }) => this.sendEvent({ type: 'MEDIASTREAM_TYPE', content: { id, type: 'VIDEO' } }));
		};

		this.rtcDataChannel.onmessage = (message) => {
			const { type, content }: Event = JSON.parse(message.data);
			if (type === 'SPEAKING') {
				this.speakingSubject.next(content);
			} else if (type === 'MICROPHONE') {
				this.microphoneMutedSubject.next(content);
			} else if (type === 'SPEAKER') {
				this.speakerMutedSubject.next(content);
			} else if (type === 'MEDIASTREAM_TYPE') {
				const { id, type } = content;
				this.mediaStreamIdentifier.set(id, type);

				const mediaStream = this.mediaStreams.find(({ stream }) => stream.id === id);
				if (mediaStream) {
					mediaStream.type = type;
				}
			}
		};
	}

	private async negotiate() {
		if (!this.rtcPeerConnection) {
			throw new Error('RtcPeerConnection is undefined (1)');
		}

		this.logService.debug(`${this.membershipCaller.connectionId} | Negotiation requested for connection...`);

		if (this.negotiationInProgress) {
			this.logService.warn(`${this.membershipCaller.connectionId} | Negotiation is already in progress...`);
		}

		try {
			this.negotiationInProgress = true;
			const offer = await this.rtcPeerConnection.createOffer({ iceRestart: true, offerToReceiveAudio: true, offerToReceiveVideo: true });
			await (this.rtcPeerConnection as any).setLocalDescription(offer);
			this.sendSignaling('DESCRIPTION', offer);
		} catch (e) {
			this.logService.handleError(e);
		} finally {
			this.negotiationInProgress = false;
		}
	}

	private async sendSignaling(type: string, data: any) {
		if (!this.membershipCaller.connectionId) {
			throw new Error('ConnectionId is undefined (1)');
		}
		this.webrtcService.sendSignaling(this.callService, this.membershipCaller.connectionId, type, data);
	}

	public async handleSignaling(type: string, data: any) {
		try {
			await new Promise((resolve) => this.setupInProgress.pipe(filter((v) => !v)).subscribe((v) => resolve()));

			if (!this.rtcPeerConnection) {
				this.logService.info(`${this.membershipCaller.connectionId} | No RTCPeerConnection present. Ignoring signaling packet!`);
				return;
			}

			if (type === 'DESCRIPTION') {
				const description = new RTCSessionDescription(data);
				this.logService.debug(`${this.membershipCaller.connectionId} | Received ${description.type}...`);

				const offerCollision =
					description.type == 'offer' && (this.negotiationInProgress || this.rtcPeerConnection.signalingState != 'stable');

				const ignoreOffer = !this.polite && offerCollision;
				if (ignoreOffer) {
					this.logService.debug(`${this.membershipCaller.connectionId} | Ignorning offer due to offer collision.`);
					return;
				}

				await this.rtcPeerConnection.setRemoteDescription(description);
				if (description.type == 'offer') {
					const answer = await this.rtcPeerConnection.createAnswer();
					await (this.rtcPeerConnection as any).setLocalDescription(answer);
					this.sendSignaling('DESCRIPTION', answer);
				}
			} else if (type === 'ICE' && this.rtcPeerConnection.remoteDescription != null) {
				this.logService.debug(`${this.membershipCaller.connectionId} | Received ICE...`);
				try {
					await this.rtcPeerConnection.addIceCandidate(new RTCIceCandidate(data));
				} catch (e) {
					this.logService.debug(`${this.membershipCaller.connectionId} | Ignoring ICE...`);
				}
			}
		} catch (e) {
			this.logService.error(`${this.membershipCaller.connectionId} | There was an error handling a signaling packet: ${e}`);
		}
	}

	private addMediaStream(mediaStream: MediaStream | undefined, type: MediaStreamType) {
		if (!mediaStream) {
			return;
		}

		if (!this.rtcPeerConnection) {
			this.logService.info(`${this.membershipCaller.connectionId} | No RTCPeerConnection present. Ignoring media stream!`);
			return;
		}

		this.sendEvent({ type: 'MEDIASTREAM_TYPE', content: { id: mediaStream.id, type } });
		for (const track of mediaStream.getTracks()) {
			this.rtcSenders.push(this.rtcPeerConnection.addTrack(track, mediaStream));
		}
	}

	private removeMediaStream(mediaStream?: MediaStream) {
		if (!mediaStream) {
			return;
		}

		for (const track of mediaStream.getTracks()) {
			this.rtcSenders.filter((sender) => sender.track === track).forEach((sender) => this.rtcPeerConnection?.removeTrack(sender));
		}
	}

	private sendEvent(event: Event) {
		if (this.rtcDataChannel?.readyState === 'open') {
			this.rtcDataChannel?.send(JSON.stringify(event));
		} else {
			this.logService.info(
				`${this.membershipCaller.connectionId} | RTCDataChannel not ready. Current state: ${
					this.rtcDataChannel?.readyState
				}, message: ${JSON.stringify(event)}.`
			);
		}
	}

	public async destroy(): Promise<void> {
		this.destroyOrReconnect(false);
	}

	private async destroyOrReconnect(reconnect: boolean) {
		this.rtcPeerConnection?.close();

		this.rtcDataChannel = this.audioStream = this.audioStreamAudio = this.rtcPeerConnection = undefined;
		this.mediaStreams = [];
		this.rtcSenders = [];

		this.negotiationInProgress = false;

		if (!this.reconnecting && reconnect) {
			this.reconnecting = true;
			this.logService.info(`${this.membershipCaller.connectionId} | Reconnecting MemberConnection...`, 'error.call.reconnecting');
			await this.setupRtcConnection();
		} else if (this.webrtcService.memberConnections.includes(this)) {
			this.logService.info(`${this.membershipCaller.connectionId} | Destroying MemberConnection...`);
			this.webrtcService.memberConnections.splice(this.webrtcService.memberConnections.indexOf(this), 1);
			this.unsubscribeFromSubjects();
		}
	}

	private subscribeToSubjects() {
		this.unsubscribeFromSubjects();
		this.microphoneStreamSubscription = this.mediaService.microphoneStreamSubject.subscribe((mediaStream) => {
			this.removeMediaStream(this.outgoingMicrophoneStream);
			this.outgoingMicrophoneStream = mediaStream;
			this.addMediaStream(this.outgoingMicrophoneStream, MediaStreamType.MICROPHONE);
		});

		this.screenStreamSubscription = this.mediaService.screenStreamSubject.subscribe((mediaStreams) => {
			this.outgoingScreenStreams
				.filter((mediaStream) => !mediaStreams.includes(mediaStream))
				.forEach((mediaStream) => {
					this.removeMediaStream(mediaStream);
					this.outgoingScreenStreams.splice(this.outgoingScreenStreams.indexOf(mediaStream), 1);
				});

			mediaStreams
				.filter((mediaStream) => !this.outgoingScreenStreams.includes(mediaStream))
				.forEach((mediaStream) => {
					this.addMediaStream(mediaStream, MediaStreamType.SCREEN);
					this.outgoingScreenStreams.push(mediaStream);
				});
		});

		this.videoStreamSubscription = this.mediaService.videoStreamSubject.subscribe((mediaStreams) => {
			this.outgoingVideoStreams
				.filter((mediaStream) => !mediaStreams.includes(mediaStream))
				.forEach((mediaStream) => {
					this.removeMediaStream(mediaStream);
					this.outgoingVideoStreams.splice(this.outgoingVideoStreams.indexOf(mediaStream), 1);
				});

			mediaStreams
				.filter((mediaStream) => !this.outgoingVideoStreams.includes(mediaStream))
				.forEach((mediaStream) => {
					this.addMediaStream(mediaStream, MediaStreamType.VIDEO);
					this.outgoingVideoStreams.push(mediaStream);
				});
		});

		this.speakingSubscription = this.mediaService.speakingSubject.subscribe((speaking) => {
			this.sendEvent({ type: 'SPEAKING', content: speaking });
		});

		this.microphoneMutedSubscription = this.mediaService.microphoneMutedSubject.subscribe((muted) => {
			this.sendEvent({ type: 'MICROPHONE', content: muted });
		});

		this.speakerMutedSubscription = this.mediaService.speakerMutedSubject.subscribe((muted) => {
			if (this.audioStreamAudio) {
				this.audioStreamAudio.muted = muted;
			}
			this.sendEvent({ type: 'SPEAKER', content: muted });
		});
	}

	private unsubscribeFromSubjects() {
		this.microphoneStreamSubscription?.unsubscribe();
		this.screenStreamSubscription?.unsubscribe();
		this.videoStreamSubscription?.unsubscribe();
		this.speakerMutedSubscription?.unsubscribe();
		this.speakingSubscription?.unsubscribe();
		this.microphoneMutedSubscription?.unsubscribe();
	}
}
