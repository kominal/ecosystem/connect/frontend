import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';
import { Subscription, BehaviorSubject } from 'rxjs';

export enum MediaStreamType {
	MICROPHONE = 'MICROPHONE',
	VIDEO = 'VIDEO',
	SCREEN = 'SCREEN',
	UNKNOWN = 'UNKNOWN',
}

export interface TypedMediaStream {
	type: MediaStreamType;
	stream: MediaStream;
}

export interface FocusedMediaStream {
	memberConnection: BaseMemberConnection;
	typedMediaStream: TypedMediaStream;
}

export class BaseMemberConnection {
	public mediaStreams: TypedMediaStream[] = [];

	public microphoneMutedSubject = new BehaviorSubject<boolean>(false);
	public speakerMutedSubject = new BehaviorSubject<boolean>(false);
	public speakingSubject = new BehaviorSubject<boolean>(false);

	public screenStreamSubscription?: Subscription;
	public videoStreamSubscription?: Subscription;
	protected microphoneStreamSubscription?: Subscription;
	protected speakerMutedSubscription?: Subscription;
	protected speakingSubscription?: Subscription;
	protected microphoneMutedSubscription?: Subscription;

	constructor(public membershipCaller: Membership) {}

	public async setup(): Promise<void> {}
	public async handleSignaling(type: string, data: any): Promise<void> {}
	public async destroy(): Promise<void> {}

	getMediaStreams(type?: MediaStreamType) {
		if (type) {
			return this.mediaStreams.filter((stream) => stream.type === type);
		}

		return this.mediaStreams;
	}
}
