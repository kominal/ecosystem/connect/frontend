import { ElementRef } from '@angular/core';

export class Position {
	private previousScrollHeight: number;
	private previousScrollTop: number;

	constructor(private viewport: ElementRef<HTMLElement>) {
		this.previousScrollHeight = viewport.nativeElement.scrollHeight;
		this.previousScrollTop = viewport.nativeElement.scrollTop;
	}

	restore() {
		requestAnimationFrame(() => {
			this.viewport.nativeElement.scrollTop = this.viewport.nativeElement.scrollHeight - this.previousScrollHeight + this.previousScrollTop;
		});
	}
}
