import { Level } from '../services/log/log.service';

export class ConnectError extends Error {
	level: Level;
	uiMessage?: string;

	public constructor(level: Level, message: string, uiMessage: string) {
		super(message);
		this.level = level;
		this.uiMessage = uiMessage;
	}
}
