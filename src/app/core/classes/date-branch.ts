import { UserBranch } from './user-branch';

export class DateBranch {
	public userBranches: UserBranch[] = [];

	constructor(public date: string) {}

	prependMessage(userBranch: UserBranch) {
		this.userBranches.splice(0, 0, userBranch);
	}

	appendMessage(userBranch: UserBranch) {
		this.userBranches.push(userBranch);
	}
}
