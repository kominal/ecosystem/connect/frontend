import { Message } from '@kominal/ecosystem-connect-models/message/message';
import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';

export class UserBranch {
	public messages: Message[] = [];

	constructor(public userId: string, public me: boolean, public membership: Membership | undefined) {}

	prependMessage(message: Message) {
		this.messages.splice(0, 0, message);
	}

	appendMessage(message: Message) {
		this.messages.push(message);
	}
}
