import { DateBranch } from './date-branch';
import { ChangeDetectorRef, ElementRef, Injector } from '@angular/core';
import { LogService } from 'src/app/core/services/log/log.service';
import { LoadingService } from 'src/app/core/services/loading/loading.service';
import { Message } from '@kominal/ecosystem-connect-models/message/message';
import { Group } from '@kominal/ecosystem-connect-models/group/group';
import { MembershipCacheService } from 'src/app/core/cache/membership/membership-cache.service';
import { MessageCryptoService } from 'src/app/core/crypto/message/message-crypto.service';
import { SerialService } from 'src/app/core/services/serial/serial.service';
import { UserBranch } from './user-branch';
import { Position } from './position';
import { MessageCacheService } from '../cache/message/message-cache.service';

export class MessageTree {
	public branches: DateBranch[] = [];
	public earliestMessageVisable = false;

	private firstMessage?: Message;
	private lastMessage?: Message;
	private sendingMessage?: Message;

	private logService: LogService;
	private loadingService: LoadingService;
	private membershipCacheService: MembershipCacheService;
	private messageCryptoService: MessageCryptoService;
	private messageCacheService: MessageCacheService;
	private serialService: SerialService;

	constructor(
		injector: Injector,
		private group: Group,
		private userId: string,
		private cdr: ChangeDetectorRef,
		private viewport: ElementRef<HTMLElement>
	) {
		this.logService = injector.get(LogService);
		this.loadingService = injector.get(LoadingService);
		this.membershipCacheService = injector.get(MembershipCacheService);
		this.messageCryptoService = injector.get(MessageCryptoService);
		this.messageCacheService = injector.get(MessageCacheService);
		this.serialService = injector.get(SerialService);
	}

	public init(cdr: ChangeDetectorRef, viewport: ElementRef<HTMLElement>) {
		this.cdr = cdr;
		this.viewport = viewport;
	}

	public async buildFromFlow(messages: Message[]) {
		const newBranches: DateBranch[] = [];
		if (messages.length > 0) {
			for (const message of messages) {
				await this.addToTree(newBranches, message, 'LAST');
			}
			this.firstMessage = messages[0];
			this.lastMessage = messages[messages.length - 1];
		}
		this.branches = newBranches;
	}

	public async addSendingMessage(message: Message) {
		this.sendingMessage = message;
		await this.addToTree(this.branches, message, 'LAST');
	}

	private async addToTree(branches: DateBranch[], message: Message, position: 'FIRST' | 'LAST') {
		const { id, keyDecrypted } = this.group;
		if (!keyDecrypted) {
			return false;
		}

		const date = new Date(message.time).toLocaleDateString(navigator.language);

		const { userId } = message;

		let dateBranch = branches.find((branch) => branch.date === date);
		if (!dateBranch) {
			dateBranch = new DateBranch(date);

			if (position === 'FIRST') {
				branches.splice(0, 0, dateBranch);
			} else {
				branches.push(dateBranch);
			}
		}

		let userBranch: UserBranch;
		if (position === 'FIRST') {
			userBranch = dateBranch.userBranches[0];
		} else {
			userBranch = dateBranch.userBranches[dateBranch.userBranches.length - 1];
		}
		if (!userBranch || userBranch.userId !== userId) {
			const membership = await this.membershipCacheService.getMembership(id, userId, keyDecrypted, 'EVENTUAL');
			userBranch = new UserBranch(userId, this.userId === userId, membership);
			if (position === 'FIRST') {
				dateBranch.userBranches.splice(0, 0, userBranch);
			} else {
				dateBranch.userBranches.push(userBranch);
			}
		}
		if (position === 'FIRST') {
			userBranch.messages.splice(0, 0, message);
		} else {
			userBranch.messages.push(message);
		}
	}

	async prependMessage(message: Message) {
		this.firstMessage = message;
		if (!this.lastMessage) {
			this.lastMessage = message;
		}
		await this.addToTree(this.branches, message, 'FIRST');
	}

	async appendMessage(message: Message) {
		this.lastMessage = message;
		if (!this.firstMessage) {
			this.firstMessage = message;
		}
		if (this.sendingMessage?.id === message.id) {
			this.sendingMessage = undefined;
			return;
		}
		await this.addToTree(this.branches, message, 'LAST');
	}

	async loadPrevious(): Promise<void> {
		return this.serialService.doOnce(async () => {
			return this.loadingService.doWithErrorHandlingWhileLoading(async () => {
				const messages = await this.messageCacheService.getPreviousMessages(this.group.id, this.firstMessage);

				if (messages.length > 0) {
					await this.setState(messages, 'TEXT');
					await this.setState(messages, 'VIEWABLE');

					for (const message of messages) {
						await this.prependMessage(message);
					}
					this.earliestMessageVisable = messages.length < 20;

					const position = new Position(this.viewport);
					this.cdr.detectChanges();
					position.restore();

					if (await this.setState(messages, 'RESOLVED')) {
						this.cdr.detectChanges();
					}
				}
			});
		});
	}

	async setState(messages: Message[], state: 'TEXT' | 'VIEWABLE' | 'RESOLVED'): Promise<boolean> {
		const { keyDecrypted } = this.group;
		if (!keyDecrypted) {
			return false;
		}

		let modified = false;
		for (const message of messages) {
			try {
				modified = (await this.messageCryptoService.setMessageState(message, keyDecrypted, state)) ? true : modified;
			} catch (e) {
				this.logService.handleError(e);
			}
		}
		return modified;
	}

	async loadLatest() {
		return this.serialService.doOnce(async () => {
			return this.loadingService.doWithErrorHandlingWhileLoading(async () => {
				const { id, keyDecrypted } = this.group;
				if (!keyDecrypted) {
					return;
				}
				const messages = await this.messageCacheService.getNextMessages(id, this.lastMessage);

				if (messages.length > 0) {
					await Promise.all(messages.map((message) => this.messageCryptoService.setMessageState(message, keyDecrypted, 'TEXT')));

					this.branches.forEach((branch) =>
						branch.userBranches.forEach((userBranch) => {
							userBranch.messages.forEach((message) => {
								if (!message.id) {
									userBranch.messages.splice(userBranch.messages.indexOf(message), 1);
								}
							});
						})
					);

					for (const message of messages) {
						await this.appendMessage(message);
					}

					await this.setState(messages, 'VIEWABLE');

					await this.scrollToBottom();

					this.group.unreadMessages = 0;

					if (await this.setState(messages, 'RESOLVED')) {
						this.cdr.detectChanges();
					}
				}
			});
		});
	}

	async scrollToBottom() {
		this.cdr.detectChanges();
		await new Promise((resolve) => {
			requestAnimationFrame(() => {
				this.viewport.nativeElement.scrollTop = this.viewport.nativeElement.scrollHeight;
				resolve();
				//Was required previously, looks like its not rn...
				/*requestAnimationFrame(() => {
					requestAnimationFrame(() => {});
				});*/
			});
		});
	}
}
