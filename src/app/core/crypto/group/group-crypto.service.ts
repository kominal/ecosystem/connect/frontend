import { Injectable } from '@angular/core';
import { CryptoService } from '../../services/crypto/crypto.service';
import { UserService } from '../../services/user/user.service';
import { AvatarService } from '../../services/avatar/avatar.service';
import { MessageCryptoService } from '../message/message-crypto.service';
import { MessageCacheService } from '../../cache/message/message-cache.service';
import { MembershipCacheService } from '../../cache/membership/membership-cache.service';
import { Group } from '@kominal/ecosystem-connect-models/group/group';

@Injectable({
	providedIn: 'root',
})
export class GroupCryptoService {
	constructor(
		private cryptoService: CryptoService,
		private userService: UserService,
		private avatarService: AvatarService,
		private messageCryptoService: MessageCryptoService,
		private messageCacheService: MessageCacheService,
		private membershipCacheService: MembershipCacheService
	) {}

	async setGroupStates(groups: Group[], cryptoState: 'VIEWABLE' | 'RESOLVED'): Promise<void> {
		await Promise.all(groups.map((group) => this.setGroupState(group, cryptoState)));
	}

	async setGroupState(group: Group, cryptoState?: 'VIEWABLE' | 'RESOLVED'): Promise<void> {
		if (group.cryptoState === cryptoState) {
			return;
		}

		if (!group.keyDecrypted) {
			group.keyDecrypted = await this.cryptoService.decryptRSA(group.groupKey, await this.userService.getPrivateKey());
		}

		const { keyDecrypted } = group;

		if (cryptoState === 'VIEWABLE' || cryptoState === 'RESOLVED') {
			if (!group.nameDecrypted && group.name) {
				group.nameDecrypted = await this.cryptoService.decryptAES(group.name, keyDecrypted, 'string');
			}
			if (!group.avatarDecrypted) {
				group.avatarDecrypted = this.avatarService.getIdenticon(group.partnerProfileId || group.id);
			}
		}

		if (cryptoState === 'RESOLVED') {
			if (group.avatar) {
				group.avatarDecrypted = await this.avatarService.getAvatar(
					group.partnerProfileId || group.id,
					await this.cryptoService.decryptAES(group.avatar, keyDecrypted)
				);
			}
			if (!group.partnerProfile && group.partnerId) {
				group.partnerProfile = (
					await this.membershipCacheService.getMembership(group.id, group.partnerId, keyDecrypted, 'EVENTUAL')
				).profile;
			}
			if (!group.latestMessage && group.latestMessageId) {
				group.latestMessage = await this.messageCacheService.getMessage(group.id, group.latestMessageId);
				if (group.latestMessage) {
					await this.messageCryptoService.setMessageState(group.latestMessage, keyDecrypted, 'TEXT');
				}
			}
		}

		group.cryptoState = cryptoState;
	}
}
