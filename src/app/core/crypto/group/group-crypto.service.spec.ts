import { TestBed } from '@angular/core/testing';

import { GroupCryptoService } from './group-crypto.service';

describe('GroupCryptoService', () => {
  let service: GroupCryptoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GroupCryptoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
