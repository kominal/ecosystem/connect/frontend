import { Injectable } from '@angular/core';
import { Message } from '@kominal/ecosystem-connect-models/message/message';
import { CryptoService } from '../../services/crypto/crypto.service';
import * as marked from 'marked';
import { StorageService } from '../../services/storage/storage.service';
import { MembershipCacheService } from '../../cache/membership/membership-cache.service';

@Injectable({
	providedIn: 'root',
})
export class MessageCryptoService {
	constructor(
		private cryptoService: CryptoService,
		private storageService: StorageService,
		private membershipCacheService: MembershipCacheService
	) {}

	async setMessageStates(messages: Message[], key: string, cryptoState?: 'TEXT' | 'ENCRYPTED' | 'VIEWABLE' | 'RESOLVED'): Promise<boolean> {
		return (await Promise.all(messages.map((message) => this.setMessageState(message, key, cryptoState)))).includes(true);
	}

	async setMessageState(message: Message, key: string, cryptoState?: 'TEXT' | 'ENCRYPTED' | 'VIEWABLE' | 'RESOLVED'): Promise<boolean> {
		let modified = false;
		const { text, textDecrypted } = message;
		if (cryptoState === 'TEXT') {
			if (text && !textDecrypted) {
				message.textDecrypted = await this.cryptoService.decryptAES(text, key, 'string');
				message.textParsed = this.parseText(message.textDecrypted);
				modified = true;
			}
			if (!message.timeString) {
				message.timeString = this.formatTime(message.time);
				modified = true;
			}
			if (!message.membership) {
				message.membership = await this.membershipCacheService.getMembership(message.groupId, message.userId, key, 'EVENTUAL');
				modified = true;
			}
		}
		if (cryptoState === 'ENCRYPTED') {
			for (const link of message.links) {
				if (link.image && !link.imageStorageId) {
					const { id, safeUrl } = await this.storageService.saveToGridFS(link.image, key);
					link.imageStorageId = id;
					link.image = safeUrl;
					modified = true;
				}
				if (link.urlDecrypted && !link.url) {
					link.url = await this.cryptoService.encryptAES(link.urlDecrypted, key);
					modified = true;
				}
				if (link.siteDecrypted && !link.site) {
					link.site = await this.cryptoService.encryptAES(link.siteDecrypted, key);
					modified = true;
				}
				if (link.titleDecrypted && !link.title) {
					link.title = await this.cryptoService.encryptAES(link.titleDecrypted, key);
					modified = true;
				}
				if (link.descriptionDecrypted && !link.description) {
					link.description = await this.cryptoService.encryptAES(link.descriptionDecrypted, key);
					modified = true;
				}
				if (link.imageSizeDecrypted && !link.imageSize) {
					link.imageSize = await this.cryptoService.encryptAES(JSON.stringify(link.imageSizeDecrypted), key);
					modified = true;
				}
			}
			for (const attachment of message.attachments) {
				if (attachment.arrayBuffer && !attachment.storageId) {
					const { id, safeUrl } = await this.storageService.saveToGridFS(attachment.arrayBuffer, key);
					attachment.storageId = id;
					if (attachment.image) {
						attachment.image = safeUrl;
						attachment.imageStorageId = id;
					}
					modified = true;
				}
				if (attachment.nameDecrypted && !attachment.name) {
					attachment.name = await this.cryptoService.encryptAES(attachment.nameDecrypted, key);
					modified = true;
				}
				if (attachment.typeDecrypted && !attachment.type) {
					attachment.type = await this.cryptoService.encryptAES(attachment.typeDecrypted, key);
					modified = true;
				}
				if (attachment.sizeDecrypted && !attachment.size) {
					attachment.size = await this.cryptoService.encryptAES(attachment.sizeDecrypted, key);
					modified = true;
				}
				if (attachment.imageSizeDecrypted && !attachment.imageSize) {
					attachment.imageSize = await this.cryptoService.encryptAES(JSON.stringify(attachment.imageSizeDecrypted), key);
					modified = true;
				}
			}
		}
		if (cryptoState === 'VIEWABLE') {
			for (const link of message.links) {
				if (!link.urlDecrypted && link.url) {
					link.urlDecrypted = await this.cryptoService.decryptAES(link.url, key, 'string');
					modified = true;
				}
				if (!link.siteDecrypted && link.site) {
					link.siteDecrypted = await this.cryptoService.decryptAES(link.site, key, 'string');
					modified = true;
				}
				if (!link.titleDecrypted && link.title) {
					link.titleDecrypted = await this.cryptoService.decryptAES(link.title, key, 'string');
					modified = true;
				}
				if (!link.descriptionDecrypted && link.description) {
					link.descriptionDecrypted = await this.cryptoService.decryptAES(link.description, key, 'string');
					modified = true;
				}
				if (!link.imageSizeDecrypted && link.imageSize) {
					link.imageSizeDecrypted = await this.cryptoService.decryptAES(link.imageSize, key, 'object');
					modified = true;
				}
			}
			for (const attachment of message.attachments) {
				if (!attachment.nameDecrypted && attachment.name) {
					attachment.nameDecrypted = await this.cryptoService.decryptAES(attachment.name, key, 'string');
					modified = true;
				}
				if (!attachment.typeDecrypted && attachment.type) {
					attachment.typeDecrypted = await this.cryptoService.decryptAES(attachment.type, key, 'string');
					modified = true;
				}
				if (!attachment.sizeDecrypted && attachment.size) {
					attachment.sizeDecrypted = await this.cryptoService.decryptAES(attachment.size, key, 'string');
					modified = true;
				}
				if (!attachment.imageSizeDecrypted && attachment.imageSize) {
					attachment.imageSizeDecrypted = await this.cryptoService.decryptAES(attachment.imageSize, key, 'object');
					modified = true;
				}
			}
		}
		if (cryptoState === 'RESOLVED') {
			for (const link of message.links) {
				if (!link.image && link.imageStorageId) {
					link.image = await this.storageService.getFromGridFSAsSafeUrl(link.imageStorageId, key);
					modified = true;
				}
			}
			for (const attachment of message.attachments) {
				if (!attachment.image && attachment.imageStorageId) {
					attachment.image = await this.storageService.getFromGridFSAsSafeUrl(attachment.imageStorageId, key);
					modified = true;
				}
			}
		}
		return modified;
	}

	parseText(text: string | undefined): string | undefined {
		if (!text) {
			return undefined;
		}
		text = text.replace(/(https?:\/\/[^\s]+)/g, '<a class="url-highlight" target="_blank" rel="noopener noreferrer" href="$&">$&</a>');
		text = marked(text);
		return `${text}`;
	}

	private formatTime(time: number) {
		const date = new Date(time);

		let hours = '' + date.getHours();
		let minutes = '' + date.getMinutes();

		if (hours.length < 2) {
			hours = '0' + hours;
		}
		if (minutes.length < 2) {
			minutes = '0' + minutes;
		}
		return [hours, minutes].join(':');
	}
}
