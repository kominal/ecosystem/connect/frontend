import { TestBed } from '@angular/core/testing';

import { MessageCryptoService } from './message-crypto.service';

describe('MessageCryptoService', () => {
  let service: MessageCryptoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MessageCryptoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
