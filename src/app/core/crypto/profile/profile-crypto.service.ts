import { Injectable } from '@angular/core';
import { CryptoService } from '../../services/crypto/crypto.service';
import { UserService } from '../../services/user/user.service';
import { AvatarService } from '../../services/avatar/avatar.service';
import { Profile } from '@kominal/ecosystem-models/profile';

export type ProfileDecryptionOptions = { keyDecrypted?: string; displaynameHash?: string };

@Injectable({
	providedIn: 'root',
})
export class ProfileCryptoService {
	constructor(private cryptoService: CryptoService, private userService: UserService, private avatarService: AvatarService) {}

	async setProfileStates(profiles: Profile[], cryptoState?: 'DECRYPTED', decryptionOptions?: ProfileDecryptionOptions): Promise<void> {
		await Promise.all(profiles.map((profile) => this.setProfileState(profile, cryptoState, decryptionOptions)));
	}

	async setProfileState(profile: Profile, cryptoState?: 'DECRYPTED', decryptionOptions?: ProfileDecryptionOptions): Promise<void> {
		if (profile.cryptoState === cryptoState) {
			return;
		}

		if (!profile.keyDecrypted) {
			if (decryptionOptions?.keyDecrypted) {
				profile.keyDecrypted = decryptionOptions.keyDecrypted;
			} else if (decryptionOptions?.displaynameHash) {
				profile.keyDecrypted = await this.cryptoService.decryptAES(profile.keyDisplayname, decryptionOptions.displaynameHash, 'string');
			} else if (profile.keyMasterEncryptionKey) {
				profile.keyDecrypted = await this.cryptoService.decryptAES(
					profile.keyMasterEncryptionKey,
					await this.userService.getMasterEncryptionKey(),
					'string'
				);
			} else {
				return;
			}
		}

		const { keyDecrypted } = profile;

		if (!keyDecrypted) {
			throw 'KeyDecrypted undefined';
		}

		if (cryptoState === 'DECRYPTED') {
			if (!profile.displaynameDecrypted && profile.displayname) {
				profile.displaynameDecrypted = await this.cryptoService.decryptAES(profile.displayname, keyDecrypted, 'string');
			}
			if (!profile.publicKeyDecrypted && profile.publicKey) {
				profile.publicKeyDecrypted = await this.cryptoService.decryptAES(profile.publicKey, keyDecrypted, 'string');
			}
			if (!profile.avatarDecrypted) {
				profile.avatarDecrypted = await this.avatarService.getAvatar(
					profile.id,
					profile.avatar ? await this.cryptoService.decryptAES(profile.avatar, keyDecrypted) : undefined
				);
			}
		}
	}
}
