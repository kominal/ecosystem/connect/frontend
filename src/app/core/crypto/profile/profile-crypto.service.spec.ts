import { TestBed } from '@angular/core/testing';

import { ProfileCryptoService } from './profile-crypto.service';

describe('ProfileCryptoService', () => {
  let service: ProfileCryptoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfileCryptoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
