import { Injectable } from '@angular/core';
import { ProfileCacheService } from '../../cache/profile/profile-cache.service';
import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';
import { CryptoService } from '../../services/crypto/crypto.service';

@Injectable({
	providedIn: 'root',
})
export class MembershipCryptoService {
	constructor(private cryptoService: CryptoService, private profileCacheService: ProfileCacheService) {}

	async setMembershipStates(memberships: Membership[], groupKey: string, cryptoState?: 'VIEWABLE'): Promise<void> {
		await Promise.all(memberships.map((membership) => this.setMembershipState(membership, groupKey, cryptoState)));
	}

	async setMembershipState(membership: Membership, groupKey: string, cryptoState?: 'VIEWABLE'): Promise<void> {
		if (membership.cryptoState === cryptoState) {
			return;
		}
		if (cryptoState === 'VIEWABLE') {
			if (!membership.profile && membership.profileId && membership.profileKey) {
				membership.profile = await this.profileCacheService.getProfile(membership.profileId, 'EVENTUAL', {
					keyDecrypted: await this.cryptoService.decryptAES(membership.profileKey, groupKey, 'string'),
				});
			}
		}
	}
}
