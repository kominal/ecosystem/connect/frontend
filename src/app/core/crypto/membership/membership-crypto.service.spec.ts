import { TestBed } from '@angular/core/testing';

import { MembershipCryptoService } from './membership-crypto.service';

describe('MembershipCryptoService', () => {
  let service: MembershipCryptoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MembershipCryptoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
