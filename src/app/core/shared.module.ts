import { NgModule } from '@angular/core';

import { AvatarComponent } from 'src/app/core/components/avatar/avatar.component';
import { CommonModule } from '@angular/common';

@NgModule({
	declarations: [AvatarComponent],
	imports: [CommonModule],
	exports: [AvatarComponent],
})
export class SharedModule {}
