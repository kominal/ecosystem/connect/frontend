import { Component, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Profile } from '@kominal/ecosystem-models/profile';

@Component({
	selector: 'app-text-with-profile-dialog',
	templateUrl: './text-with-profile-dialog.component.html',
	styleUrls: ['./text-with-profile-dialog.component.scss'],
})
export class TextWithProfileDialogComponent {
	public form: FormGroup;

	constructor(
		public dialogRef: MatDialogRef<TextWithProfileDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { title: string; label: string; button: string; text?: string; profiles: Profile[] }
	) {
		this.form = new FormGroup({
			text: new FormControl(data.text, Validators.required),
			profile: new FormControl(data.profiles[0], Validators.required),
		});
	}
}
