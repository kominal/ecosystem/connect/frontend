import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextWithProfileDialogComponent } from './text-with-profile-dialog.component';

describe('TextWithProfileDialogComponent', () => {
	let component: TextWithProfileDialogComponent;
	let fixture: ComponentFixture<TextWithProfileDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [TextWithProfileDialogComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TextWithProfileDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
