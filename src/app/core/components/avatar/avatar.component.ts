import { Component, Input } from '@angular/core';
import { Group } from '@kominal/ecosystem-connect-models/group/group';
import { StatusService } from '../../services/status/status.service';
import { Profile } from '@kominal/ecosystem-models/profile';

@Component({
	selector: 'app-avatar',
	templateUrl: './avatar.component.html',
	styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent {
	@Input() public group: Group | undefined;
	@Input() public profile: Profile | undefined;

	constructor(public statusService: StatusService) {}
}
