import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenShareCardComponent } from './screen-share-card.component';

describe('ScreenShareCardComponent', () => {
	let component: ScreenShareCardComponent;
	let fixture: ComponentFixture<ScreenShareCardComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ScreenShareCardComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ScreenShareCardComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
