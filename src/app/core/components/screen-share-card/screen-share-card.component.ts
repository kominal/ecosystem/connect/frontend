import {
	Component,
	ElementRef,
	Input,
	HostBinding,
	OnInit,
	AfterViewInit,
	Output,
	EventEmitter,
	ViewChild,
	OnDestroy,
} from '@angular/core';
import { WebrtcService } from '../../services/webrtc/webrtc.service';
import { CallService } from '../../services/call/call.service';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { LiveService } from '../../services/live/live.service';
import { MediaService } from '../../services/media/media.service';
import { UserService } from '../../services/user/user.service';
import { BaseMemberConnection, FocusedMediaStream, TypedMediaStream } from '../../classes/memberconnections/baseMemberConnection';

enum ScreenShareCardSize {
	SMALL,
	MEDIUM,
	FULLSCREEN,
}

@Component({
	selector: 'app-screen-share-card',
	templateUrl: './screen-share-card.component.html',
	styleUrls: ['./screen-share-card.component.scss'],
})
export class ScreenShareCardComponent implements OnInit, AfterViewInit, OnDestroy {
	@Output() sizeChange = new EventEmitter<{ previousSize: ScreenShareCardSize; size: ScreenShareCardSize }>();
	@Output() videoReady = new EventEmitter<void>();

	@ViewChild('player', { static: false }) player: ElementRef<HTMLVideoElement> | undefined;

	@HostBinding('class.screenshare-hidden') get hidden() {
		return this.focused === undefined && !this.fullscreen;
	}
	@HostBinding('class.screenshare-small') get small() {
		return this.size === ScreenShareCardSize.SMALL;
	}
	@HostBinding('class.screenshare-medium') get medium() {
		return this.size === ScreenShareCardSize.MEDIUM;
	}
	@HostBinding('class.screenshare-fullscreen') get fullscreen() {
		return this.size === ScreenShareCardSize.FULLSCREEN;
	}
	@HostBinding('class.overlay-hidden') get overlayHidden() {
		return this.hideOverlay;
	}
	@HostBinding('class.disable-transition') get disableTransition() {
		return this._disableTransition;
	}

	public size = ScreenShareCardSize.SMALL;
	private hideOverlay = false;
	private _disableTransition = false;
	private onMediaStreamAddSubscription: Subscription | undefined;
	public focused: FocusedMediaStream | undefined;

	constructor(
		private elementRef: ElementRef<HTMLElement>,
		public webrtcService: WebrtcService,
		public callService: CallService,
		public userService: UserService,
		public liveService: LiveService,
		public mediaService: MediaService
	) {}

	async ngOnInit() {
		this.webrtcService.onMediaStreamAdd.subscribe((focusedMediaStream) => {
			if (focusedMediaStream && !this.fullscreen) {
				this.focusMediaStream(focusedMediaStream.memberConnection, focusedMediaStream.typedMediaStream);
			}
		});

		document.onfullscreenchange = (ev) => {
			if (!document.fullscreenElement && this.size === ScreenShareCardSize.FULLSCREEN) {
				this.changeSize(ScreenShareCardSize.SMALL);
			} else if (document.fullscreenElement && this.size !== ScreenShareCardSize.FULLSCREEN) {
				this.changeSize(ScreenShareCardSize.FULLSCREEN);
			}
		};

		const mouseMove = fromEvent(document, 'mousemove');

		mouseMove.subscribe(() => {
			if (this.hideOverlay) {
				this.hideOverlay = false;
			}
		});

		mouseMove.pipe(debounceTime(3000)).subscribe(() => {
			this.hideOverlay = true;
		});
	}

	ngAfterViewInit() {
		if (this.player) {
			this.player.nativeElement.onloadeddata = (event) => {
				this._disableTransition = true;
				requestAnimationFrame(() => {
					this.videoReady.next();

					requestAnimationFrame(() => {
						this._disableTransition = false;
					});
				});
			};
		}
	}

	ngOnDestroy() {
		this.onMediaStreamAddSubscription?.unsubscribe();
	}

	focusMediaStream(memberConnection: BaseMemberConnection, typedMediaStream?: TypedMediaStream): void {
		if (typedMediaStream) {
			this.focused = { memberConnection, typedMediaStream };
		} else {
			let index = 0;
			if (this.focused) {
				const currentIndex = memberConnection.mediaStreams.indexOf(this.focused.typedMediaStream);
				if (currentIndex > -1) {
					index += currentIndex + 1;
				}
			}

			this.focused = memberConnection.mediaStreams[index]
				? { memberConnection, typedMediaStream: memberConnection.mediaStreams[index] }
				: undefined;
		}
	}

	unfocus() {
		if (this.fullscreen && this.focused) {
			this.focused = undefined;
		}
	}

	changeSize(size: ScreenShareCardSize) {
		const previousSize = this.size;
		this.size = size;

		this._disableTransition = true;

		requestAnimationFrame(() => {
			this.sizeChange.next({ previousSize, size });

			requestAnimationFrame(() => {
				this._disableTransition = false;
			});
		});
	}

	toggleFullscreen() {
		this._disableTransition = true;
		if (document.fullscreenElement) {
			document.exitFullscreen();
		} else {
			this.elementRef.nativeElement.requestFullscreen();
		}
	}
}
