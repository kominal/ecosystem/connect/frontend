import { Component, OnInit } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { LoadingService } from '../../services/loading/loading.service';

@Component({
	selector: 'app-loading-bar',
	templateUrl: './loading-bar.component.html',
	styleUrls: ['./loading-bar.component.scss'],
	animations: [
		trigger('loading', [
			state('open', style({ height: '4px', opacity: 1 })),
			state('closed', style({ height: '0px', opacity: 0 })),
			transition('open => closed', [animate('250ms')]),
			transition('closed => open', [animate('250ms')]),
		]),
	],
})
export class LoadingBarComponent {
	constructor(public loadingService: LoadingService) {}
}
