import { Component, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'app-text-dialog',
	templateUrl: './text-dialog.component.html',
	styleUrls: ['./text-dialog.component.scss'],
})
export class TextDialogComponent {
	public form: FormGroup;

	constructor(
		public dialogRef: MatDialogRef<TextDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { title: string; label: string; button: string; text?: string }
	) {
		this.form = new FormGroup({
			text: new FormControl(data.text, Validators.required),
		});
	}
}
