import { Component, OnInit } from '@angular/core';
import { MediaService } from '../../services/media/media.service';
import { CallService } from '../../services/call/call.service';

@Component({
	selector: 'app-call-controls',
	templateUrl: './call-controls.component.html',
	styleUrls: ['./call-controls.component.scss'],
})
export class CallControlsComponent implements OnInit {
	constructor(public mediaService: MediaService, public callService: CallService) {}

	ngOnInit(): void {}
}
