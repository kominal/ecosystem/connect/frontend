import { Component, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
	selector: 'app-decision-dialog',
	templateUrl: './decision-dialog.component.html',
	styleUrls: ['./decision-dialog.component.scss'],
})
export class DecisionDialogComponent {
	public form: FormGroup;

	constructor(
		public dialogRef: MatDialogRef<DecisionDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { title: string; button: string; preSelected: string; options: { name: string; value: string }[] }
	) {
		this.form = new FormGroup({
			decision: new FormControl(data.preSelected, Validators.required),
		});
	}
}
