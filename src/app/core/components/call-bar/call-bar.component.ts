import { Component, ViewChild, ChangeDetectorRef, OnInit, Injector } from '@angular/core';
import { CallService } from '../../services/call/call.service';
import { GroupService } from '../../services/group/group.service';
import { Overlay, OverlayPositionBuilder } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import { ContextMenuComponent, INJECTION_TOKEN } from '../context-menu/context-menu.component';
import { WebrtcService } from '../../services/webrtc/webrtc.service';
import { BaseMemberConnection } from '../../classes/memberconnections/baseMemberConnection';
import { CdkDrag } from '@angular/cdk/drag-drop';
import { ActionsService } from '../../services/actions/actions.service';
import { merge } from 'rxjs';
import { MediaService } from '../../services/media/media.service';
import { MatMenu } from '@angular/material/menu';
import { LocalMemberConnection } from '../../classes/memberconnections/localMemberConnection';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ActivatedRoute } from '@angular/router';

@UntilDestroy()
@Component({
	selector: 'app-call-bar',
	templateUrl: './call-bar.component.html',
	styleUrls: ['./call-bar.component.scss'],
})
export class CallBarComponent implements OnInit {
	@ViewChild('screenShareCard', { static: false, read: CdkDrag }) drag!: CdkDrag;
	@ViewChild('videoMenu', { static: true }) videoMenu?: MatMenu;
	@ViewChild('screenMenu', { static: true }) screenMenu?: MatMenu;

	selectedMember?: string;
	dragPosition = { x: 100, y: 100 };

	constructor(
		public callService: CallService,
		public webrtcService: WebrtcService,
		public activatedRoute: ActivatedRoute,
		private cdr: ChangeDetectorRef,
		private overlay: Overlay,
		private overlayPositionBuilder: OverlayPositionBuilder,
		private injector: Injector,
		private actionService: ActionsService,
		private groupService: GroupService,
		private mediaService: MediaService
	) {}

	async ngOnInit() {
		this.actionService.onSetBoundary.subscribe(() => {
			this.drag.boundaryElement = this.actionService.getBoundary();
			this.drag._dragRef.withBoundaryElement(this.drag.boundaryElement);

			requestAnimationFrame(() => {
				this.alignCard(this.getAlignmentPosition());
			});
		});

		await new Promise((resolve) => {
			const interval = setInterval(() => {
				const dragElement = this.drag.element.nativeElement;
				const dragBoundary = this.actionService.getBoundary();
				if (dragElement.clientWidth > 0 && dragElement.clientHeight > 0 && dragBoundary.clientWidth > 0 && dragBoundary.clientHeight > 0) {
					resolve();
					clearInterval(interval);
				}

				if (!this.drag) {
					throw new Error('Drag is undefined');
				}

				const { started, ended } = this.drag;

				started.subscribe(() => {
					this.drag.boundaryElement = this.actionService.getBoundary();
					this.drag._dragRef.withBoundaryElement(this.drag.boundaryElement);
				});

				ended.subscribe(() => {
					this.alignCard(this.getAlignmentPosition());
				});

				this.alignCard({ top: true, left: false });
			}, 50);
		});
		merge(this.groupService.groupUpdatedSubject.pipe(untilDestroyed(this)), this.mediaService.speakingSubject).subscribe(() => {
			this.cdr.detectChanges();
		});
	}

	onVideoReady() {
		this.alignCard(this.getAlignmentPosition());
	}

	onSizeChange(event: { previousSize: ScreenShareCardSize; size: ScreenShareCardSize }) {
		this.alignCard(this.getAlignmentPosition());
	}

	alignCard(position: AlignmentPosition) {
		const dragElement = this.drag.element.nativeElement;
		const { left, right, top, bottom } = this.actionService.getBoundary().getBoundingClientRect();

		let dragX = left + 16;
		let dragY = top + 16;

		if (!position.left) {
			dragX = right - dragElement.clientWidth - 16;
		}

		if (!position.top) {
			dragY = bottom - dragElement.clientHeight - 16;
		}

		this.dragPosition = { x: dragX, y: dragY };
	}

	getAlignmentPosition(): AlignmentPosition {
		const dragPosition = this.drag.getFreeDragPosition();
		const dragElement = this.drag.element.nativeElement;
		let top = true;
		let left = true;

		const { x, y, width, height } = this.actionService.getBoundary().getBoundingClientRect();

		if (dragPosition.y - y > y + height - (dragPosition.y + dragElement.clientHeight)) {
			top = false;
		}

		if (dragPosition.x - x > x + width - (dragPosition.x + dragElement.clientWidth)) {
			left = false;
		}

		return { top, left };
	}

	openContextMenu(memberConnection: BaseMemberConnection, event: MouseEvent) {
		if (memberConnection instanceof LocalMemberConnection) {
			return;
		}

		const positionStrategy = this.overlayPositionBuilder.global().left(`${event.clientX}px`).top(`${event.clientY}px`);

		const overlayRef = this.overlay.create({
			positionStrategy,
			hasBackdrop: true,
			backdropClass: 'cdk-overlay-transparent-backdrop',
			disposeOnNavigation: true,
		});

		overlayRef.backdropClick().subscribe(() => {
			overlayRef.dispose();
		});

		const weakMap = new WeakMap();

		weakMap.set(INJECTION_TOKEN, {
			memberConnection,
			remoteConnection: this.webrtcService.memberConnections.find(
				(m) => m.membershipCaller.connectionId === memberConnection.membershipCaller.connectionId
			),
		});
		overlayRef.attach(new ComponentPortal(ContextMenuComponent, null, new PortalInjector(this.injector, weakMap)));
		event.preventDefault();
	}
}

type AlignmentPosition = { top: boolean; left: boolean };
enum ScreenShareCardSize {
	SMALL,
	MEDIUM,
	FULLSCREEN,
}
