import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallBarComponent } from './call-bar.component';

describe('CallBarComponent', () => {
	let component: CallBarComponent;
	let fixture: ComponentFixture<CallBarComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CallBarComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CallBarComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
