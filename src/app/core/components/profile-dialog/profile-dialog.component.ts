import { Component, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Profile } from '@kominal/ecosystem-models/profile';

@Component({
	selector: 'app-profile-dialog',
	templateUrl: './profile-dialog.component.html',
	styleUrls: ['./profile-dialog.component.scss'],
})
export class ProfileDialogComponent {
	public form: FormGroup;

	constructor(
		public dialogRef: MatDialogRef<ProfileDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { title: string; label: string; button: string; profiles: Profile[] }
	) {
		this.form = new FormGroup({
			profile: new FormControl(data.profiles[0], Validators.required),
		});
	}
}
