import { Component } from '@angular/core';
import { LiveService } from '../../services/live/live.service';

@Component({
	selector: 'app-info-bar',
	templateUrl: './info-bar.component.html',
	styleUrls: ['./info-bar.component.scss'],
})
export class InfoBarComponent {
	constructor(public liveService: LiveService) {}
}
