import { Component, Inject, InjectionToken, OnInit, HostBinding } from '@angular/core';
import { DexieService } from '../../services/dexie/dexie.service';
import { WebrtcService } from '../../services/webrtc/webrtc.service';
import { CallService } from '../../services/call/call.service';
import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';
import { RemoteMemberConnection } from '../../classes/memberconnections/remoteMemberConnection';

export const INJECTION_TOKEN = new InjectionToken<Membership>('INJECTION_TOKEN');

@Component({
	selector: 'app-context-menu',
	templateUrl: './context-menu.component.html',
	styleUrls: ['./context-menu.component.scss'],
})
export class ContextMenuComponent implements OnInit {
	@HostBinding('class.mat-elevation-z4') elevation = true;

	public initVolume = 100;

	private volume = this.dexieService.volume();

	constructor(
		@Inject(INJECTION_TOKEN) public data: { memberConnection: RemoteMemberConnection },
		private dexieService: DexieService,
		private callService: CallService,
		private webrtcService: WebrtcService
	) {}

	async ngOnInit() {
		const { volume } = await this.volume.get(this.data.memberConnection.membershipCaller.userId);
		if (volume > 0) {
			this.initVolume = (volume || 1) * 100;
		}
	}

	async onChange(value: number) {
		const volume = value / 100;
		this.data.memberConnection.volume = volume;
		this.volume.put({ id: this.data.memberConnection.membershipCaller.userId, volume });

		if (!this.callService.group) {
			return;
		}

		const connectionIds = this.callService
			.getMembers(this.callService.group.id)
			.filter((member) => member.userId === this.data.memberConnection.membershipCaller.userId)
			.map((member) => member.connectionId);

		this.webrtcService.memberConnections
			.filter((connection) => connectionIds.includes(connection.membershipCaller.connectionId))
			.filter((connection) => connection instanceof RemoteMemberConnection)
			.map((connection) => connection as RemoteMemberConnection)
			.forEach((connection) => (connection.volume = volume));
	}

	sliderMoved(value: number) {
		this.data.memberConnection.volume = value / 100;
	}
}
