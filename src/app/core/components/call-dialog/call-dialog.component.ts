import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StatusService } from '../../services/status/status.service';
import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';
import { Group } from '@kominal/ecosystem-connect-models/group/group';

@Component({
	selector: 'app-call-dialog',
	templateUrl: './call-dialog.component.html',
	styleUrls: ['./call-dialog.component.scss'],
})
export class CallDialogComponent {
	constructor(
		public dialogRef: MatDialogRef<CallDialogComponent>,
		public statusService: StatusService,
		@Inject(MAT_DIALOG_DATA) public data: { group: Group; membership: Membership }
	) {}
}
