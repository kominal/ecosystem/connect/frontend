import { TestBed } from '@angular/core/testing';

import { GroupCacheService } from './group-cache.service';

describe('GroupCacheService', () => {
  let service: GroupCacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GroupCacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
