import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Group } from '@kominal/ecosystem-connect-models/group/group';
import { DexieService } from '../../services/dexie/dexie.service';
import { ChatHttpService } from '../../http/chat/chat.http.service';
import { LogService } from '../../services/log/log.service';
import { LoadingService } from '../../services/loading/loading.service';
import { LiveService } from '../../services/live/live.service';
import { GroupCryptoService } from '../../crypto/group/group-crypto.service';
import { SerialService } from '../../services/serial/serial.service';
import { filter } from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class GroupCacheService {
	public groupsSubject = new BehaviorSubject<Group[]>([]);

	private groupTable = this.dexieService.group();

	private cacheInSync = false;

	constructor(
		private dexieService: DexieService,
		private logService: LogService,
		private liveService: LiveService,
		private loadingService: LoadingService,
		private groupCryptoService: GroupCryptoService,
		private chatHttpService: ChatHttpService,
		private serialService: SerialService
	) {
		this.liveService.resetSubject.subscribe(() => (this.cacheInSync = false));

		this.liveService.eventSubject
			.pipe(filter(({ type }) => ['connect.group.joined', 'connect.group.updated'].includes(type)))
			.subscribe((event) => {
				this.cacheInSync = false;
				this.loadGroups();
			});
	}

	async loadGroups(): Promise<void> {
		return this.serialService.doOnce(async () => {
			return this.loadingService
				.doWhileLoading(async () => {
					if (this.groupsSubject.value.length === 0) {
						const groups = this.sortGroups(await this.groupTable.toArray());
						await this.groupCryptoService.setGroupStates(groups, 'VIEWABLE');
						this.groupsSubject.next(groups);
						await this.groupCryptoService.setGroupStates(groups, 'RESOLVED');
					}

					if (!this.cacheInSync) {
						let groups = await this.chatHttpService.getGroups();
						await this.groupTable.clear();
						await this.groupTable.bulkPut(groups);
						groups = this.sortGroups(await this.replaceWithCachedGroups(groups));
						await this.groupCryptoService.setGroupStates(groups, 'VIEWABLE');
						this.groupsSubject.next(groups);
						await this.groupCryptoService.setGroupStates(groups, 'RESOLVED');
						this.cacheInSync = true;
					}
				})
				.catch((e) => this.logService.handleError(e));
		});
	}

	async getGroup(groupId: string): Promise<Group> {
		return this.serialService.doOnce(async () => {
			return this.loadingService.doWhileLoading<Group>(async () => {
				await this.loadGroups();

				const group = this.groupsSubject.value.find((g) => g.id === groupId);

				if (!group) {
					throw 'error.group.notfound';
				}

				await this.groupCryptoService.setGroupState(group, 'RESOLVED');

				return group;
			});
		});
	}

	async replaceWithCachedGroups(groups: Group[]) {
		return Promise.all(groups.map((group) => this.replaceWithCachedGroup(group)));
	}

	async replaceWithCachedGroup(group: Group): Promise<Group> {
		const cachedGroup = this.groupsSubject.value.find((g) => g.id === group.id);
		if (cachedGroup) {
			if (group.updatedAt != cachedGroup.updatedAt) {
				await this.groupCryptoService.setGroupState(group, cachedGroup.cryptoState);
				Object.assign(cachedGroup, group);
			}
			return cachedGroup;
		}
		return group;
	}

	sortGroups(groups: Group[]): Group[] {
		return groups.sort((g1, g2) => {
			if (!g1.activityAt) {
				return 1;
			}
			if (!g2.activityAt) {
				return -1;
			}
			return g2.activityAt - g1.activityAt;
		});
	}
}
