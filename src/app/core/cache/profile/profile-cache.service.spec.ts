import { TestBed } from '@angular/core/testing';

import { ProfileCacheService } from './profile-cache.service';

describe('ProfileCacheService', () => {
  let service: ProfileCacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfileCacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
