import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserService } from '../../services/user/user.service';
import { ProfileCryptoService, ProfileDecryptionOptions } from '../../crypto/profile/profile-crypto.service';
import { LoadingService } from '../../services/loading/loading.service';
import { SerialService } from '../../services/serial/serial.service';
import { DexieService } from '../../services/dexie/dexie.service';
import { LogService } from '../../services/log/log.service';
import { Profile } from '@kominal/ecosystem-models/profile';
import { LiveService } from '../../services/live/live.service';
import { filter } from 'rxjs/operators';
import { UserHttpService } from '@kominal/ecosystem-angular-client';

@Injectable({
	providedIn: 'root',
})
export class ProfileCacheService {
	public profilesSubject = new BehaviorSubject<Profile[]>([]);
	public userProfilesSubject = new BehaviorSubject<Profile[]>([]);

	private profileTable = this.dexieService.profile();

	private cacheInSync = false;

	constructor(
		private dexieService: DexieService,
		private logService: LogService,
		private userService: UserService,
		private profileCryptoService: ProfileCryptoService,
		private serialService: SerialService,
		private loadingService: LoadingService,
		private userHttpService: UserHttpService,
		private liveService: LiveService
	) {
		this.profilesSubject.subscribe(async (profiles) => {
			if (profiles.length > 0) {
				const userId = await this.userService.getUserId();
				this.userProfilesSubject.next(profiles.filter((p) => p.userId === userId));
			} else {
				this.userProfilesSubject.next([]);
			}
		});

		this.liveService.eventSubject
			.pipe(filter(({ type }) => ['connect.profile.created', 'connect.profile.updated'].includes(type)))
			.subscribe((event) => this.loadUserProfiles());
	}

	async loadUserProfiles(): Promise<void> {
		return this.serialService.doOnce(async () => {
			return this.loadingService
				.doWhileLoading(async () => {
					const userId = await this.userService.getUserId();
					if (this.userProfilesSubject.value.length === 0) {
						const profiles = await this.profileTable.where('userId').equals(userId).toArray();
						await this.profileCryptoService.setProfileStates(profiles, 'DECRYPTED');
						this.addToCache(profiles);
					}

					if (!this.cacheInSync) {
						const profiles = await this.userHttpService.listProfiles();
						await this.profileTable.where('userId').equals(userId).delete();
						await this.profileTable.bulkPut(profiles);
						await this.profileCryptoService.setProfileStates(profiles, 'DECRYPTED');
						this.addToCache(await this.replaceWithCachedProfiles(profiles));
						this.cacheInSync = true;
					}
				})
				.catch((e) => this.logService.handleError(e));
		});
	}

	async getProfile(profileId: string, consistency: 'NOW' | 'EVENTUAL', decryptionOptions?: ProfileDecryptionOptions): Promise<Profile> {
		return this.serialService.doOnce(async () => {
			return this.loadingService.doWhileLoading<Profile>(async () => {
				const cachedProfile = this.profilesSubject.value.find((p) => p.id === profileId);
				if (cachedProfile) {
					return cachedProfile;
				}

				const profileFromTable = await this.profileTable.get(profileId);

				if (profileFromTable) {
					await this.profileCryptoService.setProfileState(profileFromTable, 'DECRYPTED', decryptionOptions);
					this.addToCache([profileFromTable]);
				}

				const promise = this.serialService.doOnce(async () => {
					return this.loadingService.doWhileLoading<Profile>(async () => {
						let profile = await this.userHttpService.get(profileId);
						await this.profileTable.delete(profileId);
						await this.profileTable.put(profile);
						await this.profileCryptoService.setProfileState(profile, 'DECRYPTED', decryptionOptions);
						profile = await this.replaceWithCachedProfile(profile);
						this.addToCache([profile]);
						return profile;
					});
				});

				return profileFromTable && consistency === 'EVENTUAL' ? profileFromTable : promise;
			});
		});
	}

	async replaceWithCachedProfiles(profiles: Profile[]) {
		return Promise.all(profiles.map((profile) => this.replaceWithCachedProfile(profile)));
	}

	async replaceWithCachedProfile(profile: Profile): Promise<Profile> {
		const cachedProfile = this.profilesSubject.value.find((p) => p.id === profile.id);
		if (cachedProfile) {
			if (profile.updatedAt != cachedProfile.updatedAt) {
				await this.profileCryptoService.setProfileState(profile, cachedProfile.cryptoState, { keyDecrypted: cachedProfile.keyDecrypted });
				Object.assign(cachedProfile, profile);
			}
			return cachedProfile;
		}
		return profile;
	}

	addToCache(profiles: Profile[]): void {
		let cache = [...this.profilesSubject.value];
		profiles.filter((profile) => !cache.includes(profile)).forEach((profile) => cache.push(profile));
		this.profilesSubject.next(cache);
	}
}
