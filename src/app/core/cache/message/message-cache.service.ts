import { Injectable } from '@angular/core';
import { Message } from '@kominal/ecosystem-connect-models/message/message';
import { DexieService } from '../../services/dexie/dexie.service';
import { LoadingService } from '../../services/loading/loading.service';
import { ChatHttpService } from '../../http/chat/chat.http.service';
import { SerialService } from '../../services/serial/serial.service';
import { v4 } from 'uuid';
import { LiveService } from '../../services/live/live.service';
import { filter } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class MessageCacheService {
	private messageTable = this.dexieService.message();

	private messages: Map<string, Message> = new Map();

	private latestMessageSubjects = new Map<string, BehaviorSubject<Message | undefined>>();

	constructor(
		private dexieService: DexieService,
		private loadingService: LoadingService,
		private chatHttpService: ChatHttpService,
		private serialService: SerialService,
		private liveService: LiveService
	) {
		this.liveService.eventSubject
			.pipe(filter(({ type }) => type === 'connect.message.created'))
			.subscribe(({ content }) => this.onNewMessage(content));
	}

	private async onNewMessage(content: any) {
		console.log(content);
		/*const { groupId, messageId } = content;

		try {
			const group = await this.groupCacheService.getGroup(groupId);

			if (!group.keyDecrypted) {
				throw 'KeyDecrypted is undefined';
			}

			const { id, keyDecrypted, nameDecrypted } = group;

			const message = await this.messageCacheService.getMessage(groupId, messageId);
			await this.messageCryptoService.setMessageState(message, keyDecrypted, 'TEXT');
			group.latestMessageId = message.id;
			group.latestMessage = message;
			if (Notification.permission === 'granted' || (await this.preferencesService.notificationEnabled())) {
				if (message.userId !== (await this.userService.getUserId())) {
					if (document.visibilityState === 'hidden') {
						const membership = await this.membershipCacheService.getMembership(id, message.userId, keyDecrypted, 'EVENTUAL');

						await this.messageCryptoService.setMessageState(message, keyDecrypted, 'TEXT');
						const notification = new Notification(nameDecrypted || '', {
							body: `${membership?.profile?.displayname}: ${message.textDecrypted || '📎'}`,
						});
						notification.onclick = () => {
							notification.close();
							this.ngZone.run(() => this.router.navigate([`/connect/group/${groupId}`]));
						};
					} else {
						await this.soundService.playMessage();
					}
				}
			}
		} catch (e) {
			this.logService.handleError(e);
		}*/
	}

	async getMessage(groupId: string, messageId: string): Promise<Message> {
		return this.serialService.doOnce(async () => {
			return this.loadingService.doWhileLoading<Message>(async () => {
				const messageFromCache = this.messages.get(messageId);

				if (messageFromCache) {
					return messageFromCache;
				}

				const messageFromTable = await this.messageTable.get(messageId);
				if (messageFromTable) {
					this.messages.set(messageId, messageFromTable);
					return messageFromTable;
				}

				const currentBatchLatestMessage = await this.getCurrentBatchLatestMessage(groupId);

				const { message, overflow } = await this.chatHttpService.getMessage(messageId, currentBatchLatestMessage?.id);
				message.batchId = overflow || !currentBatchLatestMessage ? v4() : currentBatchLatestMessage?.batchId;
				message.status = 'SENT';

				await this.messageTable.put(message);
				this.messages.set(messageId, message);

				return message;
			});
		});
	}

	async getPreviousMessages(groupId: string, earlistMessage?: Message): Promise<Message[]> {
		let dbMessages = await this.messageTable
			.where('groupId')
			.equals(groupId)
			.and((m) => (earlistMessage ? m.time < earlistMessage.time : true))
			.limit(20)
			.reverse()
			.sortBy('time');

		let loadMore = true;
		while (dbMessages.length < 20 && loadMore) {
			const currentBatchLatestMessage = await this.getCurrentBatchLatestMessage(groupId);

			const batchId = currentBatchLatestMessage?.batchId;

			const currentBatchEarliestMessage = await this.getCurrentBatchEarlistMessage(groupId, batchId);
			let previousBatchLatestMessage: Message | undefined;

			const { overflow, messages } = await this.chatHttpService.getPreviousMessages(
				groupId,
				currentBatchEarliestMessage?.id,
				previousBatchLatestMessage?.id
			);

			messages.forEach((message) => ((message.batchId = batchId), (message.status = 'SENT')));

			if (!overflow && previousBatchLatestMessage && previousBatchLatestMessage.batchId) {
				await this.messageTable.where(':batchId').equals(previousBatchLatestMessage.batchId).modify({ batchId });
			}

			await this.messageTable.bulkPut(messages);

			dbMessages = await this.messageTable
				.where('groupId')
				.equals(groupId)
				.and((m) => (earlistMessage ? m.time < earlistMessage.time : true))
				.limit(20)
				.reverse()
				.sortBy('time');

			loadMore = overflow;
		}

		return this.replaceWithCachedMessages(dbMessages);
	}

	async getNextMessages(groupId: string, lastMessage?: Message): Promise<Message[]> {
		let dbMessages = await this.messageTable
			.where('groupId')
			.equals(groupId)
			.and((m) => m.time > (lastMessage?.time || 0))
			.limit(20)
			.reverse()
			.sortBy('time');

		if (dbMessages.length < 20) {
			const currentBatchLatestMessage = await this.getCurrentBatchLatestMessage(groupId);

			const { overflow, messages } = await this.chatHttpService.getNextMessages(groupId, currentBatchLatestMessage?.id);

			const batchId = overflow || !currentBatchLatestMessage ? v4() : currentBatchLatestMessage?.id;

			messages.forEach((message) => ((message.batchId = batchId), (message.status = 'SENT')));

			await this.messageTable.bulkPut(messages);

			dbMessages = await this.messageTable
				.where('groupId')
				.equals(groupId)
				.and((m) => m.time > (lastMessage?.time || 0))
				.limit(20)
				.reverse()
				.sortBy('time');
		}

		return this.replaceWithCachedMessages(dbMessages.reverse());
	}

	private async getCurrentBatchEarlistMessage(groupId: string, batchId?: string): Promise<Message | undefined> {
		return (
			await this.messageTable
				.where('groupId')
				.equals(groupId)
				.and((m) => m.batchId === batchId)
				.reverse()
				.limit(1)
				.sortBy('-time')
		)[0];
	}

	async replaceWithCachedMessages(messages: Message[]): Promise<Message[]> {
		return Promise.all(messages.map((message) => this.replaceWithCachedMessage(message)));
	}

	async replaceWithCachedMessage(message: Message): Promise<Message> {
		if (!message.id) {
			return message;
		}

		const cachedMessage = this.messages.get(message.id);

		if (cachedMessage) {
			return cachedMessage;
		}

		this.messages.set(message.id, message);

		return message;
	}

	private async getCurrentBatchLatestMessage(groupId: string): Promise<Message | undefined> {
		return (await this.messageTable.where({ groupId }).reverse().limit(1).sortBy('time'))[0];
	}

	async addMessage(message: Message & { id: string }) {
		await this.messageTable.put(message);
		this.messages.set(message.id, message);
	}

	getLatestMessageSubject(groupId: string) {
		let latestMessageSubject = this.latestMessageSubjects.get(groupId);
		if (!latestMessageSubject) {
			latestMessageSubject = new BehaviorSubject<Message | undefined>(undefined);
			this.latestMessageSubjects.set(groupId, latestMessageSubject);
		}
		return latestMessageSubject;
	}

	/*async loadLatestMessage(groupId: string) {
		return this.serialService.doOnce(async () => {
			return this.loadingService.doWhileLoading(async () => {
				const messageFromCache = Array.from(this.messages.values())
					.filter((message) => message.groupId === groupId)
					.sort((a, b) => a.time - b.time)[0];

				if (messageFromCache) {
					return messageFromCache;
				}

				const messageFromTable = (await this.messageTable.where({ groupId }).limit(1).sortBy('-time'))[0];
				if (messageFromTable) {
					this.messages.set(messageId, messageFromTable);
					return messageFromTable;
				}

				const currentBatchLatestMessage = await this.getCurrentBatchLatestMessage(groupId);

				const { message, overflow } = await this.chatHttpService.getMessage(messageId, currentBatchLatestMessage?.id);
				message.batchId = overflow || !currentBatchLatestMessage ? v4() : currentBatchLatestMessage?.batchId;
				message.status = 'SENT';

				await this.messageTable.put(message);
				this.messages.set(messageId, message);

				return message;
			});
		});
		this.messages;
	}*/
}
