import { TestBed } from '@angular/core/testing';

import { MessageCacheService } from './message-cache.service';

describe('MessageCacheService', () => {
  let service: MessageCacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MessageCacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
