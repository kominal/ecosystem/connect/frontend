import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';
import { DexieService } from '../../services/dexie/dexie.service';
import { LoadingService } from '../../services/loading/loading.service';
import { SerialService } from '../../services/serial/serial.service';
import { MembershipCryptoService } from '../../crypto/membership/membership-crypto.service';
import { ChatHttpService } from '../../http/chat/chat.http.service';

@Injectable({
	providedIn: 'root',
})
export class MembershipCacheService {
	public membershipsSubjects = new Map<string, BehaviorSubject<Membership[]>>();
	public cacheInSync: string[] = [];

	private membershipTable = this.dexieService.membership();

	constructor(
		private dexieService: DexieService,
		private loadingService: LoadingService,
		private serialService: SerialService,
		private chatHttpService: ChatHttpService,
		private membershipCryptoService: MembershipCryptoService
	) {}

	getMembershipsSubject(groupId: string): BehaviorSubject<Membership[]> {
		let membershipsSubject = this.membershipsSubjects.get(groupId);
		if (!membershipsSubject) {
			membershipsSubject = new BehaviorSubject<Membership[]>([]);
			this.membershipsSubjects.set(groupId, membershipsSubject);
		}
		return membershipsSubject;
	}

	async loadMemberships(groupId: string, groupKey: string): Promise<void> {
		return this.serialService.doOnce(async () => {
			return this.loadingService.doWhileLoading(async () => {
				const membershipsSubject = this.getMembershipsSubject(groupId);

				if (membershipsSubject.value.length === 0) {
					const membershipsFromTable = await this.membershipTable.where({ groupId }).toArray();
					await this.membershipCryptoService.setMembershipStates(membershipsFromTable, groupKey, 'VIEWABLE');
					this.addToCache(membershipsSubject, membershipsFromTable);
				}

				if (!this.cacheInSync.includes(groupId)) {
					const memberships = await this.chatHttpService.getMembers(groupId);
					await this.membershipTable.where({ groupId }).delete();
					await this.membershipTable.bulkPut(memberships);
					await this.membershipCryptoService.setMembershipStates(memberships, groupKey, 'VIEWABLE');
					this.addToCache(membershipsSubject, await this.replaceWithCachedMemberships(membershipsSubject, memberships, groupKey));
					this.cacheInSync.push(groupId);
				}
			});
		});
	}

	async getMembership(groupId: string, userId: string, groupKey: string, consistency: 'NOW' | 'EVENTUAL'): Promise<Membership> {
		return this.serialService.doOnce(async () => {
			return this.loadingService.doWhileLoading<Membership>(async () => {
				const membershipProfile = this.membershipsSubjects.get(groupId)?.value.find((m) => m.userId === userId);
				if (membershipProfile) {
					return membershipProfile;
				}

				const membershipFromTable = await this.membershipTable.where({ userId, groupId }).first();

				if (membershipFromTable) {
					await this.membershipCryptoService.setMembershipState(membershipFromTable, groupKey, 'VIEWABLE');
					const membershipsSubject = this.getMembershipsSubject(groupId);
					membershipsSubject.next([...membershipsSubject.value, membershipFromTable]);
				}

				const promise = this.serialService.doOnce(async () => {
					return this.loadingService.doWhileLoading<Membership>(async () => {
						let membership = await this.chatHttpService.getMember(groupId, userId);
						await this.membershipTable.where({ userId, groupId }).delete();
						await this.membershipTable.put(membership);
						await this.membershipCryptoService.setMembershipState(membership, groupKey, 'VIEWABLE');
						const membershipsSubject = this.getMembershipsSubject(groupId);
						membership = await this.replaceWithCachedMembership(membershipsSubject, membership, groupKey);
						this.addToCache(membershipsSubject, [membership]);
						return membership;
					});
				});

				return membershipFromTable && consistency === 'EVENTUAL' ? membershipFromTable : promise;
			});
		});
	}

	async replaceWithCachedMemberships(membershipsSubject: BehaviorSubject<Membership[]>, memberships: Membership[], groupKey: string) {
		return Promise.all(memberships.map((membership) => this.replaceWithCachedMembership(membershipsSubject, membership, groupKey)));
	}

	async replaceWithCachedMembership(
		membershipsSubject: BehaviorSubject<Membership[]>,
		membership: Membership,
		groupKey: string
	): Promise<Membership> {
		const cachedMembership = membershipsSubject.value.find((m) => m.id === membership.id);
		if (cachedMembership) {
			if (membership.updatedAt != cachedMembership.updatedAt) {
				await this.membershipCryptoService.setMembershipState(membership, groupKey, cachedMembership.cryptoState);
				Object.assign(cachedMembership, membership);
			}
			return cachedMembership;
		}
		return membership;
	}

	addToCache(membershipsSubject: BehaviorSubject<Membership[]>, memberships: Membership[]): void {
		let cache = [...membershipsSubject.value];
		memberships.filter((membership) => !cache.includes(membership)).forEach((profile) => cache.push(profile));
		membershipsSubject.next(cache);
	}
}
