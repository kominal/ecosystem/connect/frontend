import { TestBed } from '@angular/core/testing';

import { MembershipCacheService } from './membership-cache.service';

describe('MembershipCacheService', () => {
  let service: MembershipCacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MembershipCacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
