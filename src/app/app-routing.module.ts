import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoreComponent } from './modules/core/core.component';
import { AuthenticationComponent } from './modules/authentication/authentication.component';
import { ConnectComponent } from './modules/connect/connect.component';
import { SettingsComponent } from './modules/settings/settings.component';
import { UserGuard } from './core/guards/user/user.guard';
import { GuestGuard } from './core/guards/guest/guest.guard';

const routes: Routes = [
	{
		path: 'core',
		component: CoreComponent,
		loadChildren: () => import('./modules/core/core.module').then((m) => m.CoreModule),
	},
	{
		path: 'authentication',
		component: AuthenticationComponent,
		canActivate: [GuestGuard],
		loadChildren: () => import('./modules/authentication/authentication.module').then((m) => m.AuthenticationModule),
	},
	{
		path: 'connect',
		component: ConnectComponent,
		canLoad: [UserGuard],
		loadChildren: () => import('./modules/connect/connect.module').then((m) => m.ConnectModule),
	},
	{
		path: 'settings',
		component: SettingsComponent,
		canLoad: [UserGuard],
		loadChildren: () => import('./modules/settings/settings.module').then((m) => m.SettingsModule),
	},
	{ path: '**', redirectTo: 'core' },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
