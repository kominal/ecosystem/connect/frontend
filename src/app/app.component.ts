import { Component, HostListener } from '@angular/core';
import { CallService } from './core/services/call/call.service';
import { LogService } from '@kominal/lib-angular-logging';
import { SoundService } from './core/services/sound/sound.service';
import { ElectronService } from './core/services/electron/electron.service';
import { environment } from 'src/environments/environment';
import { SwUpdate } from '@angular/service-worker';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from './core/services/user/user.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	constructor(
		public callService: CallService,
		public soundService: SoundService,
		public electronService: ElectronService,
		userService: UserService,
		logService: LogService,
		updates: SwUpdate,
		matSnackbar: MatSnackBar
	) {
		logService.info('Welcome to CONNECT');
		logService.info('You are a developer and would like to contribute to this project? Click here: https://gitlab.com/kominal/connect');

		updates.available.subscribe((event) => {
			const snackbarRef = matSnackbar.open('A new update is available', 'Update');
			snackbarRef.onAction().subscribe(async () => {
				await userService.logout();
				await updates.activateUpdate();
				console.log('Updating...');
			});
		});
	}

	@HostListener('document:click', ['$event'])
	onClick(event: any) {
		if (environment.isElectron && event.target.href) {
			event.preventDefault();
			this.electronService.getElectron().shell.openExternal(event.target.href);
		}
	}
}
