import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { GroupComponent } from './pages/group/group.component';
import { DetailsComponent } from './pages/details/details.component';

export const routesConnect: Routes = [
	{ path: 'home', component: HomeComponent },
	{ path: 'group/:groupId', component: GroupComponent },
	{ path: 'group/:groupId/details', component: DetailsComponent },
	{ path: '**', redirectTo: 'home' },
];

@NgModule({
	imports: [RouterModule.forChild(routesConnect)],
	exports: [RouterModule],
})
export class ConnectRoutingModule {}
