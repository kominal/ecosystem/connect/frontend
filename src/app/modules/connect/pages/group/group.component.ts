import { Component, ViewChild, ElementRef, ChangeDetectorRef, OnInit, ChangeDetectionStrategy, OnDestroy, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'src/app/core/services/message/message.service';
import { CallService } from 'src/app/core/services/call/call.service';
import { GroupService } from 'src/app/core/services/group/group.service';
import { LoadingService } from 'src/app/core/services/loading/loading.service';
import { LogService } from 'src/app/core/services/log/log.service';
import { UserService } from 'src/app/core/services/user/user.service';
import { MessageTree } from '../../../../core/classes/message-tree';
import { Position } from '../../../../core/classes/position';
import { ActionsService } from 'src/app/core/services/actions/actions.service';
import { StatusService } from 'src/app/core/services/status/status.service';
import { Subscription } from 'rxjs';
import { ProfileService } from 'src/app/core/services/profile/profile.service';
import { CryptoService } from 'src/app/core/services/crypto/crypto.service';
import { LiveService } from 'src/app/core/services/live/live.service';
import { GroupCacheService } from 'src/app/core/cache/group/group-cache.service';
import { DexieService } from 'src/app/core/services/dexie/dexie.service';
import { MessageCryptoService } from 'src/app/core/crypto/message/message-crypto.service';
import { Message } from '@kominal/ecosystem-connect-models/message/message';
import { Group } from '@kominal/ecosystem-connect-models/group/group';
import { filter } from 'rxjs/operators';

@Component({
	selector: 'app-group',
	templateUrl: './group.component.html',
	styleUrls: ['./group.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GroupComponent implements OnInit, OnDestroy {
	@ViewChild('viewport', { static: true }) viewport: ElementRef<HTMLElement> | undefined;
	@ViewChild('messageInput', { static: true }) messageInput: ElementRef<HTMLElement> | undefined;
	@ViewChild('lazyindicator', { static: true }) lazyindicator: ElementRef<HTMLElement> | undefined;

	private preferencesTable = this.dexieService.preferences();

	public group?: Group;

	public messageTree: MessageTree | undefined;

	private statusSubjectSubscription: Subscription;
	private onProfileUpdatedSubscription: Subscription;
	private onGroupUpdatedSubscription: Subscription;
	private onMembershipUpdatedSubscription: Subscription;
	private loading = false;

	private io = new IntersectionObserver(async (entries) => {
		if (this.messageTree && this.messageTree.earliestMessageVisable) {
			this.io.disconnect();
			return;
		} else if (this.loading || !entries[0].isIntersecting) {
			return;
		}

		this.loading = true;

		if (!this.viewport) {
			throw new Error('Viewport is undefined');
		}

		await this.loadOlderMessages();
		this.loading = false;
	});

	constructor(
		public callService: CallService,
		public groupService: GroupService,
		public router: Router,
		private activatedRoute: ActivatedRoute,
		private cdr: ChangeDetectorRef,
		private messageService: MessageService,
		private liveService: LiveService,
		private loadingService: LoadingService,
		private statusService: StatusService,
		private logService: LogService,
		private userService: UserService,
		private profileService: ProfileService,
		private actionService: ActionsService,
		private cryptoService: CryptoService,
		public groupCacheService: GroupCacheService,
		private dexieService: DexieService,
		private messageCryptoService: MessageCryptoService
	) {
		this.statusSubjectSubscription = this.statusService.statusSubject.subscribe(async (packet) => {
			// TODO check if update is for group member or self
			this.cdr.detectChanges();
		});

		this.liveService.eventSubject.pipe(filter(({ type }) => type === 'connect.message.created')).subscribe(async ({ content }) => {
			const { groupId } = content;

			if (groupId === this.group?.id) {
				this.loading = true;
				await this.messageTree?.loadLatest();
				this.loading = false;
			}
		});

		this.onProfileUpdatedSubscription = this.profileService.profileUpdatedSubject.subscribe(async (profileId) => {
			this.cdr.detectChanges();
		});

		this.onGroupUpdatedSubscription = this.groupService.groupUpdatedSubject.subscribe(async (groupId) => {
			this.cdr.detectChanges();
		});

		this.onMembershipUpdatedSubscription = this.groupService.membershipUpdatedSubject.subscribe(async () => {
			this.cdr.detectChanges();
		});
	}

	ngOnDestroy() {
		this.statusSubjectSubscription.unsubscribe();
		this.onProfileUpdatedSubscription.unsubscribe();
		this.onGroupUpdatedSubscription.unsubscribe();
		this.onMembershipUpdatedSubscription.unsubscribe();
	}

	ngOnInit() {
		this.actionService.setBoundary(this.viewport?.nativeElement);

		this.activatedRoute.params.subscribe(async ({ groupId }) => {
			if (!('ontouchstart' in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0)) {
				this.messageInput?.nativeElement.focus();
			}

			this.io.disconnect();

			try {
				this.group = await this.groupCacheService.getGroup(groupId);
				this.group.unreadMessages = 0;
				this.preferencesTable.put({ id: 'selectedGroup', value: groupId });
				this.cdr.detectChanges();
			} catch (e) {
				this.router.navigate(['/connect/home']);
				if ((await this.preferencesTable.get('selectedGroup')).value === groupId) {
					await this.preferencesTable.delete('selectedGroup');
				}
			}

			this.loadingService.doWithErrorHandlingWhileLoading(async () => {
				await this.buildMessageTree();
			});

			if (this.lazyindicator && this.messageTree && !this.messageTree.earliestMessageVisable) {
				this.io.observe(this.lazyindicator.nativeElement);
			}
		});

		this.callService.onMembersChange.subscribe(() => this.cdr.detectChanges());
	}

	async buildMessageTree() {
		if (!this.viewport) {
			throw new Error('Viewport is undefined');
		} else if (!this.group) {
			return;
		}

		this.messageTree = await this.messageService.getMessageTree(this.group, this.cdr, this.viewport);

		await this.messageTree.loadLatest();
		await this.messageTree.scrollToBottom();
	}

	async loadOlderMessages() {
		if (!this.messageTree) {
			return;
		}

		const subscription = this.loadingService.subscribe();
		try {
			if (!this.messageTree.earliestMessageVisable) {
				await this.messageTree.loadPrevious();
			}
		} catch (error) {
			this.logService.handleError(error);
		}
		subscription.unsubscribe();
	}

	async send(event: { text: string; files: File[] }) {
		this.loading = true;
		try {
			if (!this.group) {
				throw new Error('Group is undefined 4');
			} else if (!this.group.keyDecrypted) {
				throw new Error('Group is undefined 4');
			} else if (!this.messageTree) {
				throw new Error('MessageTree is undefined');
			}
			const { id, keyDecrypted } = this.group;

			this.groupService.markActivity(id);

			const time = Date.now();

			const textDecrypted = event.text;

			const message: Message = {
				id: undefined,
				time,
				groupId: id,
				userId: await this.userService.getUserId(),
				timeString: this.formatTime(time),
				status: 'SENDING',
				element: undefined,
				attachments: [],
				links: [],
				textDecrypted,
				textParsed: this.messageCryptoService.parseText(textDecrypted),
			};

			this.group.latestMessage = message;

			await this.messageTree.addSendingMessage(message);
			this.detectChangesAndPreservePosition();

			if (textDecrypted.replace(/(?:\r\n|\r|\n|\s)/g, '').length > 0) {
				const text = await this.cryptoService.encryptAES(textDecrypted, keyDecrypted);
				message.text = text;
			}

			if (!this.group) {
				throw 'Group is undefined';
			}

			await this.messageService.resolveLinks(this.group, message);
			this.detectChangesAndPreservePosition();

			await this.messageService.resolveAttachments(message, event.files);
			this.detectChangesAndPreservePosition();

			await this.messageCryptoService.setMessageState(message, keyDecrypted, 'ENCRYPTED');
			await this.messageService.send(this.group, message);
			this.detectChangesAndPreservePosition();
		} catch (error) {
			this.logService.handleError(error);
		}
		this.loading = false;
	}

	private detectChangesAndPreservePosition() {
		if (!this.viewport) {
			throw new Error('Viewport is undefined');
		}

		let position = new Position(this.viewport);
		this.cdr.detectChanges();
		position.restore();
	}

	private formatTime(time: number) {
		const date = new Date(time);

		let hours = '' + date.getHours();
		let minutes = '' + date.getMinutes();

		if (hours.length < 2) {
			hours = '0' + hours;
		}
		if (minutes.length < 2) {
			minutes = '0' + minutes;
		}
		return [hours, minutes].join(':');
	}
}
