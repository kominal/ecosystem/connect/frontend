import { Component, OnInit } from '@angular/core';
import { CallService } from 'src/app/core/services/call/call.service';
import { ActionsService } from 'src/app/core/services/actions/actions.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
	constructor(public callService: CallService, public actionsService: ActionsService) {}

	ngOnInit() {}
}
