import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GroupService } from 'src/app/core/services/group/group.service';
import { CallService } from 'src/app/core/services/call/call.service';
import { LoadingService } from 'src/app/core/services/loading/loading.service';
import { LogService } from 'src/app/core/services/log/log.service';
import { StatusService } from 'src/app/core/services/status/status.service';
import { UserService } from 'src/app/core/services/user/user.service';
import { ProfileService } from 'src/app/core/services/profile/profile.service';
import { ProfileDialogComponent } from 'src/app/core/components/profile-dialog/profile-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { DecisionDialogComponent } from 'src/app/core/components/decision-dialog/decision-dialog.component';
import { GroupCacheService } from 'src/app/core/cache/group/group-cache.service';
import { ProfileCacheService } from 'src/app/core/cache/profile/profile-cache.service';
import { MembershipCacheService } from 'src/app/core/cache/membership/membership-cache.service';
import { Membership } from '@kominal/ecosystem-connect-models/membership/membership';
import { Group } from '@kominal/ecosystem-connect-models/group/group';

@Component({
	selector: 'app-details',
	templateUrl: './details.component.html',
	styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
	@ViewChild('file', { static: true }) file: ElementRef<HTMLElement> | undefined;

	public group?: Group;

	public userId: string | undefined;

	constructor(
		private activatedRoute: ActivatedRoute,
		public groupService: GroupService,
		public groupCacheService: GroupCacheService,
		public loadingService: LoadingService,
		public logService: LogService,
		public callService: CallService,
		public userService: UserService,
		public profileService: ProfileService,
		public profileCacheService: ProfileCacheService,
		public membershipCacheService: MembershipCacheService,
		public statusService: StatusService,
		private matDialog: MatDialog,
		private router: Router
	) {}

	async ngOnInit() {
		this.activatedRoute.params.subscribe(async ({ groupId }) => {
			try {
				this.group = await this.groupCacheService.getGroup(groupId);
				if (this.group?.keyDecrypted) {
					this.membershipCacheService.loadMemberships(this.group.id, this.group.keyDecrypted);
				}
			} catch (e) {
				console.log(e);
				this.router.navigate(['/connect/home']);
			}
		});
		this.userId = await this.userService.getUserId();
	}

	formatFullTime(time: Date) {
		const date = new Date(time);

		const year = date.getFullYear();
		let month = '' + (date.getMonth() + 1);
		let day = '' + date.getDate();
		let hours = '' + date.getHours();
		let minutes = '' + date.getMinutes();

		if (month.length < 2) {
			month = '0' + month;
		}

		if (day.length < 2) {
			day = '0' + day;
		}

		if (hours.length < 2) {
			hours = '0' + hours;
		}

		if (minutes.length < 2) {
			minutes = '0' + minutes;
		}

		return [year, month, day].join('-') + ' ' + [hours, minutes].join(':');
	}

	async showAddMemberDialog() {
		if (!this.group) {
			throw 'Group is undefined';
		}

		await this.groupService.showAddMemberDialog(this.group);
		const subscribtion = this.loadingService.subscribe();
		try {
			if (this.group?.keyDecrypted) {
				//this.members = this.groupService.getMembers(this.group.id, this.group.keyDecrypted);
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		subscribtion.unsubscribe();
	}

	async showChangePermissionDialog(member: Membership) {
		const dialogRef = this.matDialog.open(DecisionDialogComponent, {
			data: {
				title: member.profile?.displaynameDecrypted,
				button: 'simple.ok',
				preSelected: member.role,
				options: [
					{ name: 'role.admin', value: 'ADMIN' },
					{ name: 'role.member', value: 'MEMBER' },
				],
			},
		});

		const response = await dialogRef.afterClosed().toPromise();

		if (response) {
			const subscribtion = this.loadingService.subscribe();
			try {
				await this.groupService.setRole(member.groupId, member.userId, response);
				this.logService.info(
					`The role of [user](userId=${member.userId}) was set to '${response}' for [group](groupId=${member.groupId}).`,
					'simple.roleUpdated'
				);
				member.role = response;
			} catch (error) {
				this.logService.error(
					`Failed updated role of [user](userId=${member.userId}) for [group](groupId=${member.groupId}).`,
					'error.roleUpdate'
				);
			}
			subscribtion.unsubscribe();
		}
	}

	async kick(member: Membership) {
		await this.groupService.kick(member.groupId, member.userId);
		const subscribtion = this.loadingService.subscribe();
		try {
			if (this.group?.keyDecrypted) {
				const { id, keyDecrypted } = this.group;
				//this.members = this.groupService.getMembers(id, keyDecrypted);
			}
		} catch (err) {}
		subscribtion.unsubscribe();
	}

	async selectAvatar() {
		if (!this.group || this.group.role !== 'ADMIN' || this.group.type !== 'GROUP' || this.group.left != null) {
			return;
		}
		this.file?.nativeElement.click();
	}

	async uploadAvatar(fileList?: FileList) {
		if (
			!this.group ||
			this.group.role !== 'ADMIN' ||
			this.group.type !== 'GROUP' ||
			this.group.left != null ||
			!fileList ||
			fileList.length === 0
		) {
			return;
		}

		const file = fileList.item(0);
		if (!file) {
			return;
		}

		const arrayBuffer = await new Response(file).arrayBuffer();
		const image = new Image();

		image.onload = () => {
			const canvas = document.createElement('canvas');
			const context = canvas.getContext('2d');
			canvas.height = 256;
			canvas.width = 256;
			context?.drawImage(image, 0, 0, 256, 256);

			canvas.toBlob(async (blob) => {
				if (!this.group) {
					throw 'Group is undefined';
				}
				this.groupService.changeAvatar(this.group, await new Response(blob).arrayBuffer());
			});
		};

		image.src = URL.createObjectURL(new Blob([arrayBuffer]));
	}

	async changeProfile() {
		await this.profileCacheService.loadUserProfiles();
		const profiles = this.profileCacheService.userProfilesSubject.value;

		if (profiles.length === 0) {
			this.logService.info('No profiles available.', 'error.profiles.missing');
			return;
		}

		const profile = await this.matDialog
			.open(ProfileDialogComponent, {
				data: { title: 'simple.changeProfile', label: 'simple.profile', button: 'simple.save', profiles },
			})
			.afterClosed()
			.toPromise();

		if (profile) {
			try {
				if (!this.group) {
					throw new Error('Group is undefined.');
				}
				if (!this.group.keyDecrypted) {
					throw new Error('Key is undefined.');
				}

				await this.groupService.changeProfile(this.group, profile);
				//await this.groupService.getMember(this.group.id, await this.userService.getUserId(), this.group.keyDecrypted, true);
				//this.members = this.groupService.getMembers(this.group.id, this.group.keyDecrypted);
			} catch (e) {
				this.logService.handleError(e);
			}
		}
	}
}
