import { Component, Input } from '@angular/core';
import { CallService } from 'src/app/core/services/call/call.service';
import { ActionsService } from 'src/app/core/services/actions/actions.service';
import { StatusService } from 'src/app/core/services/status/status.service';
import { LiveService } from 'src/app/core/services/live/live.service';
import { Group } from '@kominal/ecosystem-connect-models/group/group';

@Component({
	selector: 'app-group-bar',
	templateUrl: './group-bar.component.html',
	styleUrls: ['./group-bar.component.scss'],
})
export class GroupBarComponent {
	@Input() group: Group | undefined;

	constructor(
		public callService: CallService,
		public actionsService: ActionsService,
		public statusService: StatusService,
		public liveService: LiveService
	) {}
}
