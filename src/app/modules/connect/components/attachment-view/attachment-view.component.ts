import { Component, OnInit, Input } from '@angular/core';
import { StorageService } from 'src/app/core/services/storage/storage.service';
import { ElectronService } from 'src/app/core/services/electron/electron.service';
import { environment } from 'src/environments/environment';
import { Attachment } from '@kominal/ecosystem-connect-models/message/attachment';
import { Group } from '@kominal/ecosystem-connect-models/group/group';

@Component({
	selector: 'app-attachment-view',
	templateUrl: './attachment-view.component.html',
	styleUrls: ['./attachment-view.component.scss'],
})
export class AttachmentViewComponent implements OnInit {
	@Input() group?: Group;

	public isElectron = environment.isElectron;
	@Input() attachment: Attachment | undefined;

	public placeholder?: string;

	constructor(private storageService: StorageService, private electronService: ElectronService) {}

	ngOnInit(): void {
		if (!this.attachment) {
			throw new Error('Attachment can not be undefined.');
		}

		if (this.attachment.imageSizeDecrypted) {
			const canvas = document.createElement('canvas');
			canvas.width = this.attachment.imageSizeDecrypted.width;
			canvas.height = this.attachment.imageSizeDecrypted.height;
			this.placeholder = canvas.toDataURL();
		}
	}

	async download() {
		if (!this.attachment) {
			throw new Error('Attachment is undefined.');
		} else if (!this.group) {
			throw new Error('Group is undefined.');
		} else if (!this.group.keyDecrypted) {
			throw new Error('Key is undefined.');
		}

		if (!this.attachment.nameDecrypted || !this.attachment.typeDecrypted) {
			throw new Error('Attachment not yet decrypted.');
		}

		if (!this.attachment.storageId) {
			throw new Error('Attachment was not stored correctly.');
		}

		console.log('storage');
		const storage = await this.storageService.getFromGridFS(this.attachment.storageId, this.group.keyDecrypted, 'arraybuffer');
		console.log(storage);
		if (!storage) {
			throw new Error('Storage is undefined.');
		}
		const url = URL.createObjectURL(
			new File([storage.data], this.attachment.nameDecrypted, {
				type: this.attachment.typeDecrypted,
			})
		);

		if (this.isElectron) {
			const name = this.attachment?.nameDecrypted;
			const { BrowserWindow, app } = this.electronService.getElectron().remote;
			const window = BrowserWindow.getFocusedWindow();
			window.webContents.session.on('will-download', (event: any, item: any, webContents: any) => {
				item.setSavePath(
					this.electronService.getUnusedFilename().sync(this.electronService.getPath().join(app.getPath('downloads'), name))
				);

				item.on('updated', (event: any, state: any) => {
					if (state === 'interrupted') {
						console.log('Download is interrupted but can be resumed');
					} else if (state === 'progressing') {
						if (item.isPaused()) {
							console.log('Download is paused');
						} else {
							console.log(`Received bytes: ${item.getReceivedBytes()}`);
						}
					}
				});
				item.once('done', (event: any, state: any) => {
					if (state === 'completed') {
						console.log('Download successfully');
					} else {
						console.log(`Download failed: ${state}`);
					}
				});
			});
			window.webContents.downloadURL(url);
		} else {
			const a = document.createElement('a');
			a.download = this.attachment.nameDecrypted;
			a.href = url;
			a.style.display = 'none';
			document.body.append(a);
			a.click();
			document.body.removeChild(a);
			URL.revokeObjectURL(url);
		}
	}
}
