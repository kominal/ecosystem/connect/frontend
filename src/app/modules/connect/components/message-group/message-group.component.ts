import { Component, Input, HostBinding } from '@angular/core';
import { Message } from '@kominal/ecosystem-connect-models/message/message';
import { Group } from '@kominal/ecosystem-connect-models/group/group';
import { UserBranch } from 'src/app/core/classes/user-branch';

@Component({
	selector: 'app-message-group',
	templateUrl: './message-group.component.html',
	styleUrls: ['./message-group.component.scss'],
})
export class MessageGroupComponent {
	@Input() group?: Group;
	@Input() userBranch?: UserBranch;
	@HostBinding('class.right') get right() {
		return this.userBranch?.me;
	}

	trackByMessageId(index: number, item: Message) {
		return item.id;
	}
}
