import { Component, OnInit, Output, EventEmitter, HostListener, Input, ChangeDetectorRef } from '@angular/core';
import { PreferencesService } from 'src/app/core/services/preferences/preferences.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';

@Component({
	selector: 'app-input',
	templateUrl: './input.component.html',
	styleUrls: ['./input.component.scss'],
})
export class InputComponent implements OnInit {
	@Output() send = new EventEmitter<{ text: string; files: File[] }>();

	public darkTheme = false;
	public text = '';
	public files: { file: File; preview: SafeUrl }[] = [];
	public dragging = false;

	constructor(
		private preferencesService: PreferencesService,
		private domSanitizer: DomSanitizer,
		private httpClient: HttpClient,
		private cdr: ChangeDetectorRef
	) {}

	async ngOnInit(): Promise<void> {
		this.darkTheme = await this.preferencesService.isDarkTheme();
	}

	async addFiles(files: File[]) {
		for (const file of files) {
			const search = this.files.find(
				(value) => value.file.name === file.name && value.file.size === file.size && value.file.type === file.type
			);

			if (search) {
				this.files.splice(this.files.indexOf(search), 1);
			}

			let preview;
			if (file.type.startsWith('image/')) {
				preview = this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file));
			} else {
				const manifest = await this.httpClient
					.get<[{ icon: string; extensions: string[] }]>('./assets/images/file-icons/icon-manifest.json')
					.toPromise();
				const extension = file.name.substring(file.name.lastIndexOf('.') + 1);
				console.log(extension);

				let urlString = manifest.find(
					(iconScheme) => iconScheme.extensions.includes(extension) || iconScheme.extensions.includes(file.type)
				)?.icon;

				if (!urlString) {
					urlString = './assets/images/file-icons/default_file.svg';
				}
				preview = this.domSanitizer.bypassSecurityTrustUrl(urlString);
			}

			this.files.unshift({
				file,
				preview,
			});
		}
		this.cdr.detectChanges();
	}

	removeFile(index: number) {
		this.files.splice(index, 1);
	}

	onSubmit() {
		this.send.emit({ text: this.text, files: this.files.map((value) => value.file) });
		this.text = '';
		this.files = [];
	}

	onKeyup(event: KeyboardEvent) {
		if (event.key === 'Enter' && !event.shiftKey && this.valid) {
			this.onSubmit();
		}
	}

	onKeydown(event: KeyboardEvent) {
		if (event.key === 'Enter' && !event.shiftKey) {
			event.preventDefault();
		}
	}

	get valid() {
		return this.text.replace(/(?:\r\n|\r|\n|\s)/g, '').length > 0 || this.files.length > 0;
	}

	emojiBackground = (set: any, sheetsize: any) => {
		return 'assets/images/emoji64.png';
	};

	onEmojiClick(event: any) {
		this.text += event.emoji.native;
	}

	@HostListener('dragover', ['$event']) onDragOver(e: Event) {
		e.preventDefault();
		e.stopPropagation();
		this.dragging = true;
	}

	@HostListener('dragleave', ['$event']) public onDragLeave(e: Event) {
		e.preventDefault();
		e.stopPropagation();
		this.dragging = false;
	}

	@HostListener('drop', ['$event']) public ondrop(e: any) {
		e.preventDefault();
		e.stopPropagation();
		this.dragging = false;
		this.addFiles(e.dataTransfer.files);
	}
}
