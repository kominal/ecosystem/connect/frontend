import { Component, OnInit } from '@angular/core';
import { Status } from '@kominal/ecosystem-connect-models/status';
import { UserService } from 'src/app/core/services/user/user.service';
import { Router } from '@angular/router';
import { StatusService } from 'src/app/core/services/status/status.service';
import { GroupService } from 'src/app/core/services/group/group.service';
import { ProfileService } from 'src/app/core/services/profile/profile.service';

@Component({
	selector: 'app-sidenav-header',
	templateUrl: './sidenav-header.component.html',
	styleUrls: ['./sidenav-header.component.scss'],
})
export class SidenavHeaderComponent implements OnInit {
	states = Object.keys(Status);
	self = '';

	constructor(
		public userService: UserService,
		public profileService: ProfileService,
		public router: Router,
		public statusService: StatusService,
		public groupService: GroupService
	) {}

	async ngOnInit() {
		this.self = await this.userService.getUserId();
	}
}
