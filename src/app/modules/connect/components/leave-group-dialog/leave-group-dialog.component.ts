import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'app-leave-group-dialog',
	templateUrl: './leave-group-dialog.component.html',
	styleUrls: ['./leave-group-dialog.component.scss'],
})
export class LeaveGroupDialogComponent implements OnInit {
	constructor(public dialogRef: MatDialogRef<LeaveGroupDialogComponent>, @Inject(MAT_DIALOG_DATA) public left: boolean) {}

	ngOnInit(): void {}
}
