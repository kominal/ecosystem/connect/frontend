import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-date-entry',
	templateUrl: './date-entry.component.html',
	styleUrls: ['./date-entry.component.scss'],
})
export class DateEntryComponent implements OnInit {
	@Input() time: string | undefined;

	ngOnInit() {
		if (!this.time) {
			throw new Error('time is undefined');
		}
	}
}
