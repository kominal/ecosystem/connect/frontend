import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'app-password-confirm-dialog',
	templateUrl: './password-confirm-dialog.component.html',
	styleUrls: ['./password-confirm-dialog.component.scss'],
})
export class PasswordConfirmDialogComponent {
	public form: FormGroup;

	constructor(
		public dialogRef: MatDialogRef<PasswordConfirmDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { title: string; label: string; button: string }
	) {
		this.form = new FormGroup({
			password: new FormControl('', Validators.required),
		});
	}
}
