import { Component, OnInit, Input } from '@angular/core';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { Link } from '@kominal/ecosystem-connect-models/message/link';

@Component({
	selector: 'app-link-view',
	templateUrl: './link-view.component.html',
	styleUrls: ['./link-view.component.scss'],
})
export class LinkViewComponent implements OnInit {
	public type: 'YOUTUBE' | 'LINK' = 'LINK';

	public youtubeId: string | undefined;
	public youtubeUrl: SafeUrl | undefined;
	public youtubeThumpnail: SafeUrl | undefined;

	@Input() link: Link | undefined;

	public placeholder?: string;

	constructor(private domSanitizer: DomSanitizer) {}

	ngOnInit(): void {
		if (!this.link) {
			throw new Error('Link is undefined');
		}

		if (this.link.imageSizeDecrypted) {
			const canvas = document.createElement('canvas');
			canvas.width = this.link.imageSizeDecrypted.width;
			canvas.height = this.link.imageSizeDecrypted.height;
			this.placeholder = canvas.toDataURL();
		}

		/*const youtubeUrl = new RegExp(/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/gi).exec(
			this.link.url || ''
		);
		if (youtubeUrl) {
			this.type = 'YOUTUBE';
			this.youtubeId = youtubeUrl[1];
		}
		if (this.link.site) {
			this.site = this.unescape(this.link.site);
		}
		if (this.link.title) {
			this.title = this.unescape(this.link.title);
		}
		if (this.link.description) {
			this.description = this.unescape(this.link.description);
		}*/
	}

	unescape(input: string): string {
		return input
			.replace(/&#39;/g, "'")
			.replace(/&quot;/g, '"')
			.replace(/&amp;/g, '&');
	}

	loadYoutubeVideo() {
		this.youtubeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(`https://www.youtube.com/embed/${this.youtubeId}`);
	}

	resizeIframe(obj: any) {
		obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
	}
}
