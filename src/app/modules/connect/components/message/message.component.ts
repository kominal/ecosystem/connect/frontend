import { Component, Input, ElementRef, OnInit, HostBinding } from '@angular/core';
import { Message } from '@kominal/ecosystem-connect-models/message/message';
import { Group } from '@kominal/ecosystem-connect-models/group/group';

@Component({
	selector: 'app-message',
	templateUrl: './message.component.html',
	styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {
	@Input() group?: Group;
	@Input() message?: Message;
	@Input() right = false;
	@HostBinding('class.right') get rightBinding() {
		return this.right;
	}

	constructor(private element: ElementRef<MessageComponent>) {}

	ngOnInit() {
		if (this.message) {
			this.message.element = this.element;
		}
	}
}
