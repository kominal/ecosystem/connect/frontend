import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'app-select-screen-dialog',
	templateUrl: './select-screen-dialog.component.html',
	styleUrls: ['./select-screen-dialog.component.scss'],
})
export class SelectScreenDialogComponent implements OnInit {
	constructor(public dialogRef: MatDialogRef<SelectScreenDialogComponent>, @Inject(MAT_DIALOG_DATA) public sources: any[]) {}

	ngOnInit(): void {}
}
