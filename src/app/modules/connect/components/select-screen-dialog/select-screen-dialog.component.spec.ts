import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectScreenDialogComponent } from './select-screen-dialog.component';

describe('SelectScreenDialogComponent', () => {
	let component: SelectScreenDialogComponent;
	let fixture: ComponentFixture<SelectScreenDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [SelectScreenDialogComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SelectScreenDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
