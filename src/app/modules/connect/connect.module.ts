import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConnectRoutingModule } from './connect-routing.module';
import { ConnectComponent } from './connect.component';
import { HomeComponent } from './pages/home/home.component';

import { TranslateModule } from '@ngx-translate/core';
import { GroupComponent } from './pages/group/group.component';
import { DateEntryComponent } from './components/date-entry/date-entry.component';
import { DetailsComponent } from './pages/details/details.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MaterialModule } from 'src/app/core/classes/material.module';
import { LeaveGroupDialogComponent } from './components/leave-group-dialog/leave-group-dialog.component';
import { GroupBarComponent } from './components/group-bar/group-bar.component';
import { SidenavHeaderComponent } from './components/sidenav-header/sidenav-header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectScreenDialogComponent } from './components/select-screen-dialog/select-screen-dialog.component';
import { LinkViewComponent } from './components/link-view/link-view.component';
import { AttachmentViewComponent } from './components/attachment-view/attachment-view.component';
import { MessageGroupComponent } from './components/message-group/message-group.component';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { MessageComponent } from './components/message/message.component';
import { InputComponent } from './components/input/input.component';
import { SharedModule } from 'src/app/core/shared.module';
import { PasswordConfirmDialogComponent } from './components/password-confirm-dialog/password-confirm-dialog.component';

@NgModule({
	declarations: [
		ConnectComponent,
		DateEntryComponent,
		DetailsComponent,
		GroupComponent,
		HomeComponent,
		LeaveGroupDialogComponent,
		GroupBarComponent,
		SidenavHeaderComponent,
		SelectScreenDialogComponent,
		LinkViewComponent,
		AttachmentViewComponent,
		MessageGroupComponent,
		MessageComponent,
		InputComponent,
		PasswordConfirmDialogComponent,
	],
	imports: [
		CommonModule,
		ConnectRoutingModule,
		TranslateModule,
		ScrollingModule,
		MaterialModule,
		ReactiveFormsModule,
		FormsModule,
		PickerModule,
		SharedModule,
	],
})
export class ConnectModule {}
