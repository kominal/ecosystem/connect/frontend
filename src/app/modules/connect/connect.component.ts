import { Component, OnInit, ViewChild, OnDestroy, ElementRef, ChangeDetectorRef } from '@angular/core';
import { LayoutService } from 'src/app/core/services/layout/layout.service';
import { MatSidenav } from '@angular/material/sidenav';
import { GroupService } from 'src/app/core/services/group/group.service';
import { Router } from '@angular/router';
import { ActionsService } from 'src/app/core/services/actions/actions.service';
import { DexieService } from 'src/app/core/services/dexie/dexie.service';
import { StatusService } from 'src/app/core/services/status/status.service';
import { CallService } from 'src/app/core/services/call/call.service';
import { ProfileService } from 'src/app/core/services/profile/profile.service';
import { Subscription } from 'rxjs';
import { GroupCacheService } from 'src/app/core/cache/group/group-cache.service';

@Component({
	selector: 'app-connect',
	templateUrl: './connect.component.html',
	styleUrls: ['./connect.component.scss'],
})
export class ConnectComponent implements OnInit, OnDestroy {
	@ViewChild('sidenav', { static: true }) sidenav: MatSidenav | undefined;
	@ViewChild('sidenav', { static: true, read: ElementRef }) sidenavRef: ElementRef | undefined;

	openDrag = false;
	closeDrag = false;
	closeOffset = 0;

	private onGroupUpdatedSubscription: Subscription;

	constructor(
		private dexieService: DexieService,
		private actionsService: ActionsService,
		private router: Router,
		public layoutService: LayoutService,
		public groupService: GroupService,
		public groupCacheService: GroupCacheService,
		public statusService: StatusService,
		public profileService: ProfileService,
		private cdr: ChangeDetectorRef,
		public callService: CallService
	) {
		this.groupCacheService.loadGroups();
		this.router.events.subscribe(() => {
			if (this.layoutService.isXSmall()) {
				if (!this.sidenav) {
					throw new Error('Sidenav is undefined');
				}

				this.sidenav.close();
			}
		});

		this.onGroupUpdatedSubscription = this.groupService.groupUpdatedSubject.subscribe(async (groupId) => {
			this.cdr.detectChanges();
		});
	}

	async ngOnInit() {
		const selectedGroup = await this.dexieService.preferences().get('selectedGroup');

		if (selectedGroup && !this.router.url.startsWith('/connect/group/')) {
			this.router.navigate([`/connect/group/${selectedGroup.value}`]);
		}

		this.actionsService.sidenav = this.sidenav;
	}

	ngOnDestroy(): void {
		this.actionsService.sidenav = undefined;
		this.onGroupUpdatedSubscription.unsubscribe();
	}
}
