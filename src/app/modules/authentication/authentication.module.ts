import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { AuthenticationComponent } from './authentication.component';
import { TranslateModule } from '@ngx-translate/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/core/classes/material.module';

@NgModule({
	declarations: [AuthenticationComponent, LoginComponent, RegisterComponent],
	imports: [CommonModule, AuthenticationRoutingModule, TranslateModule, FormsModule, ReactiveFormsModule, MaterialModule],
})
export class AuthenticationModule {}
