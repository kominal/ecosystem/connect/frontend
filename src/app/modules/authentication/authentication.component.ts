import { Component } from '@angular/core';
import { LoadingService } from 'src/app/core/services/loading/loading.service';
import { PreferencesService } from 'src/app/core/services/preferences/preferences.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-authentication',
	templateUrl: './authentication.component.html',
	styleUrls: ['./authentication.component.scss'],
})
export class AuthenticationComponent {
	constructor(
		public preferencesService: PreferencesService,
		public loadingService: LoadingService,
		public translateService: TranslateService
	) {}
}
