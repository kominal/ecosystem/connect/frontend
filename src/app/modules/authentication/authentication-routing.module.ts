import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';

export const routesAuthentication: Routes = [
	{ path: 'register', component: RegisterComponent },
	{ path: 'login', component: LoginComponent },
	{ path: '**', redirectTo: 'login' },
];

@NgModule({
	imports: [RouterModule.forChild(routesAuthentication)],
	exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
