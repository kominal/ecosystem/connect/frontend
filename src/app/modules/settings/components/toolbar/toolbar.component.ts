import { Component, OnInit, Input } from '@angular/core';
import { CallService } from 'src/app/core/services/call/call.service';
import { ActionsService } from 'src/app/core/services/actions/actions.service';

@Component({
	selector: 'app-toolbar',
	templateUrl: './toolbar.component.html',
	styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent {
	@Input() title: string | undefined;

	constructor(public callService: CallService, public actionsService: ActionsService) {}
}
