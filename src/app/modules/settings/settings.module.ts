import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './pages/home/home.component';
import { NotificationsComponent } from './pages/notifications/notifications.component';

import { TranslateModule } from '@ngx-translate/core';
import { SessionsComponent } from './pages/sessions/sessions.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';

import { MaterialModule } from 'src/app/core/classes/material.module';
import { AppearanceComponent } from './pages/appearance/appearance.component';
import { SidenavHeaderComponent } from './components/sidenav-header/sidenav-header.component';
import { LocalComponent } from './pages/local/local.component';
import { SettingsComponent } from './settings.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AboutComponent } from './pages/about/about.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { ProfilesComponent } from './pages/profiles/profiles.component';
import { SharedModule } from 'src/app/core/shared.module';
import { AccountComponent } from './pages/account/account.component';

@NgModule({
	declarations: [
		SettingsComponent,
		HomeComponent,
		NotificationsComponent,
		SessionsComponent,
		ChangePasswordComponent,
		AppearanceComponent,
		SidenavHeaderComponent,
		LocalComponent,
		AboutComponent,
		ToolbarComponent,
		ProfilesComponent,
		AccountComponent,
	],
	imports: [CommonModule, SettingsRoutingModule, TranslateModule, MaterialModule, FormsModule, ReactiveFormsModule, SharedModule],
})
export class SettingsModule {}
