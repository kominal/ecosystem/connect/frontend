import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { ActionsService } from 'src/app/core/services/actions/actions.service';
import { LayoutService } from 'src/app/core/services/layout/layout.service';
import { CallService } from 'src/app/core/services/call/call.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-settings',
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit, OnDestroy {
	@ViewChild('sidenav', { static: true }) sidenav: MatSidenav | undefined;

	constructor(
		public actionsService: ActionsService,
		public layoutService: LayoutService,
		public callService: CallService,
		public router: Router
	) {
		this.router.events.subscribe(() => {
			if (this.layoutService.isXSmall()) {
				if (!this.sidenav) {
					throw new Error('Sidenav is undefined');
				}

				this.sidenav.close();
			}
		});
	}

	async ngOnInit() {
		this.actionsService.sidenav = this.sidenav;
	}

	ngOnDestroy(): void {
		this.actionsService.sidenav = undefined;
	}
}
