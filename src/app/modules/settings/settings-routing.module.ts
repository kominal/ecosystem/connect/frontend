import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { NotificationsComponent } from './pages/notifications/notifications.component';
import { SessionsComponent } from './pages/sessions/sessions.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { AppearanceComponent } from './pages/appearance/appearance.component';
import { LocalComponent } from './pages/local/local.component';
import { AboutComponent } from './pages/about/about.component';
import { ProfilesComponent } from './pages/profiles/profiles.component';
import { AccountComponent } from './pages/account/account.component';

const routes: Routes = [
	{ path: 'home', component: HomeComponent },
	{ path: 'profiles', component: ProfilesComponent },
	{ path: 'appearance', component: AppearanceComponent },
	{ path: 'notifications', component: NotificationsComponent },
	{ path: 'account', component: AccountComponent },
	{ path: 'sessions', component: SessionsComponent },
	{ path: 'changePassword', component: ChangePasswordComponent },
	{ path: 'local', component: LocalComponent },
	{ path: 'about', component: AboutComponent },
	{ path: '**', redirectTo: 'home' },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class SettingsRoutingModule {}
