import { Component, ViewChild, ElementRef } from '@angular/core';
import { LogService } from 'src/app/core/services/log/log.service';
import { ActionsService } from 'src/app/core/services/actions/actions.service';
import { CallService } from 'src/app/core/services/call/call.service';
import { ProfileService } from 'src/app/core/services/profile/profile.service';
import { StatusService } from 'src/app/core/services/status/status.service';
import { UserService } from 'src/app/core/services/user/user.service';
import { ProfileCacheService } from 'src/app/core/cache/profile/profile-cache.service';
import { Profile } from '@kominal/ecosystem-models/profile';

@Component({
	selector: 'app-profiles',
	templateUrl: './profiles.component.html',
	styleUrls: ['./profiles.component.scss'],
})
export class ProfilesComponent {
	@ViewChild('file', { static: true }) file: ElementRef<HTMLElement> | undefined;

	private workingProfile: Profile | undefined;

	constructor(
		public callService: CallService,
		public actionsService: ActionsService,
		public logService: LogService,
		public profileService: ProfileService,
		public userService: UserService,
		public statusService: StatusService,
		public profileCacheService: ProfileCacheService
	) {
		this.profileCacheService.loadUserProfiles();
	}

	async selectAvatar(profile: Profile) {
		this.workingProfile = profile;
		this.file?.nativeElement.click();
	}

	async uploadAvatar(fileList?: FileList) {
		if (!fileList || fileList.length === 0) {
			return;
		}

		const file = fileList.item(0);
		if (!file) {
			return;
		}

		const arrayBuffer = await new Response(file).arrayBuffer();
		const image = new Image();

		image.onload = () => {
			const canvas = document.createElement('canvas');
			const context = canvas.getContext('2d');
			canvas.height = 256;
			canvas.width = 256;
			context?.drawImage(image, 0, 0, 256, 256);

			canvas.toBlob(async (blob) => {
				image.src = '';
				if (this.workingProfile) {
					this.profileService.changeAvatar(this.workingProfile, await new Response(blob).arrayBuffer());
				}
			});
		};

		image.src = URL.createObjectURL(new Blob([arrayBuffer]));
	}
}
