import { Component, OnInit } from '@angular/core';
import { PreferencesService } from 'src/app/core/services/preferences/preferences.service';

@Component({
	selector: 'app-appearance',
	templateUrl: './appearance.component.html',
	styleUrls: ['./appearance.component.scss'],
})
export class AppearanceComponent implements OnInit {
	darkTheme = false;

	constructor(public preferencesService: PreferencesService) {}

	async ngOnInit() {
		this.darkTheme = await this.preferencesService.isDarkTheme();
	}
}
