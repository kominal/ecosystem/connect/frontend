import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/core/services/user/user.service';

@Component({
	selector: 'app-change-password',
	templateUrl: './change-password.component.html',
	styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent {
	form: FormGroup;

	constructor(formBuilder: FormBuilder, private userService: UserService, private translateService: TranslateService) {
		this.form = formBuilder.group({
			password: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
			newPassword1: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
			newPassword2: new FormControl('', [
				Validators.required,
				Validators.minLength(5),
				Validators.maxLength(50),
				this.passwordMatcher.bind(this),
			]),
		});
	}

	onSubmit() {
		const password = this.form.get('password');
		const newPassword2 = this.form.get('newPassword2');

		if (!password) {
			throw new Error('Password is undefined.');
		}

		if (!newPassword2) {
			throw new Error('NewPassword2 is undefined.');
		}

		this.userService.changePassword(password.value, newPassword2.value);
	}

	getPasswordErrors(): string {
		const field = this.form.get('password');

		if (!field) {
			throw new Error('Field is null');
		}

		if (field.hasError('required')) {
			return this.translateService.instant('error.required');
		} else if (field.hasError('minlength')) {
			return this.translateService.instant('error.minLength', { minLength: field.getError('minlength').requiredLength });
		} else if (field.hasError('maxlength')) {
			return this.translateService.instant('error.maxLength', { maxLength: field.getError('maxlength').requiredLength });
		}
		return '';
	}

	getNewPassword1Errors(): string {
		const field = this.form.get('newPassword1');

		if (!field) {
			throw new Error('Field is null');
		}

		if (field.hasError('required')) {
			return this.translateService.instant('error.required');
		} else if (field.hasError('minlength')) {
			return this.translateService.instant('error.minLength', { minLength: field.getError('minlength').requiredLength });
		} else if (field.hasError('maxlength')) {
			return this.translateService.instant('error.maxLength', { maxLength: field.getError('maxlength').requiredLength });
		}
		return '';
	}

	getNewPassword2Errors(): string {
		const field = this.form.get('newPassword2');

		if (!field) {
			throw new Error('Field is null');
		}

		if (field.hasError('required')) {
			return this.translateService.instant('error.required');
		} else if (field.hasError('minlength')) {
			return this.translateService.instant('error.minLength', { minLength: field.getError('minlength').requiredLength });
		} else if (field.hasError('maxlength')) {
			return this.translateService.instant('error.maxLength', { maxLength: field.getError('maxlength').requiredLength });
		} else if (field.hasError('passwordMissmatch')) {
			return this.translateService.instant('error.passwordMissmatch');
		}
		return '';
	}

	private passwordMatcher(control: FormControl): { passwordMissmatch: boolean } {
		return { passwordMissmatch: this.form && control.value !== this.form.controls.newPassword1.value };
	}
}
