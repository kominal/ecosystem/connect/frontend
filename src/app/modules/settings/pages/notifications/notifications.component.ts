import { Component, OnInit } from '@angular/core';
import { PushSubscription as ConnectPushSubscription } from '@kominal/ecosystem-connect-models/pushsubscription/pushsubscription';
import { CallService } from 'src/app/core/services/call/call.service';
import { ActionsService } from 'src/app/core/services/actions/actions.service';
import { environment } from 'src/environments/environment';
import { PreferencesService } from 'src/app/core/services/preferences/preferences.service';
import { SwPush } from '@angular/service-worker';
import { timeout, take } from 'rxjs/operators';
import { LogService } from 'src/app/core/services/log/log.service';
import { PushService } from 'src/app/core/services/push/push.service';

@Component({
	selector: 'app-notifications',
	templateUrl: './notifications.component.html',
	styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit {
	public swPushEnabled = false;
	public notificationsEnabled = false;

	public subscriptions: ConnectPushSubscription[] = [];

	public isElectron = environment.isElectron;

	constructor(
		public pushService: PushService,
		public callService: CallService,
		public actionsService: ActionsService,
		public preferencesService: PreferencesService,
		public logService: LogService,
		public swPush: SwPush
	) {}

	async ngOnInit() {
		this.subscriptions = await this.pushService.getSubscriptions();
		if (this.isElectron) {
			this.notificationsEnabled = await this.preferencesService.notificationEnabled();
			this.swPushEnabled = true;
			this.swPush.subscription.pipe(take(1)).subscribe((p) => console.log(p));
		} else {
			this.swPushEnabled = this.swPush.isEnabled;
		}
	}

	async toggleNotifications(enabled: boolean) {
		if (this.isElectron) {
			this.preferencesService.toggleNotifications(enabled);
		} else {
			if (enabled) {
				await this.enablePushNotifications();
			} else {
				try {
					await this.swPush.unsubscribe();
				} catch (e) {
					this.logService.handleError(e);
				}
			}
		}
	}

	async enablePushNotifications() {
		await this.pushService.subscribeToNotifications();
		this.subscriptions = await this.pushService.getSubscriptions();
	}

	async remove(id: string) {
		await this.pushService.remove(id);
		this.subscriptions = await this.pushService.getSubscriptions();
	}
}
