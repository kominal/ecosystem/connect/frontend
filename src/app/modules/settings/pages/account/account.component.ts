import { Component } from '@angular/core';
import { UserService } from 'src/app/core/services/user/user.service';

@Component({
	selector: 'app-account',
	templateUrl: './account.component.html',
	styleUrls: ['./account.component.scss'],
})
export class AccountComponent {
	constructor(public userService: UserService) {}
}
