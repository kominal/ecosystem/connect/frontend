import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ElectronService } from 'src/app/core/services/electron/electron.service';
import { PreferencesService } from 'src/app/core/services/preferences/preferences.service';
import { TranslateService } from '@ngx-translate/core';
import { CallService } from 'src/app/core/services/call/call.service';
import { MediaService } from 'src/app/core/services/media/media.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-local',
	templateUrl: './local.component.html',
	styleUrls: ['./local.component.scss'],
})
export class LocalComponent implements OnInit, OnDestroy {
	public isElectron = environment.isElectron;
	public microphoneSensitivity = 0.5;

	public m = Math;

	autoStart = false;

	private speakingSubscription?: Subscription;

	constructor(
		private electronService: ElectronService,
		public translateService: TranslateService,
		public preferencesService: PreferencesService,
		private cdr: ChangeDetectorRef,
		public callService: CallService,
		public mediaService: MediaService
	) {
		if (this.isElectron) {
			const electron = this.electronService.getElectron();
			this.autoStart = electron.remote.app.getLoginItemSettings(this.buildLoginItemSettings()).openAtLogin;
			console.log(electron.remote.app.getLoginItemSettings(this.buildLoginItemSettings()));
		}
	}

	async ngOnInit() {
		this.microphoneSensitivity = await this.preferencesService.getMicrophoneSensitivity();
		this.speakingSubscription = this.mediaService.speakingSubject.subscribe(() => {
			this.cdr.detectChanges();
		});
	}

	async ngOnDestroy() {
		this.speakingSubscription?.unsubscribe();
		if (!this.callService.group) {
			this.mediaService.stopMicrophoneStream();
		}
	}

	async toggleMicrophone() {
		if (!this.callService.group && this.mediaService.microphoneStreamSubject.value) {
			this.mediaService.stopMicrophoneStream();
		} else {
			this.mediaService.toggleMicrophone();
		}
	}

	toggleAutoStart(enabled: boolean) {
		const electron = this.electronService.getElectron();
		electron.remote.app.setLoginItemSettings({
			...this.buildLoginItemSettings(),
			openAtLogin: enabled,
		});
	}

	buildLoginItemSettings() {
		const electron = this.electronService.getElectron();
		const platform = electron.remote.process.platform;
		if (platform === 'win32') {
			return {
				path: electron.remote.process.env.PORTABLE_EXECUTABLE_FILE,
			};
		}
		return undefined;
	}

	async onChange(value: number) {
		this.microphoneSensitivity = value;
		await this.preferencesService.setMicrophoneSensitivity(value);
	}

	async sliderMoved(value: number) {
		this.microphoneSensitivity = value;
		await this.preferencesService.updateMicrophoneSensitivity();
	}
}
