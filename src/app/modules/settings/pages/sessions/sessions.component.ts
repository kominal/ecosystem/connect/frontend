import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user/user.service';
import { Session } from '@kominal/ecosystem-models/session';

@Component({
	selector: 'app-sessions',
	templateUrl: './sessions.component.html',
	styleUrls: ['./sessions.component.scss'],
})
export class SessionsComponent implements OnInit {
	public sessions: Session[] = [];

	constructor(public userService: UserService) {}

	async ngOnInit() {
		this.sessions = await this.userService.getSessions();
	}

	formatFullTime(time: Date) {
		const date = new Date(time);

		const year = date.getFullYear();
		let month = '' + (date.getMonth() + 1);
		let day = '' + date.getDate();
		let hours = '' + date.getHours();
		let minutes = '' + date.getMinutes();

		if (month.length < 2) {
			month = '0' + month;
		}
		if (day.length < 2) {
			day = '0' + day;
		}
		if (hours.length < 2) {
			hours = '0' + hours;
		}
		if (minutes.length < 2) {
			minutes = '0' + minutes;
		}

		return [year, month, day].join('-') + ' ' + [hours, minutes].join(':');
	}

	async remove(id: string) {
		await this.userService.removeSession(id);
		this.sessions = await this.userService.getSessions();
	}
}
