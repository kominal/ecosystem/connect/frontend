import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user/user.service';
import { TranslateService } from '@ngx-translate/core';
import { PreferencesService } from 'src/app/core/services/preferences/preferences.service';

@Component({
	selector: 'app-core',
	templateUrl: './core.component.html',
	styleUrls: ['./core.component.scss'],
})
export class CoreComponent {
	constructor(
		router: Router,
		userService: UserService,
		public preferencesService: PreferencesService,
		public translateService: TranslateService
	) {
		if (userService.isLoggedIn()) {
			router.navigate(['/connect']);
		}
	}
}
