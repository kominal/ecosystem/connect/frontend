import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { CoreComponent } from './core.component';
import { HomeComponent } from './pages/home/home.component';
import { MaterialModule } from 'src/app/core/classes/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { GdprComponent } from './pages/gdpr/gdpr.component';

@NgModule({
	declarations: [CoreComponent, HomeComponent, GdprComponent],
	imports: [CommonModule, TranslateModule, CoreRoutingModule, MaterialModule],
})
export class CoreModule {}
