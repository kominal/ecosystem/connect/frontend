import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { GdprComponent } from './pages/gdpr/gdpr.component';

export const routesCore: Routes = [
	{ path: 'home', component: HomeComponent },
	{ path: 'gdpr', component: GdprComponent },
	{ path: '**', redirectTo: 'home' },
];

@NgModule({
	imports: [RouterModule.forChild(routesCore)],
	exports: [RouterModule],
})
export class CoreRoutingModule {}
