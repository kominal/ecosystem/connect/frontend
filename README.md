# Connect Frontend

## Download

|                | Production                                                                                                                              | Test                                                                                                                              |
| -------------- | --------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| Stable Windows | [Download](https://gitlab.com/kominal/connect/frontend/-/jobs/artifacts/master/raw/release/Connect.exe?job=Electron%20Production)       | [Download](https://gitlab.com/kominal/connect/frontend/-/jobs/artifacts/master/raw/release/Connect.exe?job=Electron%20Test)       |
| Stable Linux   | [Download](https://gitlab.com/kominal/connect/frontend/-/jobs/artifacts/master/raw/release/Connect.AppImage?job=Electron%20Production)  | [Download](https://gitlab.com/kominal/connect/frontend/-/jobs/artifacts/master/raw/release/Connect.AppImage?job=Electron%20Test)  |
| Latest Windows | [Download](https://gitlab.com/kominal/connect/frontend/-/jobs/artifacts/develop/raw/release/Connect.exe?job=Electron%20Production)      | [Download](https://gitlab.com/kominal/connect/frontend/-/jobs/artifacts/develop/raw/release/Connect.exe?job=Electron%20Test)      |
| Latest Linux   | [Download](https://gitlab.com/kominal/connect/frontend/-/jobs/artifacts/develop/raw/release/Connect.AppImage?job=Electron%20Production) | [Download](https://gitlab.com/kominal/connect/frontend/-/jobs/artifacts/develop/raw/release/Connect.AppImage?job=Electron%20Test) |
